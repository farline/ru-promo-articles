# Устойчивость к падениям в KDE Plasma

Архитектура KDE Plasma 5 построена так, чтобы минимизировать влияние фатальных ошибок в отдельных компонентах на общую стабильность рабочего окружения. Так, в отличие от, например, GNOME Shell, графический интерфейс и компоновщик выполняются в разных процессах:

![0](https://i.imgur.com/1k0s5IMr.png)

*Текущая архитектура Plasma*

Интерфейс Plasma (*plasmashell*) и компоновщик (*kwin*) обмениваются информацией через механизмы межпроцессного взаимодействия, такие как оконная система (Wayland или X11) и D-Bus, по возможности используя стандартные протоколы.

В сеансе Wayland KWin выполняет роль сервера для графических приложений и управляет их отображением на экране. В случае с X11 и приложения, и KWin выступают в роли клиентов X Server, что обеспечивает дополнительную изоляцию: перезапуск KWin не влияет на работу приложений.

Расширения Plasma работают в процессе `plasmashell`. Если одно из них приводит к падению, графическая оболочка перезапускается без ущерба для запущенных приложений. Кроме того, расширения не имеют доступа к коду компоновщика, что положительно сказывается на безопасности.

При подготовке KWin к замене X Server был значительно повышен уровень покрытия кода KWin автоматическими тестами.

## Следующие шаги

В дальнейшем планируется разделить саму графическую оболочку на несколько процессов. Первые шаги в этом направлении были сделаны в Plasma 5.12: плагины поисковой системы KRunner получили возможность запуска вне графической оболочки, и эта возможность уже используется для некоторых нестабильных плагинов.

Но впереди более значимое изменение: из `plasmashell` будут вынесены расширения Plasma:

![1](https://i.imgur.com/OgFZWjT.png)

*Планируемая архитектура*

Смена архитектуры не повлияет на совместимость с расширениями.

## Подробнее

David Edmundson в деталях расскажет о работе над архитектурой в своём докладе на [Akademy](https://conf.kde.org/en/Akademy2018/public/events/66). Позже подробная статья будет опубликована в его [блоге](https://blog.davidedmundson.co.uk/).

*Автор:* Eike Hein  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://blogs.kde.org/2018/08/02/engineering-plasma-extensions-and-stability-—-present-and-future>
