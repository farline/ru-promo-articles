# KDE.ru Promo Articles

This repository is used for collaborative translation and editing of KDE-related news, blog posts, and articles.

## How-to

To preserve formatting of articles, we use the common [Markdown](https://commonmark.org/help/) format.

If the article you want to translate is not in this repository yet, you can generate a Markdown document for translation using [ru-promo-scripts](https://invent.kde.org/ilyabizyaev/ru-promo-scripts).

For translation, consider using an offline text editor that supports Markdown linting and spell checking. For example, [Visual Studio Code](https://code.visualstudio.com) with [LanguageTool integration](https://marketplace.visualstudio.com/items?itemName=davidlday.languagetool-linter) and a [Markdown linter](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint).

Make sure your text follows the [guidelines](https://community.kde.org/RU/Контрольный_список_для_переводов_статей): they list the most common pitfalls that will otherwise have to be addressed during review.

**To submit your changes:**

* Log in to Invent with your Identity account. If you don't have one, [get it here](https://identity.kde.org); make sure to specify your name in English during registration.
* (For those who don't know Git) Make changes using the “Edit” button in GitLab above the target file: just replace its contents with your work. Open a merge request (or commit your work directly, if you have push access).
* You can make changes to your uploaded work by editing the file in your repository fork. When the translation is accepted, consider deleting the fork (from its settings) to simplify the workflow for the next time.
* If this turns out to be too difficult, ask the team or open an [issue](https://invent.kde.org/ilyabizyaev/ru-promo-articles/-/issues) instead and attach your translation there.

When your translation gets edited, you can view editor's changes by clicking the commit name above the viewed file. The fewer changes are required, the sooner your work is published — so learn from this review! 😉

## Useful links

### Tools

* [KDE Translation Search](https://l10n.kde.org/dictionary/search-translations.php)
* [LanguageTool](https://languagetool.org/ru)
* [Yoficator](https://e2yo.github.io/eyo-browser)
* [Reverso Context](https://context.reverso.net/перевод/английский-русский/)

### Resources

* [Published articles (VK)](https://vk.com/@kde_ru)
* [Russian front end & VCS dictionaries](https://github.com/web-standards-ru/dictionary)
* [gramota.ru search](http://new.gramota.ru/spravka/buro/search-answer)
* [RusCorpora](https://ruscorpora.ru/new/search-main.html)
