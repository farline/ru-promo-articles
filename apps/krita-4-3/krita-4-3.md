# Примечания к выпуску Krita 4.3

Krita 4.3.0 выходит спустя год после релиза Krita 4.2.0, она наполнена новыми функциями, но в основном — многими и многими исправлениями ошибок. Мы хотим, чтобы это был самый стабильный релиз Krita! Мы потратили более года, полностью сосредоточившись на улучшении стабильности и производительности в Krita — но также добавили множество новых возможностей. Теперь всё больше и больше людей ведут работу над Krita, что даёт отличные результаты: в Krita 4.3.0 более чем 2000 изменений по сравнению с Krita 4.2.9!

![0](https://krita.org/wp-content/uploads/2020/05/kiki_4.3.3_sm.png)

В этом выпуске также содержится информация о работе над проектами Google Summer of Code (GSoC) 2019. Sharaf Zaman работал над адаптацией Krita для работы на Android, а первая бета-версия (на основе Krita 4.2.9) поступила в Google Play Store для планшетов с Chrome OS и Android. Krita 4.3.0 будет доступна там как вторая бета: пока мы не будем довольны состоянием Krita на Android, она будет оставаться в бета-канале. Tusooa Zhu переписал систему отмены действия в Krita, и это позволило добавить панель снимков. А Kuntal Majumder добавил совершенно новый магнитный инструмент выделения.

<https://www.youtube.com/embed/VTlh2KhkOM4>

## Android

На Android теперь поддерживаются действия Samsung Air. Анна Медоносова добавила жесты для стилуса, поддерживаемые Samsung Tab S6, Note 9 (частично) и Note 10. Вы можете настроить действия в диалоговом окне «Настроить Krita…».

## Анимация

С начала марта Emmet и Eoin работали над целой кучей исправлений, оптимизаций и улучшений в наших инструментах для анимации.

Несмотря на то, что многие из этих изменений не будут включены в Krita до следующего крупного релиза, некоторые из них попали в Krita 4.3.0:

* **Новая функция:** В диалоге обработки анимации появилась новая опция экспорта только уникальных кадров анимации.
* **Новая функция:** Мы сделали новые действия с горячими клавишами для выбора предыдущего или следующего родственного слоя, для работы внутри одной группы слоёв.
* **Исправление:** Общее улучшение кэширования анимации должно привести к более плавному и последовательному воспроизведению и прокрутке.
* **Исправление:** Скрытые слои теперь можно нормально редактировать и стирать в режиме активной изоляции.
* **Исправление:** Временная шкала теперь правильно выделяет текущий кадр при загрузке нового документа.
* **Исправление:** «Луковая кожа» больше не перекашивается после обрезки.
* **Исправление:** Присутствие масок трансформации на дереве слоёв больше не вызывает сбоев в воспроизведении анимации.
* **Исправление:** Настройки изображения в диалоге обработки анимации теперь независимы и больше не конфликтуют с настройками изображения в диалоге «Экспорт».
* **Исправление:** Диалог обработки анимации теперь корректно синхронизирует настройки HDR между параметрами видео и последовательности изображений.
* **Исправление:** Добавлены небольшие заголовки в несколько контекстных меню анимации, чтобы усложнить случайный выбор.
* **Исправление:** Сообщение об ошибке, которое показывалось, когда Krita не могла найти FFMPEG, теперь содержит полезную информацию и ссылку на официальную документацию.
* **Изменение:** Функция «Изолировать слой» теперь называется «Изолировать активный слой», чтобы лучше отражать её работу.
* Загляните на [страницу проекта](https://phabricator.kde.org/T12769), чтобы узнать подробности!

Наряду со многими из этих изменений, наши планы на будущее для анимационного рабочего процесса в Krita были основаны на отзывах и критике нашего сообщества.

Поэтому мы хотели бы поблагодарить всех, кто нашёл время, чтобы заполнить подробные отчёты об ошибках, поделиться своими предложениями, помочь в тестировании исправлений и заполнить наш недавний опрос («Animation Feedback Survey»). Спасибо!

## Ресурсы

* Ramon Miranda создал новый набор профилей кисти с эффектом акварели.

<https://krita.org/wp-content/uploads/2020/04/crocus_water_colorpreview.webm?_=1>

* Пакеты теперь правильно обрабатывают часовые пояса и показывают даты в предпочитаемом пользователем формате.
* Появилась пара новых шаблонов от David Gowers, которые хорошо подходят для фильтра палитризации.

## Фильтры

### Цветовое пространство карты градиента

Для поклонников пиксель-арта, в этом релизе появилось два изменения фильтров, оба из которых были реализованы Carl Olsson.

Первое — это выбор цветового пространства в фильтре [карты градиента](https://docs.krita.org/en/reference_manual/filters/map.html#gradient-map). Это позволяет установить промежуточные цвета, чтобы использовать шаблон сглаживания или ограничить цвета́ до ближайшего цве́та опорной точки.

![2](https://krita.org/wp-content/uploads/2020/04/krita_43_dither_gradient_map.png)

### Фильтр палитризации

Второе изменение, созданное Carl Olsson, — это новый [фильтр палитризации](https://docs.krita.org/en/reference_manual/filters/map.html#palettize), который работает аналогично фильтру карты градиента, но использует палитру для определения цветов. Фильтр палитризации также поддерживает размывание:

![3](https://krita.org/wp-content/uploads/2020/04/krita_4_3_paletize_filter.png)

### Фильтр высоких частот

[Фильтр высоких частот](https://docs.krita.org/en/reference_manual/filters/edge_detection.html#gaussian-high-pass), созданный Miguel Lopez, особенно хорош для того, чтобы сделать изображение более резким. Лучше всего применять его в качестве слоя с наложением. Этот фильтр особенно полезен в качестве последнего шага для изображений, загружаемых в социальные сети; дополнительная резкость компенсирует ужасные алгоритмы масштабирования, используемые на хостингах изображений, которые делают изображения размытыми.

![4](https://krita.org/wp-content/uploads/2020/04/highpass_filter_sharpen.png)

Слева вверху: оригинал;  
справа вверху: повышение резкости;  
справа внизу: повышение резкости с помощью фильтра наложения;  
справа внизу: тонкое повышение резкости.

### Другие улучшения фильтров

* Были исправлены утечки памяти в фильтре карты градиента.
* Фильтр определения края и фильтр преобразования высоты в карту нормалей больше не показывают артефакты «лесенкой».
* Все свёрточные фильтры (такие как резкость, размытие) теперь корректно работают на изображениях с непрозрачным фоном.
* Фильтр HSV теперь корректнее работает с изображениями в градациях серого.
* Фильтр размывания теперь правильно рассчитывает соотношение сторон.
* Фильтр размывания в движении теперь производит меньше артефактов.

## Слои

* Производительность стилей слоёв была улучшена.
* Функция отдельных каналов снова работает.
* Диалоговое окно «Разделить изображение» теперь поддерживает разделение по направляющим, перетаскиваемым из линейки, а также имеет улучшенный предварительный просмотр.
* Клонирование слоёв теперь работает намного лучше и намного стабильнее.
* Диалог для изменения источника клонирующих слоёв: ещё один вклад Tusooa, теперь вы можете изменить источник клонирующего слоя, чтобы он указывал на другой слой. Клонирующие слои действуют как экземпляры исходного слоя и могут использоваться для интересных эффектов, но до сих пор их нельзя было указывать на другой исходный слой.

## Python

* `ManagedColor` теперь также может быть инициализирован с помощью `QColor`.
* Добавлен метод `setDocument` в класс `View`.
* Действия, создаваемые в расширениях Python, загружаются до создания меню и панелей инструментов.

## Живопись

* Кисти RGBA: Krita теперь позволяет настраивать непрозрачность и яркость [окрашенных кончиков кисти](https://docs.krita.org/en/reference_manual/brushes/brush_settings/brush_tips.html#brush-mode) отдельно. Это создаёт ряд новых возможностей, среди которых — возможность получить текстуру, напоминающую масло или акриловый импасто.

<https://www.youtube.com/embed/BMQzJ5n6LPE>

* Движок обычных кистей теперь на 20% быстрее.

### Многомерный экспорт для GIH

Формат [GIMP image hose (gih)](https://docs.krita.org/en/general_concepts/file_formats/file_gih.html) поддерживает несколько измерений, поэтому можно иметь несколько рядов кистей, которые могут быть установлены в произвольное положение по горизонтали и, возможно, будут увеличиваться по вертикали. И теперь экспортёр Krita поддерживает это благодаря работе Ivan Yossi!

![5](https://krita.org/wp-content/uploads/2019/06/gih.png)

*Изображение, показывающее многомерное отображение кисти Gimp. Изображение со штампом чередуется между левой и правой рукой и вращается в соответствии с углом.*

## Работа с окнами

Теперь можно вынести центральную область холста из окна и поместить её в собственное окно: режим отсоединённого холста удобен, когда у вас есть большой и маленький экран, так что вы можете поместить изображение на большой экран, а все инструменты и панели — на маленький экран.

![6](https://krita.org/wp-content/uploads/2020/05/separate_view.png)

## Работа с цветом

* Рендеринг каналов Lab теперь выполняется правильно как в окне настройки каналов, так и на холсте, благодаря L.E. Segovia.
* Инструмент выбора цвета больше не теряет контраст при регулировке насыщенности ползунка HSL, а изменение освещённости лучше компенсирует сохранение правильной насыщенности.
* Теперь Krita пытается распознать сломанные профили ICC, которые устанавливает Photoshop, и не пробует их загрузить.
* Палитры с более чем 4096 столбцами теперь загружаются и отображаются правильно
* Инструменты выбора цвета не только работают намного лучше, но также более корректны и просты в использовании благодаря работе Mattias Wein:

![7](https://krita.org/wp-content/uploads/2020/05/2020-05-14_two-panels-HSV-RGB_selector43.gif)

## Панель снимков

[Панель снимков](https://docs.krita.org/en/reference_manual/dockers/snapshot_docker.html), являющаяся частью работы Tusooa по переписыванию системы отмены действия в Krita, позволяет сохранять определённые состояния прогресса и переключаться между ними.

![8](https://krita.org/wp-content/uploads/2020/04/krita_4_3_snapshot_docker.gif)

*Панель снимков в действии, переключение между различными этапами рисования*

## Инструменты

Некоторые инструменты поставляются с «действиями», такими как увеличение и уменьшение размера кончика кисти. Эти действия теперь создаются до загрузки изображения, поэтому их можно поместить на панель инструментов.

### Инструмент магнитного выделения

Проект GSoC 2019 от Kuntal Majumer, [инструмент магнитного выделения](https://docs.krita.org/en/reference_manual/tools/magnetic_select.html), делает свободное выделение, но с одной особенностью: он пытается выровнять выделение по краям, которые может найти внутри изображения, значительно упрощая процесс выделения.

![9](https://krita.org/wp-content/uploads/2020/04/magnetic_selection_mode_mixed.gif)

### Новые режимы для инструмента градиентов

![10](https://krita.org/wp-content/uploads/2020/04/krita_4_3_spiralgradients.png)

Спираль, обратная спираль и билинейный режим были добавлены Miguel Lopez.

### Инструмент заливки и инструмент выделения смежных областей («волшебная палочка»)

Теперь в [инструменте заливки](https://docs.krita.org/en/reference_manual/tools/fill.html) и [инструменте выделения смежных областей](https://docs.krita.org/en/reference_manual/tools/contiguous_select.html) («волшебной палочке») вы можете выбирать между всеми слоями, текущим слоем и слоями с цветовой маркировкой, чтобы указать, какую область необходимо заполнить или выделить инструментом.

!![11](https://krita.org/wp-content/uploads/2020/05/Fill_Tool_Labeled_Layers.png)

![12](https://krita.org/wp-content/uploads/2020/05/Magic_Wand_Labeled_Layers.png)

## Прочее

* Создание выделений теперь намного быстрее.
* Теперь Krita очень старается убедиться, что файл действительно был сохранён правильно: мы проверяем размер, дату, открываем файл, проверяем содержимое сразу после сохранения.
* Wolthera исправила действительно неприятную ошибку: выделенные участки, находящиеся полностью за пределами изображения, можно было сохранить, а при загрузке рисование оказывалось невозможным.
* Saurabh Kumar добавил возможность открывать изображение как файловый слой в уже загруженном изображении.
* Появилась новая опция сохранения изображений в формате .kra со всеми слоями, обрезанными до размера изображения. По умолчанию она отключена; включите её, если вы склонны перемещать объекты за границы изображения и не хотите сохранять эти данные между сеансами.

*Автор:* Команда Krita  
*Перевод:* [Дима](https://vk.com/pxllime), [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://krita.org/en/krita-4-3-release-notes/>
