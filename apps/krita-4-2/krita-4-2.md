# Примечания к выпуску Krita 4.2

Версия 4.2 графического редактора Krita [была выпущена](https://krita.org/en/item/krita-4-2-0-is-out/) 29 мая 2019. В то время как основная команда сосредоточена на стабильности и производительности, в выпуск также входит много новых возможностей, улучшений производительности и усовершенствований обработки палитры, выполненных студентами в ходе Google Summer of Code 2018. Итак, поехали!

## Обновлённая поддержка планшетов для Windows, Linux и macOS

Мы наконец-то справились с объединением кода, который написали для поддержки планшетов на Windows (как Wintab, так и Windows Ink), Linux и macOS, с существующим кодом на нашей платформе для разработки, Qt. Это улучшило поддержку многомониторных конфигураций, расширило поддержку планшетов и исправило многие ошибки. Огромное количество работы!

**Примечание: чтобы реализовать эти функции, нам потребовалось вносить изменения в Qt. [Правки были опубликованы](https://phabricator.kde.org/T10838), но могут не быть включены в ваш дистрибутив Linux. Пока этого не произойдёт, вам может потребоваться использовать Linux AppImage вместо сборки Krita для вашего дистрибутива**

## Рисование с HDR

<https://www.youtube.com/embed/Uy_22vFu1R0>

*Анимация HDR, созданная Agata Cacko в Krita*

Krita могла работать с изображениями HDR с 2005, но теперь возможно просматривать HDR-изображения в HDR на поддерживаемом оборудовании. Вы теперь можете не только сохранять свои HDR изображения в файлах .kra или OpenEXR, но также в расширенном PNG. С правильной версией FFMPEG вы можете даже создавать анимации в HDR! Подготовить правильную конфигурацию компьютера для этого может быть довольно сложно, поэтому перейдите к документации, чтобы посмотреть, что требуется. *HDR-дисплеи поддерживаются только в Windows 10*.

Если у вас включён HDR, то в панели «Простой выбор цвета» появится дополнительный ползунок [«ниты»](https://ru.wikipedia.org/wiki/Нит_(единица_измерения)), который позволяет вам менять яркость конкретного цвета.

<https://www.youtube.com/embed/V_0ocw1H3vs?start=51>

*Krita на CES 2019 (0:50)*

* Функция — <https://docs.krita.org/en/reference_manual/hdr_display.html>
* Общая информация — <https://docs.krita.org/en/general_concepts/colors/scene_linear_painting.html>

Эта работа стала возможной благодаря сотрудничеству с Intel. Также потребовалось помочь этим изменениям для HDR попасть в фреймворк, который используется для разработки Krita, Qt.

## Улучшенная производительность и скорость кисти благодаря векторизации и неблокирующему программированию

![1](https://krita.org/wp-content/uploads/2018/09/avx_cgauss_60.gif)

Два наших студента на Google Summer of Code 2018 ускорили Krita при помощи техник программирования, известных как неблокирующая хэш-таблица (для управления данными пикселей, Андрей Камакин) и векторизация видеоядром (Ivan Yossi). Неблокирующая хэш-таблица должна улучшить скорость Krita за счёт многопоточности, график показывает прирост производительности в зависимости от числа ядер вашего процессора. Векторизация для кисти Гаусса и мягкой кисти оптимизирует Krita, используя способность вашего процессора делать похожие вычисления очень быстро, GIF выше показывает различие в скорости для кисти Гаусса.

![2](https://krita.org/wp-content/uploads/2018/09/lockless.png)

Левая ось на графике — время в миллисекундах. Вы можете видеть, что операции рисования ускорились с 1,5 секунд до 1 секунды с неблокирующей хэш-таблицей. Синяя линяя показывает, как раньше работала Krita.

## Улучшенная панель цветовой палитры

![3](https://krita.org/wp-content/uploads/2018/10/color-palette-improvements.png)

Улучшенная цветовая палитра от одного из наших студентов Google Summer of Code 2018 — Michael Zhou. Она более стабильна; кроме того:

1. Вместо панели на основе элементов — панель на основе строк и столбцов;
2. она может содержать пустые элементы, что удобно для упорядочивания;
3. стабильное перетаскивание цветов;
4. простое добавление в элементы щелчком по ним в панели;
5. щелчок ПКМ удаляет элемент;
6. палитры могут быть добавлены в файл KRA;
7. вы можете нажать на значок папки для открытия диалога изменения палитры, где можно указать палитре место сохранения в документе или папке ресурсов.

## API на Python для анимации

<https://www.youtube.com/embed/VsYGtPXgJIg>

Контролируйте и создавайте настраиваемые алгоритмы работы, связанные с анимациями.

* Переход к указанному кадру
* Установка частоты кадров
* Установка длины воспроизведения начального и конечного кадров

Уже создана пара плагинов, использующих некоторых из этих функций. Также отметим исправление в API узлов/слоёв, где вызов «scaleNode» передавал неверный аргумент.

**Плагин импорта видео для аниматора:** <https://github.com/scottpetrovic/animator-video-reference>  
**Менеджер листов спрайтов:** <https://github.com/Falano/kritaSpritesheetManager>

## Настраиваемое резервное копирование файлов

![4](https://krita.org/wp-content/uploads/2019/02/4-2-file-backup-options.png)

Появились параметры, контролирующие, как делаются ваши резервные копии. Если хотите, вы можете даже настроить местоположение резервных копий ваших файлов. Эти параметры находятся в главном меню «Настроить Krita» в разделе «Общие».

Если у вас есть очень большие файлы, и вы испытываете проблемы с сохранением, вы можете включить опцию Zip64.

## Маска по цветовой гамме

![5](https://krita.org/wp-content/uploads/2019/02/4-2-gamut-mask.png)

Новая панель цветовой гаммы, где вы можете ограничить показываемые вам цвета. Её добавила новая участница Анна Медоносова. Эта возможность доступна с художественным выбором цвета и панелью круга расширенного цвета. Примечание: стандартный треугольник выбора цвета не предоставляет такой возможности.

Вы также можете вращать маску по гамме, используя ползунок. Создать новые маски и изменить уже существующие можно через панель маски гаммы. Вы можете отключить маску при помощи значка селектора вверху, слева от ползунка поворота.

## Новости о Krita

![6](https://krita.org/wp-content/uploads/2019/04/news-widget-splash-screen-2.png)

На начальный экран был добавлен виджет новостей, что позволяет Krita получать последние новости с krita.org. Теперь, если произойдёт что-то интересное, вроде нового выпуска, вы узнаете первым! Щелчок по элементу новости отправит вас на веб-страницу. Он выключен по умолчанию, так что вам надо его включить. Особая благодарность Alvin Wong за включение библиотеки OpenSSL.

## Улучшенный художественный выбор цвета

![7](https://krita.org/wp-content/uploads/2018/09/improved-artistic-color-selector.png)

Более функциональный и облегчённый художественный выбор цвета. Теперь есть «непрерывный режим» (символ бесконечности), который позволяет вам удалить шаг у определённых параметров. Дополнительные функции для новой маски по гамме. Анна Медоносова добавила эту замечательную возможность.

## Отмена действий с инструментом перемещения

![8](https://krita.org/wp-content/uploads/2018/09/undo-move-feature.gif)

Инструмент перемещения теперь учитывается в истории отмены. Это значит, что вы можете отменить несколько перемещения подряд, когда используете инструмент перемещения.

## Перемещение и изменение выделений

<https://www.youtube.com/embed/gWv--Do9L0E>

С лёгкостью перемещайте, поворачивайте или изменяйте выделение. Вы можете даже изменить точки привязки, с которыми сделано выделение, чтобы, например, скруглить углы.

## Улучшено отображение используемой памяти

![9](https://krita.org/wp-content/uploads/2019/03/image.png)

Лучше показывает, когда у вашего компьютера заканчивается память. Это также может быть полезно при создании анимации, когда вам не хватает памяти для отрисовки. Эта полоска уже была в строке состояния, но теперь должна помогать лучше предупреждать пользователей.

## Улучшения окна обзора

![10](https://krita.org/wp-content/uploads/2019/02/4-2-rotate-flip.png)

Быстро поворачивайте и отражайте холст из окна обзора. Также лучше сохраняется соотношение сторон, и не происходит растяжения при наличии скрытых слоёв.

## Изменение размера миниатюр слоёв

<https://krita.org/wp-content/uploads/2018/08/resize-thumbnail.mp4?_=1>

Добавлен новый ползунок в панели слоёв, который позволяет вам изменять размер миниатюр слоёв, чтобы видеть их крупнее. Размер сохраняется, когда вы возвращаетесь к Krita в следующий раз.

## Улучшение мультикисти

![12](https://krita.org/wp-content/uploads/2019/02/4-2-multi-brush-preview.png)

Лучший предпросмотр при показе нескольких осей.

Также появился новый режим, «Перенос». Он позволяет задать несколько курсоров на экране и рисовать одновременно. Он доступен через параметры инструмента, как и другие режимы.

## Улучшение производительности маски рисования

<https://krita.org/wp-content/uploads/2019/05/painting_selection_mask.webm?_=2>

Вы можете создать выделение обычными кистями, используя функцию глобального выделения. До сих пор это было медленной областью. Теперь они гораздо быстрее. Также было сделано маленькое изменение рабочего процесса: если вы отобразите маску глобального выделения без выделения, по умолчанию будет выбрано всё.

## Улучшения в выделении непрозрачности

<https://krita.org/wp-content/uploads/2019/05/select_opque_improvements.webm?_=3>

Ctrl+щелчок по предпросмотру слоя в панели слоёв выделяет содержимое слоя. Это то же самое, что и функция выделения непрозрачности по щелчку правой кнопкой. Также появились новые методы для выделения слоев. Действие «Выделить непрозрачную область» было переименовано в «Выделить непрозрачную область (заменить)» с учётом новых функций.

* Ctrl+щелчок = замена выделения
* Ctrl+Shift+щелчок = добавить выделение
* Ctrl+Alt+щелчок = вычесть выделение
* Ctrl+Shift+Alt+щелчок = пересечь выделение

## Изменение резкости

![15](https://krita.org/wp-content/uploads/2019/05/sharpness_in_motion.gif)

Опция резкости, которая устанавливает фильтр по порогу для текущей кисти, теперь позволяет контролировать этот порог давлением, упрощая создание щетинистых кистей из любых с помощью пиксельной кисти.

## Изменение нажима/прозрачности

![16](https://krita.org/wp-content/uploads/2019/05/flow_opacity_adapt_flow_preset.gif)

Нажим и прозрачность теперь работают более схоже с другими программами. GIF сверху показывает, как изменить кисть, использующую новое поведение, чтобы вернуться к старому поведению. Новое поведение должно сделать в разы проще создание тонких штрихов. На данный момент вы можете вернуться к использованию старого поведения глобально, перейдя в *Настроить Krita… → Общие → Инструменты → Режим нажима на кисть*.

## Клонирующая кисть – сброс источника

<https://krita.org/wp-content/uploads/2019/05/clone_brush-v2.webm?_=4>

Новая функция была добавлена к клонирующей кисти, которая позволяет вам сбрасывать источник после каждого мазка кисти.

## Генератор симплексного шума

<https://krita.org/wp-content/uploads/2019/05/fill_simplex_noise.webm?_=5>

Добавлена возможность динамически добавлять шум в ваш документ. Также есть функции для создания бесшовного шума.

## Новые режимы наложения

<https://krita.org/wp-content/uploads/2019/05/blend_modes.webm?_=6>

Новые категории режимов наложения были добавлены, чтобы создавать интересные эффекты.

## Другие изменение и исправления ошибок

За более подробной информацией обращайтесь к анонсу на английском языке на официальном сайте проекта Krita: <https://krita.org/en/krita-4-2-release-notes/>.

*Автор:* Команда Krita  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://krita.org/en/krita-4-2-release-notes/>

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: directory → каталог -->
<!-- 💡: icon → значок -->
<!-- 💡: location → расположение -->
<!-- 💡: thumbnail → миниатюра -->
<!-- 💡: upstream → [компонент-]зависимость | исходный репозиторий -->
