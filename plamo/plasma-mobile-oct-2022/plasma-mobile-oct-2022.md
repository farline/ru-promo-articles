# Выпуск Plasma Mobile Gear 22.09

Команда Plasma Mobile рада сообщить о достижениях разработки за период с июля по сентябрь 2022 года, а также о выпуске набора приложений Plasma Mobile Gear версии 22.09.

## Оболочка

Plasma 5.26 сейчас тестируется, выпуск запланирован на 11 октября.

### Шторка действий

Yari добавил в список уведомлений кнопку «Очистить все уведомления» и быструю настройку «Не беспокоить», отключающую всплывающие окна уведомлений.

Devin добавил в быструю настройку мобильной передачи данных более подробные предупреждения, сообщающие об отсутствии SIM-карты или конфигурации имени точки доступа (APN).

![0](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/notifications-1.png)

*Кнопка очистки уведомлений*

![1](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/notifications-2.png)

*Быстрая настройка «Не беспокоить»*

### Панель навигации

Aleix добавил в диспетчер окон KWin возможность сообщать о том, поддерживают ли приложения самостоятельное открытие виртуальной клавиатуры, а Devin использовал это в панели навигации: теперь кнопка вызова клавиатуры появляется, если текущее приложение не умеет само показывать клавиатуру. Это полезно для приложений, работающих через прослойку XWayland.

![2](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09//keyboard-button.png)

На снимке — приложение Freetube, построенное на фреймворке Electron и запущенное в XWayland, что для системы означает отсутствие поддержки виртуальных клавиатур.

### Переключатель задач

Yari добавил кнопку для закрытия всех запущенных приложений. Для её срабатывания требуется двойное касание.

![3](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09//task-switcher-close-all.png)

### Строка состояния

Devin исправил ошибку, из-за которой в строке состояния при подключении к мобильной сети дублировался значок статуса передачи данных.

### Домашний экран

Devin работал над новым стандартным домашним экраном («Halcyon»), нацеленным на простоту и использование одной рукой. Он также обновил внешний вид страницы настроек домашнего экрана, упростив переключение между старым («Folio») и новым домашними экранами.

![4](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/halcyon-1.png)

*Страница «Избранное»*

![5](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/halcyon-2.png)

*Страница приложений*

![6](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/homescreen-1.png)

*Меню настройки*

![7](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/homescreen-2.png)

*Страница настройки*

### Прочее

Devin работал над возможностью параллельной установки Plasma Mobile и обычной настольной версии Plasma, упрощая необходимую настройку. Теперь достаточно просто сменить «Оформление рабочей среды» в Параметрах системы на «Plasma Mobile» и войти в сеанс с одноимённым названием. Кроме того, команда запуска Plasma Mobile была изменена с `kwinwrapper` на `startplasmamobile`.

На видео ниже — Plasma Mobile в дистрибутиве Arch Linux, запущенная на планшете Surface Pro 3. Над удобством использования на планшетах ещё предстоит много работать!

<https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09//surface-pro.mp4>

## Приложения

### Номеронабиратель

Алексей Андреев работал над экраном входящего вызова, а также исправлял ошибки, связанные с приёмом входящих звонков, показом уведомлений и вибрацией, автоматическим переключением аудиорежимов, непрерывной интеграцией для проверки правок от разработчиков. Благодаря содействию Влада Загороднего в ревью изменений и фундаменту, который заложил Aleix Pol, предложив расширение протокола Wayland и добавив его поддержку в KWin, Алексей реализовал перекрытие экрана блокировки экраном входящего звонка. Ещё Алексей добавил новый элемент управления для приёма звонков смахиванием.

Michael добавил функцию блокировки вызовов с неизвестных номеров.

Marco исправил подавление перехода в спящий режим во время звонка.

![9](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/dialer-answer-swipe.png)

*Экран входящего вызова*

![10](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/dialer-call-blocking.png)

*Страница настройки блокировки вызовов*

<https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09//dialer-answer-swipe.mp4>

### Подкасты (Kasts)

Devin упростил интерфейс, объединив страницу подкаста и страницу со списком эпизодов. Он улучшил страницы с информацией о подкасте и конкретном эпизоде, поменяв их структуру и вынеся кнопки действий из заголовка приложения на панель инструментов.

Bart добавил таймер сна и реализовал некоторые маленькие пожелания: синхронизацию отметок прослушивания с сервером (gpodder), опцию для пометки указанного числа эпизодов непрослушанными при добавлении подкаста, запоминание геометрии окна; наконец, он исправил несколько ошибок в интерфейсе.

![12](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/kasts-1.png)

*Страница подкаста*

![13](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/kasts-2.png)

*Страница эпизода*

![14](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/kasts-3.png)

*Таймер сна*

### Погода

Han Young перевёл фоны, соответствующие погоде, на отрисовку напрямую через OpenGL, чтобы улучшить производительность на маломощных устройствах.

Devin обновил стиль настроек и улучшил поддержку планшетов: список местоположений и настройки теперь отображаются в диалоговых окнах на достаточно больших экранах. Ещё Devin исправил проблему, из-за которой местоположение не менялось после его удаления в динамическом виде.

![15](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/kweather-1.png)

*Фон, нарисованный напрямую через OpenGL*

![16](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/kweather-2.png)

*Новые настройки*

![17](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/kweather-3.png)

*Диалог выбора местоположений*

![18](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/kweather-4.png)

*Диалог настройки в настольной версии*

### Эмулятор терминала (QMLKonsole)

Devin обновил стиль настроек, добавил возможность настройки шрифта и степени прозрачности фона терминала. Он также работал над улучшением терминала для использования на планшетах: добавил панель вкладок и сделал так, что настройки открываются в диалоговом окне, когда для этого достаточно места.

![19](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/qmlkonsole-1.png)

*Полупрозрачный фон*

![20](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/qmlkonsole-2.png)

*Новые настройки*

![21](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/qmlkonsole-3.png)

*Терминал на планшете*

![22](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/qmlkonsole-4.png)

*Диалог настройки на планшете*

### Настройки

Devin обновил стиль многих страниц настроек. Он также добавил больше информации на страницу сведений о батарее.

![23](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/settings-1.png)

*Стартовая страница*

![24](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/settings-2.png)

*Информация о системе*

![25](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/settings-3.png)

*Настройки экранной клавиатуры*

![26](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/settings-4.png)

*Настройки даты и времени*

![27](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/settings-5.png)

*Параметры энергопотребления*

![28](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/settings-6.png)

*Страница сведений о батарее*

### Почтовый клиент Raven

Devin возобновил работу над «родным» почтовым клиентом для Plasma Mobile, в основе которого лежит Pelikan — прототип почтового клиента, использующего Akonadi, начатый Carl Schwan. Raven пока умеет только показывать письма; запланировано совместное с Kalendar использование компонентов поддержки электронной почты.

![29](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/raven-1.png)

![30](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/raven-2.png)

![31](https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/raven-3.png)

### Клиент Matrix (NeoChat)

Многое произошло в NeoChat. В частности, James Graham реализовал поддержку настройки уведомлений на уровне комнат. Snehit в рамках своего проекта для Google Summer of Code добавил в список комнат фильтр по вхождению в выбранное пространство. James также переработал временную шкалу, чтобы она выглядела лучше, когда окно очень широкое. Было внесено множество других исправлений и улучшений в переводы, расположение элементов и стабильность.

## Как присоединиться?

Хотите помочь с развитием Plasma Mobile?

Если вам не терпится попробовать Plasma Mobile, прочитайте [эту страницу](https://www.plasma-mobile.org/get/), чтобы узнать о поддерживаемых устройствах для каждого дистрибутива. В нашей [документации](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Issue-Tracking) написано, как сообщить о возможных проблемах. Наконец, присоединяйтесь к нашему [чату в Matrix](https://go.kde.org/matrix/#/#plasmamobile:matrix.org) и дайте нам знать, над чем бы вы хотели поработать!

*Автор:* Команда Plasma Mobile  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://plasma-mobile.org/2022/09/27/plasma-mobile-gear-22-09/>  

<!-- 💡: Dialer → Номеронабиратель -->
<!-- 💡: icons → значки -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
