# Май/июнь в KDE PIM

*Автор:* [Volker Krause](https://www.volkerkrause.eu/about/)

[Вслед за Daniel](https://www.dvratil.cz/2019/05/march-and-april-in-kde-pim/), настала моя очередь предоставить вам обзор того, что произошло вокруг Kontact за прошедшие два месяца. Правда, с более чем 850 изменениями от 22 людей в репозиториях KDE PIM, это будет довольно поверхностно.

## Почтовый клиент KMail

Что касается почты, значимым направлением работы были безопасность и приватность:

* Sandro работал над дальнейшим укреплением KMail против так называемых атак «decryption oracle» (ошибка 404698). Это атака, при которой части атакуемого зашифрованного сообщения аккуратно встраиваются в другое сообщение, чтобы заставить предполагаемого получателя случайно расшифровать их при ответе на письмо атакующего.
* Jonathan Marten добавил более детализованный контроль над настройками прокси для подключений IMAP.
* André улучшил выбор ключа и процесс принятия ключа для OpenGPG и S/MIME.

Laurent также продолжил работу над рядом новых функций для продуктивности в составителе писем:

* Поддержкой цветных эмодзи Unicode:

![0](https://www.volkerkrause.eu/assets/posts/25/color-emoji-menu.png)

* Интеграцией со средствами проверки правописания, такими как [LanguageTool](https://languagetool.org/) и [Grammalecte](https://grammalecte.net/):

![1](https://www.volkerkrause.eu/assets/posts/25/grammalecte-error.png)

* Поддержкой Markdown, с возможностью изменять HTML-содержимое письма в синтаксисе Markdown:

![2](https://www.volkerkrause.eu/assets/posts/25/markdown-link.png)

Конечно, это не всё, у нас много исправлений и улучшений в KMail:

* Albert устранил бесконечный цикл, когда кэш списка цепочек сообщений повреждён.
* David исправил падение Kontact при выходе (ошибка 404881).
* Laurent исправил доступ ко второму и последующим сообщениям при предпросмотре файлов MBox (ошибка 406167).
* Плагин извлечения маршрута стал работать лучше благодаря исправлениям в движке, смотрите [эту запись](https://www.volkerkrause.eu/2019/06/01/kde-itinerary-april-may-2019.html) для подробностей.

И, конечно, не обошлось без устранения общих неприятных недочётов:

* Исправлены прыжки курсора в поле Bcc в новом письме (ошибка 407967).
* Исправлено открытие конфигурации сервиса уведомлений о новой почте.
* Исправлен размер окна настроек, который был слишком маленьким (ошибка 407143).
* Мастер аккаунтов теперь реагирует на Alt+F4 (ошибка 388815).
* Исправлено положение всплывающего окна в просмотре сообщения с масштабом, отличным от 100%.
* Исправлен импорт прикреплённых файлов vCard (ошибка 390900).
* Добавлено сочетание клавиш для блокировки/разблокировки режима поиска.
* David исправил проблему взаимодействия с панелью хода выполнения в строке состояния.

## KOrganizer

Что касается календаря, большинство работы относилось к попыткам сделать [KCalCore частью KDE Frameworks 5](https://www.volkerkrause.eu/2019/04/27/kf5-kcontacts-and-kcalcore.html), что особенно выгодно разработчикам, использующим KCalCore за пределами KDE PIM. Изменения в KCalCore также нацелены на упрощение его использования в QML, за счёт преобразования типов данных в неявно разделяемые с аннотациями `Q_GADGET`. Эта работа скоро должна подойти к завершению, так что мы сможем продолжить процесс ревью для KF5.

Из заметных для пользователей исправлений:

* Исправлен бесконечный цикл в модели задачи в случае дублирования UID.
* Улучшена видимость временно́й шкалы и диаграммы Ганта в KOrganizer с тёмными цветовыми схемами.
* Damien Caliste исправил кодировку продолжительности с нулевой задержкой в формате iCal.

## Управление контактами (KAddressBook)

Как и работа с календарём, работа с контактами также включала ряд изменений, связанных с внесением KContacts в KDE Frameworks 5. Пересмотр кода, использующего KContacts, привёл к включению в библиотеку ряда повторяющихся шаблонов, а также к улучшению кода работы с контактами, что упростит его поддержку. Попутно был исправлен ряд проблем вокруг интеграции KContacts и Grantlee, например, были устранены ограничения, относящиеся к локализации просмотра и печати контактов.

Есть ещё один шаг, требуемый для завершения подготовки KContacts для KDE Frameworks 5: переход от устаревших собственных записей в vCard к элементу IMPP для адресов обмена сообщениями.

## Просмотрщик лент новостей Akregator

Akregator также получил немного исправлений:

* Heiko Becker исправил связь уведомлений с приложением.
* Wolfgang Bauer исправил падение с Qt 5.12 (ошибка 371511).
* Laurent исправил проблемы с размером шрифта комментариев на экранах с высоким DPI (ошибка 398516) и отображение комментариев ленты в диалоге свойств ленты (ошибка 408126).

## Общая инфраструктура

Пожалуй, самое важное изменение за прошедшие два месяца произошло в Akonadi: Daniel реализовал путь для автоматического восстановления из ужасной ошибки «Несколько кандидатов для слияния» (ошибка 338658). Это состояние ошибки, в котором содержимое базы данных Akonadi может оказаться по всё ещё неизвестной причине, и это до сих пор мешало успешной синхронизации IMAP. Akonadi теперь может автоматически восстанавливаться из этого состояния и возвращаться к внутренней целостности при следующей синхронизации с сервером IMAP.

Но это ещё не все:

* Другое важное исправление ослабило ограничение на размер удалённых идентификаторов до 1024 символов (ошибка 394839), также от Daniel.
* David исправил ряд утечек памяти.
* Filipe Azevedo исправил несколько специфичных для macOS проблем развёртывания.
* Daniel реализовал улучшения при использовании PostgreSQL в качестве бэкенда для Akonadi.

Коннекторы бэкендов также не остались без внимания:

* Для ресурса Kolab были исправлены проблемы управления памятью, код был избавлен от зависимости от KDELibs4Support.
* David Jarvie исправил несколько проблем с конфигурацией в ресурсе KAlarm.
* Laurent исправил проблемы конфигурации в ресурсе MBox.

Утилита `pimdataexporter`, которая позволяет импортировать и экспортировать весь набор настроек KDE PIM и связанных данных, тоже получила большое число изменений: исправления различных проблем импорта и экспорта, а также улучшения связности и формулировок в интерфейсе.

## Помогите нам сделать Kontact ещё лучше!

Взгляните на задачи для новичков, которые у нас есть! Они простые, большинство задач по программированию не требуют какого-либо глубокого знания или понимания Kontact, так что каждый может работать над ними. Можете выбрать любую задачу из списка и написать нам! Мы будем рады помочь вам и ответить на все ваши вопросы. [Подробнее здесь…](https://www.dvratil.cz/2018/08/kde-pim-junior-jobs-are-opened)

*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://www.volkerkrause.eu/2019/06/30/kde-pim-may-june-2019.html>
