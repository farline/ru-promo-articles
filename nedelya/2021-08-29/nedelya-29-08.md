# На этой неделе в KDE: цвет акцента!

> 22–29 августа, основное — прим. переводчика

## Новые возможности

Страница «Цвета» Параметров системы [теперь позволяет вам выбрать цвет акцента, отличающийся от определённого в цветовой схеме](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/305) (Jan Blackquill, Plasma 5.23):

![0](https://pointieststick.files.wordpress.com/2021/08/screenshot_20210827_220236.png)

## Wayland

В сеансе Plasma Wayland пользователи проприетарного драйвера NVIDIA версии 470 и новее [больше не будут страдать от вертикально отражённых или чёрных окон приложений XWayland](https://bugs.kde.org/show_bug.cgi?id=440852) (Xaver Hugl, Plasma 5.22.4)

Многомониторные конфигурации теперь [сохраняются между сеансами X11 и Wayland](https://bugs.kde.org/show_bug.cgi?id=385135) (David Edmundson, Plasma 5.23)

Возможность Spectacle автоматически копировать снимок экрана в буфер обмена [теперь работает корректно в сеансе Wayland](https://bugs.kde.org/show_bug.cgi?id=421974) (Méven Car, Spectacle 21.08.1)

В сеансе Wayland [панель задач теперь показывает отклик запуска на значках приложений при нажатии на них](https://bugs.kde.org/show_bug.cgi?id=402903), как в X11! (Влад Загородний, Plasma 5.23)

При повороте экрана в сеансе Wayland, вручную или автоматически (по датчику положения), [теперь показывается анимированный переход между ориентациями](https://invent.kde.org/plasma/kwin/-/merge_requests/1204) (Aleix Pol Gonzalez, Plasma 5.23)

## Исправления ошибок и улучшения производительности

Вы снова можете [переименовывать элементы из контекстного меню панели папок Dolphin](https://bugs.kde.org/show_bug.cgi?id=441124) (Jan Paul Batrina, Dolphin 21.08.1)

Действие Spectacle «Открыть содержащую папку» [теперь открывает правильный путь после копирования снимка в буфер обмена вместо ручного или автоматического сохранения где-либо](https://bugs.kde.org/show_bug.cgi?id=441047) (Jan Paul Batrina, Spectacle 21.12)

Dolphin [больше не открывает без надобности новое окно после сжатия или распаковки файлов в Ark через контекстное меню](https://bugs.kde.org/show_bug.cgi?id=440663) (Alexander Lohnau, Dolphin 21.08.1)

Действие «Восстановить масштаб» в Dolphin [теперь работает при отключённых миниатюрах](https://bugs.kde.org/show_bug.cgi?id=437349) (Евгений Попов, Dolphin 21.08.1)

Plasma [больше не зависает при доступе к виджету или всплывающему меню буфера обмена, если любой из элементов очень длинный](https://bugs.kde.org/show_bug.cgi?id=431673) ([ValdikSS](https://habr.com/ru/users/ValdikSS/), Plasma 5.23)

Фон блокировщика экрана [больше не смещается в многомониторных конфигурациях](https://bugs.kde.org/show_bug.cgi?id=419545) (David Edmundson, Plasma 5.23)

Discover [стал немного быстрее запускаться и отображать свой начальный экран](https://invent.kde.org/plasma/discover/-/merge_requests/155) (Aleix Pol Gonzalez, Plasma 5.23)

Системный монитор [теперь корректно сообщает информацию о процессорах на FreeBSD](https://invent.kde.org/plasma/ksystemstats/-/merge_requests/14) (Adriaan de Groot, Plasma 5.23)

Исправлены [две существенные утечки памяти, затрагивавшие Plasma и её виджеты](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/323) (Matt Whitlock, Frameworks 5.86)

## Улучшения пользовательского интерфейса

Страница «Ночная цветовая схема» Параметров системы [теперь сообщает вам, когда вы совершаете действие, которое запросит местоположение через сторонние сервисы](https://bugs.kde.org/show_bug.cgi?id=419677) (Bharadwaj Raju, Plasma 5.23):

![2](https://pointieststick.files.wordpress.com/2021/08/screenshot_20210823_121040.png)

На странице обратной связи Параметров системы [вы можете увидеть историю отправки данных в KDE (если она совершалась)](https://bugs.kde.org/show_bug.cgi?id=441181) (Aleix Pol Gonzalez, Plasma 5.23)

Различные значки Breeze, связанные с закладками, [теперь визуально отличаются друг от друга](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/119) (Nate Graham, Frameworks 5.86)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://pointieststick.com/2021/08/27/this-week-in-kde-accent-colors/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: icons → значки -->
<!-- 💡: location → расположение -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
