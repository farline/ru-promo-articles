# На этой неделе в KDE: дробное масштабирование в Wayland

Эта неделя вдвойне крутая! Мы не только реализовали долгожданную поддержку дробного масштабирования под Wayland, но и наконец-то в корне устранили заклятые ошибки в работе Plasma с несколькими экранами!

> 11–18 декабря, основное — прим. переводчика

## Дробное масштабирование в Wayland

На прошлой неделе был принят [протокол Wayland для дробного масштабирования](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/143), предложенный Kenny Levinson. А уже на этой неделе были добавлены его реализации на стороне [KDE](https://invent.kde.org/plasma/kwin/-/merge_requests/2598) (Plasma 5.27) и [Qt](https://codereview.qt-project.org/c/qt/qtwayland/+/420041), подготовленные David Edmundson. Огромное всем спасибо!

«Что это даёт?», спросите вы. Отвечаю: это позволяет фреймворку Qt задействовать в сеансе Wayland существующую поддержку дробного масштабирования, ранее использовавшуюся в X11. Приложения больше не придётся рисовать в завышенном до следующего целочисленного множителя разрешении и затем уменьшать до нужного размера! Как итог, приложения на Qt с дробным масштабом (не 200%, а, например, 125%) теперь должны выглядеть менее размытыми, работать быстрее и тратить меньше энергии.

А что же насчёт приложений GTK? Они получат те же преимущества, если GTK когда-нибудь добавит поддержку дробного масштабирования и реализует упомянутый протокол. До тех пор приложения GTK продолжат использовать менее эффективный метод масштабирования.

Когда всё это станет доступным вам? Итак, диспетчер окон KWin уже поддерживает всё необходимое в Plasma 5.27. Поддержка в Qt сейчас есть только в шестой версии, что обычно говорит о необходимости ждать Plasma 6. Однако есть шанс, что мы адаптируем необходимые изменения к Qt 5 и включим их в набор правок Qt от KDE. Следите за новостями!

## Исправление работы с несколькими мониторами

Несколько экранов — это всегда сложно, поскольку требует поддержки от многих составляющих программной архитектуры. В корне большинство наших проблем были вызваны использованием идентификаторов разъёмов для определения экранов и сопоставления экранов с рабочими столами и панелями Plasma (так называемыми «контейнерами»). Это работало плохо, потому что идентификаторы разъёмов могут меняться (и меняются) при самых разных обстоятельствах. В итоге на практике получалась каша с либо случайным, либо стабильно неправильным поведением.

Теперь всё иначе. [Подробности можно прочитать здесь](https://notmart.org/blog/2022/12/multi-screen/). Вкратце, теперь мы используем систему, основанную на нумерации экранов, и контейнеры Plasma тесно привязаны к этим номерам, но сами номера могут перемещаться между экранами в зависимости от их числа. Так, например, когда экран 1 с рабочим столом и панелью Plasma становится недоступным, другой экран становится экраном 1, а рабочий стол и панель Plasma перемещаются на него.

Эта новая система должна значительно повысить стабильность, надёжность и предсказуемость в отношении того, как экраны включаются и отключаются, где размещаются и какие рабочие столы Plasma показывают. Она исправляет печально известные ошибки, такие как [произвольное перемещение контейнеров Plasma и их потерю](https://bugs.kde.org/show_bug.cgi?id=450068), а также [утрату настроек рабочих столов (обоев, виджетов, параметров значков)](https://bugs.kde.org/show_bug.cgi?id=427861). Также обеспечено сохранение [расположений экранов](https://bugs.kde.org/show_bug.cgi?id=429236) и [контейнеров Plasma](https://bugs.kde.org/show_bug.cgi?id=385135) между сеансами Plasma X11 и Wayland. Большое событие.

Теперь давайте немного скорректируем ожидания: всё перечисленное не значит, что исправлены вообще все ошибки в многомониторных конфигурациях. Скорее, у нас появился новый фундамент без заведомых ошибок проектирования, на котором мы можем решать проблемы, не создавая в процессе новых. Теперь мы действительно можем постепенно делать взаимодействие с несколькими экранами надёжнее и надёжнее, а не перекладывать проблемы из одной стопки в другую от выпуска к выпуску. Изменения войдут в Plasma 5.27, над ними вместе работали Marco Martin, Иван Ткаченко, Xaver Hugl и David Edmundson. Огромная вам благодарность, друзья!

## Другие новые возможности

Теперь можно изменять порядок окон в панели задач на противоположный на вертикальных панелях (Tanbir Jishan, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1301))

В сеансе Plasma Wayland теперь можно разрешить приложениям, использующим XWayland, подслушивать нажатия клавиш, сделанные в родных приложениях Wayland, как будто они работают в X11. Это уязвимое поведение по умолчанию отключено, но может пригодиться для некоторых приложений, например, для режима рации в Discord. Есть несколько уровней, чтобы можно было настроить под себя баланс безопасности и поддержки устаревших технологий (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/1950)):

![0](https://pointieststick.files.wordpress.com/2022/12/image-4.png)

Клавиши-модификаторы (например, одиночную Meta) теперь можно использовать в качестве комбинаций клавиш при настройке вызова действий с клавиатуры. Со временем это позволит нам заменить странную старую обработку клавиш-модификаторов в KWin и просто напрямую назначать клавиши-модификаторы на такие действия, как вызов меню приложений или запуск режима обзора (Aleix Pol Gonzalez, Plasma 5.27 и Frameworks 5.102. [Ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1268), [ссылка 2](https://invent.kde.org/frameworks/kguiaddons/-/merge_requests/77) и [ссылка 3](https://invent.kde.org/frameworks/kdeclarative/-/merge_requests/162))

Центр приложений Discover получил специальный модуль для работы на SteamOS, так что теперь можно выполнять обновления системы на устройствах Steam Deck из режима рабочего стола (Jeremy Whiting, Plasma 5.27, но, возможно, в SteamOS появится даже раньше. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/410))

## Улучшения пользовательского интерфейса

Боковая панель Spectacle теперь показывает отдельные кнопки для разных режимов создания снимка экрана. Раньше для выполнения снимка в определённом режиме требовалось 2 щелчка: один для выбора режима, второй для запуска захвата (Noah Davis, Spectacle 23.04. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/162)):

![1](https://pointieststick.files.wordpress.com/2022/12/image-5.png)

Строка поиска и запуска KRunner больше не ищет среди имён исполняемых файлов приложений, так как это вызывало слишком много ложных срабатываний при поиске несвязанных вещей (Alexander Lohnau, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2423))

При изменении настроек индексации файлов, требующих перезагрузки для вступления в силу, Параметры системы теперь выводят сообщение об этом с большой удобной кнопкой для немедленной перезагрузки (Nate Graham, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1271)):

![2](https://pointieststick.files.wordpress.com/2022/12/baloo-kcm-reboot-message.png)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Отключение вставки щелчком по колёсику мыши в сеансе Plasma Wayland больше не ломает выделение текста в некоторых приложениях GTK и не приводит к их сбою (Влад Загородний, Plasma 5.26.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461498))

В Wayland подключение внешних экранов теперь работает для различных устройств на базе ARM (Xaver Hugl, Plasma 5.26.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462214))

Когда Discover устанавливает обновления для дополнений из KDE Store, выводящих в процессе обновления сообщение об ошибке или вопрос, Discover теперь показывает их вам, а не просто игнорирует, оставляя вас в замешательстве (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460136))

Отключение и повторное включение крайнего верхнего левого экрана больше не приводит к тому, что он начинает дублировать экран справа от него (Иван Ткаченко, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463045))

Диалоговое окно «Добавить правило…» на странице «Брандмауэр» Параметров системы теперь работает как надо с брандмауэром `ufw` (Paul Worall, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461726))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 4 ошибки в Plasma с очень высоким приоритетом (на прошлой неделе было 6). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 47 (на прошлой неделе их было 46). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 96 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-12-09&chfieldto=2022-12-16&chfieldvalue=RESOLVED&list_id=2231378&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Теперь у нас [есть целая система для автоматизированного тестирования графического интерфейса через механизм специальных возможностей](https://apachelog.wordpress.com/2022/12/14/selenium-at-spi-gui-testing/), что убивает одним выстрелом двух зайцев! Новую схему тестирования сразу же испытали на виджете калькулятора (Harald Sitter, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/291)). Не стесняйтесь, напишите свой тест! Документация [тут](https://invent.kde.org/sdk/selenium-webdriver-at-spi/-/wikis/writing-tests).

Добавлен базовый автоматический тест для комнат Plasma (Александр Кузнецов, Plasma 5.27. [Ссылка](https://invent.kde.org/network/kio-extras/-/merge_requests/206))

На [портале для разработчиков](https://develop.kde.org) появилась [инструкция по написанию продвинутых виджетов Plasma, содержащих код на C++](https://develop.kde.org/docs/extend/plasma/widget/c-api/) (Chris Holland, [ссылка](https://invent.kde.org/documentation/develop-kde-org/-/merge_requests/207))

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/12/16/this-week-in-kde-wayland-fractional-scaling-oh-and-we-also-fixed-multi-screen/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icon set → набор значков -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
