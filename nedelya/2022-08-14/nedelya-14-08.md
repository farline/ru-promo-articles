# На этой неделе в KDE: крупные улучшения доступности

> 7–14 августа, основное — прим. переводчика

Хотя процесс выбора новых целей KDE ещё не завершён, участники проекта всерьёз начали работать над доступностью Plasma для лиц с ограниченными возможностями! Начиная с Plasma 5.26, все предустановленные виджеты будут [совместимы со средством чтения с экрана](https://invent.kde.org/plasma/plasma-desktop/-/issues/41) — спасибо Fushan Wen и Harald Sitter, который ему помогал! Дальше — больше, а пока расскажу про другие прекрасные наработки:

## Новые возможности

Теперь можно управлять разрешениями общих папок Samba удалённо! (Harald Sitter, kdenetwork-filesharing 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=40892))

Плагин OpenConnect VPN модуля управления сетями Plasma теперь поддерживает протоколы «F5», «Fortinet» и «Array» (Enrique Melendez, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/144))

В стандартном меню запуска приложений теперь есть опциональный «компактный» режим, позволяющий одновременно видеть больше элементов. При использовании сенсорного режима, компактный режим для удобства использования автоматически отключается (Nate Graham, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/699))

* ![0](https://pointieststick.files.wordpress.com/2022/08/big-mode.jpeg)

*Обычный режим*

* ![1](https://pointieststick.files.wordpress.com/2022/08/compact-mode.jpeg)

*Компактный режим*

Глобальные темы (Параметры системы > Оформление рабочей среды) теперь могут менять порядок и расположение кнопок в заголовках окон, а также управлять опцией «Безрамочные распахнутые окна» (`BorderlessMaximizedWindows`). Вы можете согласиться на изменение этих параметров или отказаться от него при применении темы (Dominic Hayes, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1682))

По умолчанию модули, предоставляющие обои рабочего стола, больше не загружают новые данные, когда используется тарифицируемое сетевое подключение — но это ограничение можно снять (Fushan Wen, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/207)):

![2](https://pointieststick.files.wordpress.com/2022/08/checkbox.jpeg)

Теперь можно стереть историю поиска в панели команд (Ctrl+Alt+I) (Евгений Попов, Frameworks 5.98. [Ссылка](https://invent.kde.org/frameworks/kconfigwidgets/-/merge_requests/147))

## Улучшения пользовательского интерфейса

Страница «Ночная цветовая схема» Параметров системы теперь позволяет вам использовать карту, чтобы вручную выбрать местоположение, а также показывает индикатор загрузки, когда используется автоматическое определение местоположения и сервис ещё не вернул результат (Bharadwaj Raju, Plasma 5.26. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1642), [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1997)):

![3](https://pointieststick.files.wordpress.com/2022/08/night-color-map.jpeg)

Анимации открытия и закрытия эффектов «Обзор», «Все окна» и «Все рабочие столы» теперь длятся дольше и задействуют другую функцию плавности, благодаря чему ощущаются куда плавнее (Blake Sperling, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/2781))

## Заметные исправления

(Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Применение глобальной темы со встроенной цветовой схемой теперь автоматически меняет цвет во всех запущенных приложениях GTK, использующих тему Breeze (David Redondo, Plasma 5.24.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=421745))

Исправлено серьёзное ухудшение поддержки нескольких мониторов в сеансе Plasma Wayland, которое могло привести к отсутствию изображения на экранах (Xaver Hugl, Plasma 5.25.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=455532))

В сеансе Wayland некоторые запущенные приложения, к примеру, GIMP, больше не забывают иногда появиться на панели задач (Влад Загородний, Plasma 5.25.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=444325))

Исправлено значимое аварийное завершение Plasma, связанное с панелью задач (Nicolas Fella, Plasma 5.25.5. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2010))

Другие сведения об ошибках, которые могут вас заинтересовать:

* 50 15-минутных ошибок в Plasma (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 25 очень высокоприоритетных ошибок в Plasma (на прошлой неделе было 26). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 125 ошибок исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-08-05&chfieldto=2022-08-12&chfieldvalue=RESOLVED&list_id=2130941&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/08/12/this-week-in-kde-major-accessibility-improvements/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: task manager → панель задач -->
