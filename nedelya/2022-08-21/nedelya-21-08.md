# На этой неделе в KDE: режим выделения в Dolphin

> 14–21 августа, основное — прим. переводчика

Сегодня мы добавили кое-что очень крутое: у Dolphin теперь есть специальный «режим выделения», который вы при желании можете использовать, чтобы упростить выбор объектов на сенсорном экране или с включённой опцией открытия файлов по одиночному щелчку! В этом режиме даже есть панель инструментов с подходящими по ситуации действиями для выбранных элементов! При использовании мыши и клавиатуры вы можете легко вызвать или покинуть этот режим, нажав пробел, удерживая элемент в области просмотра или с помощью соответствующего пункта контекстного меню. Использование этой функции совершенно опционально, так что если вы предпочитаете выбирать файлы по-старому, можете им просто не пользоваться. Большое спасибо Felix Ernst, который [реализовал эту возможность для Dolphin 22.12](https://bugs.kde.org/show_bug.cgi?id=427202)!

![0](https://pointieststick.files.wordpress.com/2022/08/selection-mode-in-dolphin.jpeg)

…Но это далеко не всё! Читайте дальше!

## Другие новые возможности

В музыкальном проигрывателе Elisa теперь есть полноэкранный режим (Nate Graham, Elisa 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=429730)):

![1](https://pointieststick.files.wordpress.com/2022/08/elisa-full-screen.jpeg)

Теперь вы можете изменить системный формат адресов, имён и номеров телефона (Akseli Lahtinen, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=430801)):

![2](https://pointieststick.files.wordpress.com/2022/08/more-things-to-configure.jpeg)

Стандартное меню запуска приложений теперь можно настроить на отображение в горизонтальной панели в виде надписи, а не значка (Denys Madureira, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1004))

Текстовый редактор Kate теперь позволяет настроить шрифт, которым будет напечатан документ, прямо в диалоговом окне печати (Christoph Cullmann, Frameworks 5.98. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457480))

Добавлена генерация миниатюр файлов для изображений RAW формата `.arw` (Mirco Miranda, Frameworks 5.98. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=454208))

## Улучшения пользовательского интерфейса

На странице исполнителя в Elisa теперь показываются сеткой все его альбомы, вместо моря ни о чём не говорящих одинаковых значков (Stefan Vukanović, Elisa 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=406475)):

![3](https://pointieststick.files.wordpress.com/2022/08/elisa-artist-grid.jpeg)

При включении случайного порядка воспроизведения в Elisa воспроизводимая в данный момент дорожка теперь всегда остаётся первой в перемешанном списке песен (Дмитрий Колесников, Elisa 22.12. [Ссылка](https://invent.kde.org/multimedia/elisa/-/merge_requests/379))

Когда вы настраиваете особые параметры окон, диалог со списком доступных свойств теперь остаётся открытым, пока вы явным образом его не закроете, что удобно для добавления нескольких свойств подряд (Ismael Asensio, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/1954))

Теперь вы можете запускать исполняемые файлы из результатов поиска в меню запуска приложений, строке поиска и запуска, эффекте «Обзор» и т. д.; отныне вам будет предложен стандартный диалог «Что сделать с этим файлом?» (Nate Graham, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=455924))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Круглый индикатор задержки показа в уведомлениях Plasma теперь полностью видим вне зависимости от плотности пикселей экрана и коэффициента масштабирования (Евгений Попов, Plasma 5.24.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=435004))

Меню запуска приложений, отличные от стандартного (Kickoff), [снова могут искать файлы](https://bugs.kde.org/show_bug.cgi?id=456562) (Alexander Lohnau, Plasma 5.25.5)

Пролистывание в стандартном меню запуска приложений снова работает на сенсорных экранах (Noah Davis, Plasma 5.25.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=450448))

Глобальными комбинациями клавиш теперь можно запускать приложения, которые в пункте `Exec=` своих файлов `.desktop` задают аргументы командной строки (Nicolas Fella, Frameworks 5.98. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=440507))

Приложения на основе библиотеки Kirigami и области просмотра, задействующие распространённый компонент `FormLayout`, больше не будут иногда зависать в случайные моменты при определённых сочетаниях настроек шрифтов, размеров окон и содержимого (Connor Carney, Frameworks 5.98. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457675))

Другие сведения об ошибках, которые могут вас заинтересовать:

* 51 15-минутная ошибка в Plasma (на прошлой неделе было 50). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 22 очень высокоприоритетных ошибок в Plasma (стало меньше по сравнению с 25 на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 99 ошибок исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-08-12&chfieldto=2022-08-19&chfieldvalue=RESOLVED&list_id=2134042&query_format=advanced&resolution=FIXED)

## Изменения вне KDE, затрагивающие KDE

Та ошибка в Qt, из-за которой в приложениях на QtQuick исчезали вертикальные полосы прокрутки, была исправлена, и мы скоро добавим исправление в наш набор патчей для Qt (Mitch Curtis, версия Qt уже в пути к вам. [Ссылка](https://bugreports.qt.io/browse/QTBUG-104983))

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/08/19/this-week-in-kde-dolphin-selection-mode/>  

<!-- 💡: Kicker → классическое меню запуска приложений -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: thumbnail → миниатюра -->
<!-- 💡: upstream → [компонент-]зависимость | исходный репозиторий -->
