# На этой неделе в KDE: после Plasma 5.21

> 14–21 февраля, основное — прим. переводчика

На этой неделе мы выпустили [Plasma 5.21](https://kde.org/ru/announcements/plasma/5/5.21.0) и усердно работали над исправлением ошибок, которые вы, друзья, нашли в ней. 🙂 Честно говоря, я довольно сильно устал после этой недели, так что давайте сразу перейдём к делу:

## Нововведения

Теперь в Kate [можно выполнять](https://invent.kde.org/utilities/kate/-/merge_requests/255) основные операции Git, такие как просмотр изменений, отслеживание, фиксацию и [сохранение в запасник](https://invent.kde.org/utilities/kate/-/merge_requests/272)! (Waqar Ahmed, Kate 21.04)

Распахивание окон по горизонтали и вертикали [теперь работает](https://bugs.kde.org/show_bug.cgi?id=407793) в сеансе Plasma Wayland (Влад Загородний, Plasma 5.22)

## Исправления ошибок и улучшение производительности

Ark [больше не запрашивает](https://bugs.kde.org/show_bug.cgi?id=382606) подтверждение дважды при обновлении файла в архиве (Jan Paul Batrina, Ark 21.04)

Сеанс Plasma Wayland [больше не вылетает](https://bugs.kde.org/show_bug.cgi?id=433145) при входе в него на ноутбуках с Nvidia Optimus (Xaver Hugl, Plasma 5.21.1)

При нажатии на снимок экрана в Discover теперь [открывается соответствующее ему изображение](https://bugs.kde.org/show_bug.cgi?id=433123) (Aleix Pol Gonzalez, Plasma 5.21.1)

Стандартное меню запуска приложений теперь [работает со стилусом](https://bugs.kde.org/show_bug.cgi?id=433026) (Mikel Johnson, Plasma 5.21.1)

Стрелка назад в заголовке боковой панели Параметров системы больше [не выглядит неправильно](https://bugs.kde.org/show_bug.cgi?id=433062) при использовании набора значков, отличного от Breeze (Nate Graham, Plasma 5.21.1)

Синхронизация пользовательских настроек с экраном входа SDDM теперь [позволяет](https://bugs.kde.org/show_bug.cgi?id=432930) использовать нестандартные настройки шрифта, по крайней мере, при использовании SDDM версии 0.19 и выше (Filip Fila и David Redondo, Plasma 5.21.1)

Plasma больше [не падает](https://bugs.kde.org/show_bug.cgi?id=431334) при установке приложения Windows с помощью Wine (Kai Uwe Broulik, Frameworks 5.80)

Всплывающие подсказки кнопок по обе стороны от строки поиска и запуска (KRunner) больше [не глючат](https://bugs.kde.org/show_bug.cgi?id=433243) при отсутствии результатов поиска (Noah Davis, Frameworks 5.80)

Размещение элементов в виде сетки в Параметрах системы и в других местах больше [не создаёт](https://invent.kde.org/frameworks/kdeclarative/-/merge_requests/52#note_185751) раздражающее смещение между соседними элементами, где один имеет подзаголовок, а другой — нет (Nate Graham, Frameworks 5.80):

![0](https://pointieststick.files.wordpress.com/2021/02/aligned-now.png?w=942)

## Улучшения пользовательского интерфейса

Discover больше [не сокращает отзывы](https://bugs.kde.org/show_bug.cgi?id=433078), отображаемые на страницах приложений (Nate Graham, Plasma 5.21.1)

В разделе настройки экрана загрузки Параметров системы теперь [используется](https://invent.kde.org/plasma/plymouth-kcm/-/merge_requests/5) более приятный диалог получения материалов из сети в новом стиле (Alexander Lohnau, Plasma 5.22)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку фонду](https://kde.org/community/donations/) [KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)
*Перевод:* [Илья Андреев](https://vk.com/ilyandl)  
*Источник:* <https://pointieststick.com/2021/02/19/this-week-in-kde-plasma-5-21-is-finally-here/>

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
