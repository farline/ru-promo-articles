# На этой неделе в KDE: тестирование окупается

На этой неделе мы выпустили Plasma 5.26, и пока можно говорить о том, что наше внимание к контролю качества того стоило! Выпуск в целом прошёл гладко, сообщалось всего о нескольких ухудшениях, и большинство из них уже исправлены. Спасибо всем, кто тестировал, готовил отчёты об ошибках и, собственно, исправлял их!

## Новые возможности

Текстовые редакторы Kate и KWrite теперь используют KHamburgerMenu! Поскольку это большие и сложные приложения, по умолчанию по-прежнему отображается обычная строка меню. На данный момент меню-гамбургер показывает внутри себя полную традиционную структуру меню, а не пытается предложить выборку из основных действий. Это можно сделать в будущем! (Christoph Cullmann, Kate и KWrite 22.12. [Ссылка](https://invent.kde.org/utilities/kate/-/merge_requests/899)):

![0](https://pointieststick.files.wordpress.com/2022/10/image-1.png)

На экране приветствия Kate теперь больше функций (Евгений Попов, Kate 22.12. [Ссылка](https://invent.kde.org/utilities/kate/-/merge_requests/932)):

![1](https://pointieststick.files.wordpress.com/2022/10/image-3.png)

Сессия Plasma Wayland теперь поддерживает колёса прокрутки с высоким разрешением для более плавной прокрутки длинных областей просмотра (Влад Загородний, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457556))

Модуль настройки сетевых соединений Plasma теперь поддерживает 192-битный режим WPA3-Enterprise (Tomohiro Mayama, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/97))

## Улучшения пользовательского интерфейса

Диспетчер файлов Dolphin больше не открывает без необходимости новое окно после извлечения или создания архива с помощью контекстного меню (Андрей Бутырский, Dolphin 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=440663))

Центр приложений Discover больше не зависает на несколько секунд при запуске без подключения к Интернету, теперь он быстрее и лучше реагирует на временные сетевые проблемы с удалёнными ресурсами (Aleix Pol Gonzalez, Plasma 5.26.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=401666) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=457408))

Виджет «Проигрыватель» теперь лучше справляется с приложениями с примитивными реализациями MPRIS, такими как GNOME Videos (ранее Totem) и Celluloid (ранее GNOME MPV) (Bharadwaj Raju, Plasma 5.26.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=456516))

Теперь можно выделять и копировать текст надписей в приложении Информация о системе (Bharadwaj Raju, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=428881))

При ручной смене темы Plasma теперь воспроизводится приятный полноэкранный эффект перехода (Nate Graham, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2199))

Discover теперь показывает больше разрешений для приложений Flatpak, в том числе для доступа к принтеру и Bluetooth (Jakob Rech, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/372))

Ещё некоторые окна приложений KDE стали подниматься на передний план при внешней активации в сеансе Plasma Wayland: Параметры системы — при запуске из строки поиска и запуска KRunner, Discover — при вызове из меню внешних инструментов (KMoreTools), Dolphin — просто при активации из других приложений (Nicolas Fella, Plasma 5.26.1, Frameworks 5.100 и Dolphin 22.12. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=459213), [ссылка 2](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/213), [ссылка 3](https://invent.kde.org/system/dolphin/-/merge_requests/461))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Plasma больше не падает иногда при подключении внешней гарнитуры Bluetooth или при пробуждении системы после того, как она была переведена в ждущий режим во время потоковой передачи звука по сети (Harald Sitter, Plasma 5.24.7. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=454647), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=437272))

Изменение расположения или настроек экранов больше не приводит иногда к сбою Параметров системы (Кто-то классный, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=447199))

Система больше не теряет отзывчивость после использования комбинации клавиш «Выключить монитор» (Влад Загородний, Plasma 5.26.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460322))

Перетаскивание экранов для изменения их расположения на соответствующей странице Параметров системы больше не приводит иногда вместо этого к прокрутке представления или перемещению всего окна (Marco Martin, Plasma 5.26.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460280))

Веб-приложения Chrome больше не теряют собственный значок в пользу значка Chrome при запуске (Mladen Milinkovic, Plasma 5.26.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=358277))

При использовании сеанса Plasma Wayland в многомониторных конфигурациях с внешними экранами, которые не дублируются, система больше не считает иногда ошибочно, что они дублируются, и не включает режим «Не беспокоить», а также не забывает, включены ли эти экраны (Влад Загородний, Plasma 5.26.1. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=460247) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=460341))

Discover стал значительно лучше определять общий ход выполнения задач при установке или обновлении приложений Flatpak, так что он больше не будет таким странным и дёрганым (Aleix Pol Gonzalez, Plasma 5.26.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=435450))

Исправлено ухудшение в версии 5.26 взаимодействия диспетчера окон KWin с некоторыми сторонними эффектами (David Edmundson, Plasma 5.26.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460277))

Изображения, доступные по символическим ссылкам, снова появляются в обоях типа «Слайд-шоу» (Fushan Wen, Plasma 5.26.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460287))

Наконец-то полностью решена печально известная проблема с некрасивыми уголками! Исправлена последняя ошибка — светлые точки на закруглённых углах тёмных панелей (Niccolò Venerandi, Plasma 5.26.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=417511))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 10 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 8, но это вполне ожидаемо сразу после нового выпуска Plasma, т.к. мы используем этот приоритет для отслеживания наиболее важных недочётов, которые необходимо исправить). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 53 (на прошлой неделе было 49, но это в основном связано с работой нашего нового бота, автоматически помещающего существующие проблемы в эту категорию). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 141 ошибка была исправлена на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-10-07&chfieldto=2022-10-14&chfieldvalue=RESOLVED&list_id=2176812&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Это новый раздел, который я добавляю, чтобы осветить часть работы по достижению новой цели [«Автоматизация и систематизация»](https://phabricator.kde.org/T15627). Сюда входят темы автоматизации, документации, расширения тестового покрытия и всё остальное, что улучшает институциональную память KDE, перемещая информацию из голов людей в общедоступные системы.

Harald Sitter внёс большое изменение в нашего бота для Bugzilla. Теперь он будет [«подначивать»](https://invent.kde.org/sysadmin/bugzilla-bot/-/commit/f064fb18f78eed0dd6634f23f60f9c5a619852d2) пользователей, отправивших отчёт об ошибке из версии Plasma, выпущенной непосредственно до последней поддерживаемой версии, обновиться:

![2](https://pointieststick.files.wordpress.com/2022/10/image-2.png)

*«Благодарим вас за отчёт об ошибке! Обратите внимание, что поддержка Plasma 5.25.5 скоро закончится; поддерживаемые версии: 5.24, либо 5.26 и новее. По возможности выполните обновление до поддерживаемой версии и проверьте, воспроизводится ли проблема в ней»*

Что такое «бот для Bugzilla»? Это программа, которая:

* закрывает отчёты, которые в течение месяца находились в состоянии ожидания информации от автора (`NEEDSINFO WAITINGFORINFO`);
* автоматически выставляет серьёзность проблемы в значение «аварийное завершение» для отчётов, в названии которых есть «crash» или «segfault»;
* автоматически (но вежливо) закрывает отчёты об ошибках в слишком старых версиях Plasma.

Это всё тоже сделал Harald; спасибо ему! Это действительно круто, и [вам стоит помочь в развитии бота](https://invent.kde.org/sysadmin/bugzilla-bot), особенно если у вас есть навыки работы с Ruby!

Идеи для новых функций:

* Если к отчёту об ошибке, описывающему аварийное завершение, не прикреплён стек вызовов, просить об этом.
* Когда стек вызовов прикреплён в виде файла, автоматически извлекать из него кадр со сбоем и вставлять в отчёт сообщением.
* Если стек вызовов сгенерирован без отладочных символов, автоматически добавлять комментарий с просьбой доустановить их и прислать новый, прикрепляя ссылку на <https://community.kde.org/Guidelines_and_HOWTOs/Debugging/How_to_create_useful_crash_reports>.
* Просить пользователей повторно проверить наличие ошибок, не подтверждённых другими пользователями в течение года.

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/10/14/this-week-in-kde-qa-pays-off/>  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: task manager → панель задач -->
