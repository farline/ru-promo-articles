# На этой неделе в KDE: исправления нон-стоп

> 19–26 июня, основное — прим. переводчика

Первый корректирующий выпуск Plasma 5.25 вышел несколько дней назад, и следующий ожидается в начале следующей недели. Надеюсь, что большинство ошибок, которые вы нашли, исправлены! А среди них есть также несколько 15-минутных ошибок.

Иногда спрашивают: «Боже, такое ощущение, что вы всё время исправляете ошибки… не пора бы им всем уже быть исправленными? Почему в ваших программах так много косяков?» Дело в том, что такова природа программного обеспечения. Всегда находятся ошибки, которые нужно исправить, сколько бы вы над ними не работали. И чем больше людей используют программу, тем больше ошибок они в ней найдут. Это общее правило, верное для каждой программы. Лучшая метрика — это не количество исправленных ошибок, а, скорее, их «вопиющесть». Нужно стремиться к тому, чтобы ошибки, которые мы исправляем, со временем становились всё более странными и эзотерическими: что означает, что основа становится надёжнее. Мы ещё не дошли до этого момента, но я считаю, что мы делаем успехи!

## Исправленные «15-минутные ошибки»

Текущее число ошибок: **59, вместо бывших 65**. 0 добавлено, 2 оказались ошибками в зависимостях и 4 исправлены:

Окна из восстановленных сеансов [больше не восстанавливаются на неправильных виртуальных рабочих столах, когда используется загрузка при помощи Systemd](https://bugs.kde.org/show_bug.cgi?id=442380) (которая теперь по умолчанию) (David Edmundson, Plasma 5.25.2)

В сеансе Plasma X11 [кнопки в эффектах «Все окна» и «Обзор» больше не срабатывают только через раз](https://bugs.kde.org/show_bug.cgi?id=454275) (Marco Martin, Plasma 5.25.2)

Переключение между виджетами Plasma при помощи панели «Взаимозаменяемые виджеты» [теперь сохраняет их настройки, так что если вы переключитесь обратно на виджет, которым пользовались раньше, его параметры будут восстановлены](https://bugs.kde.org/show_bug.cgi?id=355588) (Fushan Wen, Plasma 5.26)

В сеансе Plasma X11 значок поиска, показываемый в полях поиска в виджетах Plasma и эффектах диспетчера окон KWin, [больше не смехотворно большой](https://bugs.kde.org/show_bug.cgi?id=454131) (Nate Graham, Frameworks 5.96)

[Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)

## Новые возможности

В сеансе Plasma Wayland [теперь можно отключить вставку по нажатию средней кнопки мыши](https://bugs.kde.org/show_bug.cgi?id=441668) (Méven Car, Plasma 5.26):

![0](https://pointieststick.files.wordpress.com/2022/06/middle-click-paste.png)

## Улучшения пользовательского интерфейса

Панель режима редактирования [теперь делится на несколько рядов, если экран недостаточно широкий, чтобы вместить её](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/991) (Fushan Wen, Plasma 5.25.2)

Виджеты «Переключатель», «Сворачивание всех окон» и «Показать рабочий стол» [теперь правильно обрабатывают фокус клавиатуры](https://bugs.kde.org/show_bug.cgi?id=454651) (Иван Ткаченко, Plasma 5.26)

Вход и выход из буквенной сетки в меню запуска приложений [теперь сопровождаются маленькой приятной анимацией](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/992) (Tanbir Jishan, Plasma 5.26):

<https://i.imgur.com/S7YLDLS.mp4?_=1>

Приложения на основе библиотеки Kirigami с боковыми панелями в настольном режиме [больше не показывают невидимую кнопку закрытия в правом нижнем углу боковой панели, на которую можно было случайно нажать и закрыть панель без возможности вернуть её](https://bugs.kde.org/show_bug.cgi?id=450902) (Frameworks 5.96)

Если значок приложения поменяется на диске, [Plasma теперь заметит это и покажет новый значок за 1 секунду, вместо бывших 10 секунд](https://invent.kde.org/frameworks/kded/-/merge_requests/20) (David Redondo, Frameworks 5.96)

Виджет «Батарея и яркость» [теперь показывает уровень заряда батареи подключённых беспроводных сенсорных панелей](https://invent.kde.org/frameworks/solid/-/merge_requests/90) (Влад Загородний, Frameworks 5.96)

В диалоге «Открыть с помощью…», который можно наблюдать в неизолированных приложениях, [теперь есть кнопка «Найти больше приложений в Discover…»](https://invent.kde.org/frameworks/kio/-/merge_requests/869), как в выглядящем по-другому аналогичном диалоге из изолированных приложений (Jakob Rech, Frameworks 5.96):

![2](https://pointieststick.files.wordpress.com/2022/06/button.png)

*И да, прежде чем вы спросите: глупо, что у нас есть два различных диалога «Открыть с помощью…» с разным видом и разными кодовыми базами. Их объединение — активная область работы!*

## Исправления ошибок и улучшения производительности

Ползунок воспроизведения в музыкальном проигрывателе Elisa [снова работает правильно, если текущая дорожка дольше 3 минут](https://invent.kde.org/multimedia/elisa/-/merge_requests/364) (Bart De Vries, Elisa 22.04.3)

Диалог удалённого рабочего стола для изолированных приложений [теперь появляется, когда надо](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/114) (Jonas Eymann, Plasma 5.24.6)

В эффекте «Все окна» снова можно [активировать окна не на том экране, на котором вводился текст в фильтр](https://invent.kde.org/plasma/kwin/-/merge_requests/2572) (Marco Martin, Plasma 5.25.2)

Внешние экраны, подключённые по USB-C, [снова работают как положено](https://bugs.kde.org/show_bug.cgi?id=454086) (Xaver Hugl, Plasma 5.25.1)

[Исправлено множество проблем с поиском, навигацией и выбором при помощи клавиатуры в новом эффекте «Все окна»](https://invent.kde.org/plasma/kwin/-/merge_requests/2562), что возвращает удобство его использования с клавиатуры до уровня Plasma 5.24 (Niklas Stephanblom, Plasma 5.25.2)

Снова [можно выбирать рабочие столы при помощи клавиатуры в эффекте «Все рабочие столы»](https://bugs.kde.org/show_bug.cgi?id=455292) (Влад Загородний, Plasma 5.25.1)

В сеансе Plasma X11 [распахивание окон на левую или на правую половину экрана больше не вызывает временами странное мерцание](https://bugs.kde.org/show_bug.cgi?id=455617) (Влад Загородний, Plasma 5.25.1)

Квадратная подсветка значков [снова появляется при наведении курсора на значки на доске приложений](https://bugs.kde.org/show_bug.cgi?id=453980) (Иван Ткаченко, Plasma 5.25.2)

Применение новой опции «Использовать цвет выделения для тонирования» [теперь тонирует также заголовок окна, не требуя дополнительно отмечать флажок, который явным образом применяет цвет выделения к заголовку окна](https://bugs.kde.org/show_bug.cgi?id=455395) (Евгений Попов, Plasma 5.25.2)

Настройка продвинутых правил брандмауэра [снова работает](https://invent.kde.org/plasma/plasma-firewall/-/merge_requests/46) (Daniel Vrátil, Plasma 5.25.2)

При использовании традиционной панели задач, открытые задачи [больше не переставляются спонтанно, когда закреплённое приложение перемещается с отключённой опцией «Запретить перемешивать с кнопками запуска»](https://bugs.kde.org/show_bug.cgi?id=444816) (Fushan Wen, Plasma 5.26)

Встроенные кнопки в списке учётных записей в клиенте сети Matrix NeoChat [снова видны](https://bugs.kde.org/show_bug.cgi?id=453959) (Jan Blackquill, Frameworks 5.96)

У диалоговых окон в настольном режиме [больше нет избыточного внутреннего отступа снизу](https://invent.kde.org/frameworks/kirigami/-/merge_requests/569#note_471790) (Ismael Asensio, Frameworks 5.96)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* Пётр Шкенев  
*Источник:* <https://pointieststick.com/2022/06/24/this-week-in-kde-a-mad-bugfixing-spree/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: upstream → [компонент-]зависимость | исходный репозиторий -->
