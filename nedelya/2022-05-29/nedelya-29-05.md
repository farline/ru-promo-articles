# На этой неделе в KDE: изменяемый размер окон виджетов

Мы занимаемся отчётами об ошибках, которые вы отправляете нам в рамках тестирования бета-версии Plasma 5.25, и на данный момент их осталось [15](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2075033&query_format=advanced&version=5.24.90&version=5.25.0&version=5.25.1&version=5.25.2&version=5.25.3&version=5.25.4&version=5.25.5). Работа над ними — отличный способ быстро изменить ситуацию к лучшему!

Кроме того, функции, которые не попали в Plasma 5.25, начинают появляться в версии 5.26. На этой неделе есть несколько очень крутых вещей, так что давайте приступим:

> 22–29 мая, основное — прим. переводчика

## Исправленные «15-минутные ошибки»

Текущее число ошибок: 64, вместо бывших 63. 3 добавлены и 2 исправлены:

В сеансе Plasma X11 уведомления Plasma, экранные уведомления и всплывающие окна виджетов [больше нельзя ненадлежащим образом сворачивать и распахивать](https://bugs.kde.org/show_bug.cgi?id=411462) (Luca Carlon, Plasma 5.26)

[Исправлена ошибка](https://bugs.kde.org/show_bug.cgi?id=411462) диспетчера окон KWin, из-за которой он мог выходить из строя при подключении или отключении монитора по HDMI (Xaver Hugl, Plasma 5.24.6)

[Актуальный список «15-минутных ошибок»](https://tinyurl.com/plasma-15-minute-bugs)

## Новые возможности

Теперь вы можете [изменить цветовую схему](https://invent.kde.org/graphics/okular/-/merge_requests/461), используемую Okular, независимо от системной цветовой схемы (George Florea Bănuș, Okular 22.08):

![0](https://pointieststick.files.wordpress.com/2022/05/color-scheme-in-okular.png)

Размер всплывающих окон виджетов на панели Plasma [теперь можно изменять](https://bugs.kde.org/show_bug.cgi?id=332512), как и у обычных окон; к тому же, они запоминают заданные вами размеры! (Luca Carlon, Plasma 5.26):

![2](https://pointieststick.files.wordpress.com/2022/05/tall-system-tray.png)

## Исправления ошибок и улучшения производительности

Удаление действий контекстного меню в диспетчере файлов Dolphin теперь [работает для действий, среди установленных файлов которых есть символические ссылки](https://bugs.kde.org/show_bug.cgi?id=452289) (Christian Hartmann, Dolphin 22.08)

Сеанс Plasma Wayland [больше не вылетает сразу после входа в систему](https://bugs.kde.org/show_bug.cgi?id=419492), когда у вас подключён внешний экран в режиме «Переключиться на внешний экран» (некто прекрасный, скорее всего, Влад, Xaver или Marco; Plasma 5.25)

Многострочные встроенные сообщения в программном обеспечении на базе QtQuick [больше не будут некорректно отображать свой текст](https://invent.kde.org/frameworks/kirigami/-/merge_requests/556) при определённых обстоятельствах (Ismael Asensio, Frameworks 5.95)

Окно загрузки новых цветовых схем в эмуляторе терминала Konsole снова [работает должным образом](https://bugs.kde.org/show_bug.cgi?id=452593) (David Edmundson и Alexander Lohnau, Frameworks 5.95, но дистрибутивы должны получить исправление раньше)

## Улучшения пользовательского интерфейса

Кнопка «Размонтировать» рядом со смонтированными дисками на панели «Точки входа» в Dolphin [больше не отображается](https://bugs.kde.org/show_bug.cgi?id=453890) для внутренних дисков, а также тех дисков, которые были вручную добавлены в файл `/etc/fstab` (Kai Uwe Broulik, Dolphin 22.08)

При использовании Spectacle для копирования снимка экрана в буфер обмена, появляющееся в результате уведомление [больше не сбивает с толку](https://bugs.kde.org/show_bug.cgi?id=454269), говоря о сохранении изображения (Felipe Kinoshita, Spectacle 22.08)

Когда установлена Okteta (шестнадцатеричный редактор от KDE), она [больше не открывается](https://invent.kde.org/utilities/ark/-/merge_requests/49) при предварительном просмотре с помощью архиватора Ark файлов, на самом деле не являющихся двоичными (Nicolas Fella, Ark 22.08)

Центр приложений Discover теперь [отображает более полезное сообщение об ошибке](https://invent.kde.org/plasma/discover/-/merge_requests/307), если вы запускаете его без каких-либо доступных в системе модулей для управления приложениями (Nate Graham, Plasma 5.25):

* ![4](https://pointieststick.files.wordpress.com/2022/05/other-distro-message.png)

*«Discover не может быть использован для установки приложений или обновления системы, так как не установлено ни одного модуля управления приложениями. Вы можете установить их в разделе „Отсутствующие модули“ страницы настроек. Сообщите об этой ситуации разработчикам используемого дистрибутива»*

Теперь вы можете [закрыть Discover](https://bugs.kde.org/show_bug.cgi?id=419622) во время установки, удаления или обновления программного обеспечения, и он превратится в уведомление о ходе выполнения (Aleix Pol Gonzalez, Plasma 5.26)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад. Также взгляните на [ошибки в бета-версии Plasma 5.25](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2023541&query_format=advanced&version=5.24.90&version=5.25.0&version=5.25.1&version=5.25.2&version=5.25.3&version=5.25.4&version=5.25.5), они тоже важны.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Андреев](https://vk.com/ilyancod)  
*Источник:* <https://pointieststick.com/2022/05/27/this-week-in-kde-resizable-plasma-panel-pop-ups>  

<!-- 💡: OSDs → экранные уведомления -->
<!-- 💡: Places panel → панель «Точки входа» -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
