# На этой неделе в KDE: наконец-то root-права в Dolphin

> 26 декабря – 2 января, основное — прим. переводчика

С Новым годом!

На этой неделе была добавлена последняя часть большого проекта длительностью почти в 5 лет: [поддержка Polkit в KIO](https://bugs.kde.org/show_bug.cgi?id=179678)! Она позволяет диспетчеру файлов Dolphin и другим приложениям KDE, использующим библиотеку KIO, создавать, перемещать, копировать, удалять (в том числе и в корзину) файлы в не принадлежащих пользователю расположениях! Это заняло много времени, но наконец-то получилось. Большое спасибо Janet Blackquill за то, что довела это до победного конца, и Chinmoy Ranjan Pradhan за то, что положил начало и продвинул проект очень далеко за прошедшие годы. Данный функционал появится в Frameworks 5.91 чуть позже, чем через месяц. До тех пор, пожалуйста, тестируйте его в [KDE Neon Unstable](https://neon.kde.org/download), [openSUSE Krypton](https://download.opensuse.org/repositories/KDE:/Medias/images/iso/) или в «нестабильных» пакетах KDE вашего любимого дистрибутива и [присылайте отчёты об ошибках в компонент `frameworks-kio`](https://bugs.kde.org/enter_bug.cgi?product=frameworks-kio), если что-нибудь работает неправильно!

## Другие новые возможности

Эмулятор терминала Konsole [теперь позволяет открывать текущий каталог (или любую другую папку, по которой щёлкнули правой кнопкой мыши) в любом приложении, а не только в диспетчере файлов](https://invent.kde.org/utilities/konsole/-/merge_requests/527) (Janet Blackquill, Konsole 22.04):

![0](https://pointieststick.files.wordpress.com/2021/12/open-with.png)

В строке поиска и запуска KRunner [теперь есть встроенная справка, которую можно активировать, щёлкнув по значку вопросительного знака в панели инструментов или набрав «?»](https://bugs.kde.org/show_bug.cgi?id=433636). В режиме справки нажатие на какой-либо модуль покажет все различные синтаксисы поиска для него! (Alexander Lohnau, Plasma 5.24)

* ![1](https://pointieststick.files.wordpress.com/2021/12/overview.png)

* ![2](https://pointieststick.files.wordpress.com/2021/12/details.png)

Пользователи модуля обоев рабочего стола «Изображение дня» [теперь могут получать изображения с сайта](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/87) <https://simonstalenhag.se>, полного классных и жутковатых научно-фантастических картинок (Алексей Андреев, Plasma 5.24)

## Wayland

[Диспетчер окон KWin теперь поддерживает глубину цвета более 8 бит](https://invent.kde.org/plasma/kwin/-/merge_requests/1641) в сеансе Plasma Wayland (Xaver Hugl, Plasma 5.24)

[Дополнительные параметры клавиатуры снова работают правильно](https://bugs.kde.org/show_bug.cgi?id=433265) в сеансе Wayland (Fabian Vogt, Plasma 5.23.5)

В сеансе Wayland [больше не происходит сбой Параметров системы, если позволить таймеру отмены в настройках экрана дойти до нуля](https://bugs.kde.org/show_bug.cgi?id=447199) (Méven Car, Plasma 5.24)

В сеансе Wayland восстановление свёрнутого или распахнутого окна [теперь делает то же, что и в X11](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/549): переключается на виртуальный рабочий стол, на котором окно находилось до восстановления, вместо того чтобы восстанавливать его на текущий рабочий стол (Alex Rosca, Plasma 5.24)

Поведение прокрутки в программном обеспечении Qt Quick [стало во многом лучше, например, скорость сенсорной панели теперь такая же, как в областях просмотра с прокруткой Qt Widgets, особенно в сеансе Wayland при использовании масштабирования экрана](https://invent.kde.org/frameworks/kirigami/-/merge_requests/415) (Noah Davis, Frameworks 5.90)

## Исправления ошибок и улучшения производительности

В просмотрщике изображений Gwenview [исправлен сбой, происходивший иногда при открытии файлов JPEG, если версия системной библиотеки `libexiv2` была старше 0.27.5](https://bugs.kde.org/show_bug.cgi?id=441121) (Lukáš Karas, Gwenview 21.12.1)

Когда раздел переформатируется с помощью Диспетчера разделов, [он больше не принадлежит по умолчанию пользователю root](https://bugs.kde.org/show_bug.cgi?id=407192) (Tomaz Canabrava и Andrius Štikonas, Диспетчер разделов 22.04)

Прозрачность системного лотка [теперь соответствует настройкам прозрачности родительской панели](https://bugs.kde.org/show_bug.cgi?id=439025) (Konrad Materka, Plasma 5.23.5)

Оформления окон со скруглёнными углами [избавились от визуальных искажений, связанных с прозрачностью или вращением, при использовании дробного коэффициента масштабирования, например, 125% или 150%](https://invent.kde.org/plasma/kwin/-/merge_requests/1805) (Julius Zint, Plasma 5.24)

Приложения, которые часто обновляют заголовок своего окна, [больше не приводят к зависанию Plasma или излишнему потреблению ресурсов процессора](https://bugs.kde.org/show_bug.cgi?id=414121) (Fushan Wen, Plasma 5.24)

## Улучшения пользовательского интерфейса

Многострочные названия файлов и папок в Dolphin теперь по умолчанию ограничены 3 строками, [и при наведении на них курсора можно увидеть всплывающую подсказку с полным текстом](https://invent.kde.org/system/dolphin/-/merge_requests/312) (Leo Treloar, Dolphin 22.04):

![3](https://pointieststick.files.wordpress.com/2021/12/tooltip.png)

В контекстном меню панели задач пункт «Запустить новый экземпляр» [был переименован в «Открыть новое окно» для ясности](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/133) и [больше не отображается для однооконных приложений](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1309) или приложений, [обеспечивающих собственные действия для открытия нового окна](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1312). Пункт «Дополнительно» [был перемещён вниз](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/769) (Nicolas Fella и Nate Graham, Plasma 5.24):

* ![4](https://pointieststick.files.wordpress.com/2021/12/dolphin.png)

* ![5](https://pointieststick.files.wordpress.com/2021/12/konsole.png)

В центре приложений Discover [теперь есть опция автоматической перезагрузки после завершения обновлений](https://bugs.kde.org/show_bug.cgi?id=419504), которая отображается внизу после начала процесса обновления (Aleix Pol Gonzalez, Plasma 5.24)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Сергей Гримальский](https://t.me/user32767)  
*Источник:* <https://pointieststick.com/2021/12/31/this-week-in-kde-finally-root-file-operations-in-dolphin/>  

<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: directory → каталог -->
<!-- 💡: file manager → диспетчер файлов -->
<!-- 💡: icon → значок -->
<!-- 💡: locations → расположения -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
