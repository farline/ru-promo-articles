# На этой неделе в KDE: Хотите стабильность? Их есть у меня!

> 18–25 сентября, основное — прим. переводчика

Нас всегда просят притормозить работу над новыми функциями и на какое-то время сосредоточиться на стабильности. Что ж, мы вас услышали и делаем именно это для Plasma 5.26; в частности, мы почти полностью сосредоточены на работе над ошибками в течение месячного периода бета-тестирования. Результаты радуют! Я думаю, что каждого читателя ждёт что-то приятное в разделе «Заметные исправления»! Возможно, и не одно. Самое время [сообщить об ошибках в бета-версии](https://kde.ru/bugs)! Их быстро исправят. Давайте все вместе сделаем Plasma 5.26 самой стабильной версией!

## Новые возможности

Видеоредактор Kdenlive переведён на использование меню-гамбургера, так что если вы отключите в нём обычную строку меню (которая остаётся видимой по умолчанию), то всё равно сохраните доступ к полной структуре меню (Julius Künzel, Kdenlive 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=358390))

Если на вашей клавиатуре есть клавиша «Калькулятор», нажатие на неё теперь запускает калькулятор KCalc (Paul Worrall, KCalc 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=353596))

## Улучшения пользовательского интерфейса

Виджеты «Проигрыватель» и «Уведомления» в системном лотке теперь группируются с системными службами, а не с индикаторами состояния приложений, поэтому больше не будут появляться вперемешку со значками ваших приложений (Nate Graham, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2159))

Снова можно переключать вкладки в меню запуска приложений сочетанием клавиш Ctrl+Tab, а теперь ещё и сочетаниями Ctrl+PageUp/PageDown и Ctrl+\[ или \] (Иван Ткаченко, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=440307))

Пометки, которые вы делаете на экране с помощью эффекта «Рисование мышью», теперь видны на снимках и видеозаписях экрана (Влад Загородний, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457639))

Всплывающие подсказки в Plasma и приложениях на основе QtQuick теперь плавно появляются и исчезают (Bharadwaj Raju, Frameworks 5.99. [Ссылка 1](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/604) и [ссылка 2](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/188))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Plasma больше не падает иногда в сеансе Wayland при перетаскивании из меню запуска приложений куда-либо элементов, которых нет на странице «Избранное» (Fushan Wen, Plasma 5.24.7. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1157))

На странице «Шрифты» Параметров системы отображаемые по умолчанию настройки субпиксельной отрисовки и степени хинтинга теперь соответствуют действительности, то есть настройкам вашего дистрибутива, вместо того чтобы всегда ошибочно показывать «RGB, лёгкий» (Harald Sitter, Plasma 5.24.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=416140))

Исправлен наиболее распространённый сбой Plasma, который мог происходить при использовании строки поиска и запуска KRunner (Arjen Hiemstra, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=414920))

Также исправлен второй по распространённости сбой Plasma, который мог случаться при перетаскивании виджетов из их проводника (Fushan Wen, последний выпуск набора правок Qt от KDE. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=446111))

Виджеты и значки на рабочем столе больше не перескакивают и не сбрасывают иногда свои расположения при входе в систему! (Marco Martin, Plasma 5.26. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=413645) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=454345))

Мы исправили серьёзную проблему с видеокартами NVIDIA, приводившую к визуальному повреждению различных элементов Plasma после выхода системы из спящего режима (David Edmundson и Андрей Бутырский, Plasma 5.26. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=365716), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=457284) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=456122))

Рабочий стол больше не проглядывает на мгновение после выхода из спящего режима и перед появлением экрана блокировки (Xaver Hugl, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=316734))

В сеансе Plasma Wayland перетаскивание файлов в окно браузера Firefox снова работает как надо (Влад Загородний, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=445661))

Восстановление размера распахнутого окна при использовании плавающей панели больше не оставляет странную висящую тень (Влад Загородний, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=455270))

В подменю «Добавить панель» контекстного меню рабочего стола больше не отображаются сломанные элементы «Пустой группирующий виджет» и «Пустой системный лоток» (Marco Martin, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=454416))

В сеансе Plasma Wayland у тех из вас, кто использует последнюю версию Frameworks и Plasma 5.25.5, уведомления и всплывающие окна виджетов снова располагаются в нужных местах (Xaver Hugl, Frameworks 5.99. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=459188))

По углам плавающих панелей, диалоговых и всплывающих окон Plasma больше нет печально известных точек и других визуальных искажений 🙂 (Niccolò Venerandi, Frameworks 5.99. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=438644))

Исправлена ещё одна причина, по которой некоторые области прокрутки в приложениях на основе библиотеки Kirigami при наличии последней версии набора правок Qt от KDE могли отображать ненужную горизонтальную полосу прокрутки (Marco Martin, Kirigami 5.99. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=458486))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 11 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 17). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2159030&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Остались 44 15-минутные ошибки в Plasma (на прошлой неделе было 45). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2159029&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 190 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. Это много! [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-09-16&chfieldto=2022-09-23&chfieldvalue=RESOLVED&list_id=2155516&query_format=advanced&resolution=FIXED)

Стоит отметить, что на этой неделе мы настроили нашего бота для Bugzilla, чтобы он автоматически помечал ошибки как 15-минутные или высокоприоритетные и сортировщикам отчётов не приходилось делать это вручную. В результате общее число ошибок на этой неделе росло по мере того, как бот переклассифицировал старые отчёты. Так что тот факт, что нам всё равно удалось опередить этот рост и снизить общее число ошибок, да ещё и в период бета-тестирования Plasma, когда люди присылают больше отчётов, чем обычно, меня очень впечатляет!

## Как вы можете помочь

Если вы разработчик, помогите исправить [ошибки в бета-версии Plasma 5.26](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2159064&query_format=advanced&version=5.25.90)! Давайте опустошим этот список до итогового выпуска!

Кроме того, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/09/23/this-week-in-kde-yo-dawg-i-heard-you-wanted-stability/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
