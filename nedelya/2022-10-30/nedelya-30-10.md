# На этой неделе в KDE: на рубеже поколений

По мере того как мы приближаемся к завершению ветки Plasma 5, многие задумываются о том, что ждёт Plasma 6, помимо собственно перехода на Qt 6. Мы сходимся на том, чтобы избежать больших архитектурных изменений; большинство крупных изменений будут связаны с улучшением пользовательского интерфейса и добавлением новых возможностей. Поэтому [команда дизайна (KDE VDG)](https://community.kde.org/Get_Involved/Design) была занята планированием на будущее, что привело к множеству доработок для последней и лучшей версии Plasma 5!

## Новые возможности

Если вам не нравится недавно изменённое поведение табличного режима просмотра в диспетчере файлов Dolphin, когда элементы можно выбирать или открывать щелчком в пустом месте их строк, теперь вы можете переключиться на старое поведение в настройках (Felix Ernst, Dolphin 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=453700)):

![0](https://pointieststick.files.wordpress.com/2022/10/image-12.png)

В центре приложений Discover изменено оформление домашней страницы: теперь она показывает динамически обновляемые категории с популярными приложениями, а набор рекомендуемых приложений KDE был обновлён (Aleix Pol Gonzalez, Carl Schwan, Nate Graham и Devin Lin, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/discover/-/merge_requests/398), [ссылка 2](https://invent.kde.org/websites/autoconfig-kde-org/-/merge_requests/15), [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=431316) и [ссылка 4](https://invent.kde.org/frameworks/kirigami/-/merge_requests/808)):

![1](https://pointieststick.files.wordpress.com/2022/10/new-discover-homepage-1.jpg)

Теперь вы можете щёлкнуть колёсиком мыши по значку «Сетевые подключения», чтобы включить или выключить режим полёта (Nate Graham, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/166)):

![2](https://pointieststick.files.wordpress.com/2022/10/middle-click-for-airplane-mode-toggle.png)

В выпадающих меню строки адреса Dolphin теперь содержатся скрытые папки, если их показ в основной области просмотра включён (Евгений Попов, Frameworks 5.100. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/825)):

![3](https://pointieststick.files.wordpress.com/2022/10/image-11.png)

## Улучшения пользовательского интерфейса

На страницах приложения Информация о системе, заполненных моноширинным текстом, текст теперь можно выделять (а заодно и копировать), и он больше не выходит за видимую область с правой стороны окна (Иван Ткаченко, Plasma 5.26.2. [Ссылка](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/117))

В сеансе Plasma X11 диалоги портала XDG, отображаемые приложениями Flatpak, больше не используют неправильные темы и цвета (Harald Sitter, Plasma 5.26.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=458865))

Вокруг окон с оформлением Breeze появилась тонкая обводка, что не только чертовски стильно выглядит, но и помогает разграничивать окна с тёмной цветовой схемой (Akseli Lahtinen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/breeze/-/merge_requests/241)):

*![4](https://pointieststick.files.wordpress.com/2022/10/dark.jpg)
*![5](https://pointieststick.files.wordpress.com/2022/10/light.jpg)

Плавающие панели теперь «приземляются», когда какое-либо окно касается их, а используемые при этом отступы стали меньше и выглядят не так странно. Кроме того, всплывающие окна виджетов теперь касаются края плавающей панели (Niccolò Venerandi, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1206) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2226))

Текст вверху нового диалогового окна выбора приложения («Открыть с помощью…»), предоставляемого порталом XDG, стал лаконичнее (Nate Graham, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/132) и [ссылка 2](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2255)):

![6](https://pointieststick.files.wordpress.com/2022/10/image-10.png)

Модуль «Последние файлы» строки поиска и запуска KRunner теперь ищет по подстрокам, а не только префиксам (Natalie Clarius, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461006))

Теперь при желании можно сделать окно стандартного меню запуска приложений меньше, чем по умолчанию (Niccolò Venerandi, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460544))

Отступы и выравнивание заголовков страниц Параметров системы, основанных на QtWidgets и Kirigami, теперь одинаковые, так что при переключении между ними больше нет неожиданных отличий (Ismael Asensio, Plasma 5.27. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=460103) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=460542))

В программном обеспечении KDE значительно улучшен внешний вид списковых представлений и заголовков разделов списков (Devin Lin, Frameworks 5.100. [Ссылка 1](https://invent.kde.org/frameworks/kirigami/-/merge_requests/779) и [ссылка 2](https://invent.kde.org/frameworks/kitemviews/-/merge_requests/19)):

![7](https://pointieststick.files.wordpress.com/2022/10/dope-af.jpg)

Всплывающие окна виджетов на панели теперь отображаются по центру содержащей их панели в случаях, когда это не отрывает их от своих значков (Niccolò Venerandi, Frameworks 5.100. [Ссылка](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/624)):

![8](https://invent.kde.org/frameworks/plasma-framework/uploads/82b668e35bc35899ce37507218d6b4bb/image.png)

В диалоговых окнах, где вы можете безвозвратно удалить файлы, кнопка удаления теперь подписана «Удалить безвозвратно», чтобы вы точно-точно понимали, во что ввязываетесь (Guilherme Marçal Silva, Frameworks 5.100. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/1009))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

В сеансе Plasma Wayland режим ускорения курсора «Плоский» теперь работает как надо (John Brooks, Plasma 5.26.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=444510))

В сеансе Plasma Wayland при касании адресной строки Firefox с сенсорного экрана виртуальная клавиатура теперь появляется всегда, как и ожидалось, и не приходится сначала переключаться на другое приложение (Xaver Hugl и Xuetian Weng, Plasma 5.26.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460537))

Исправлен один из самых распространённых сбоев при использовании зашифрованных папок Plasma (David Edmundson, Plasma 5.26.3. [Ссылка](https://invent.kde.org/plasma/plasma-vault/-/merge_requests/23))

Исправлена недавняя проблема, из-за которой было сложно нажать на кнопку закрытия распахнутого окна щелчком по пикселю в самом углу (Arjen Hiemstra, Plasma 5.26.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460686))

Исправлено недавнее ухудшение в сеансе X11, из-за которого содержимое распахнутых окон не обновлялось должным образом при использовании масштабирования (Xaver Hugl, Plasma 5.26.3. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461032))

В сеансе Plasma Wayland перетаскивание чего-либо в окне Firefox больше не приводит к тому, что курсор застревает в состоянии «хватающей руки», пока вы не перетащите вкладку (Влад Загородний, Plasma 5.26.3. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/3110))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Известно об 11 ошибках в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 56 (на прошлой неделе было 55). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 144 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-10-21&chfieldto=2022-10-28&chfieldvalue=RESOLVED&list_id=2191453&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Добавлен тест на правильное отображение значков веб-приложений Chromium на панели задач, чтобы оно больше не ломалось (Nicolas Fella, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2265))

Добавлен тест сопоставления панелей и контейнеров с экранами в Plasma, и теперь мы можем начать искоренять проблемы в нём, ничего при этом не ухудшая (Marco Martin, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2231))

Мы завели новую вики-страницу, на которой приведены некоторые способы зарабатывать на жизнь, работая над проектами KDE (Nate Graham. [Ссылка](https://community.kde.org/Make_A_Living))

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/10/28/this-week-in-kde-next-generation-improvements/>  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: task manager → панель задач -->
