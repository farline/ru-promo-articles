# На этой неделе в KDE: большой улов

> 11–18 сентября, основное — прим. переводчика

На этой неделе мы, так сказать, вдарили по полной. Мы выпустили бета-версию Plasma 5.26, устранили огромное число заметных ошибок, добавили новые функции и улучшили пользовательский интерфейс во всей Plasma!

## Новые возможности

Архиватор Ark переведён на использование меню-гамбургера для более чистого пользовательского интерфейса по умолчанию (Андрей Бутырский, Ark 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=444664)):

![0](https://pointieststick.files.wordpress.com/2022/09/ark-hamburger-menu-1.jpg)

Не совсем новая функция, скорее, оживлённая, но всё же… Снова можно выбрать показ флага вместе с кодом языка на индикаторе раскладки клавиатуры (Nate Graham, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=444864)):

![1](https://pointieststick.files.wordpress.com/2022/09/flag_and_code.jpg)

Теперь в контекстное меню рабочего стола при желании можно добавить пункт «Открыть терминал» (Neal Gompa, Daniel Vrátil, Jan Grulich, Marc Deop и Rex Dieter, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=451217)):

![2](https://pointieststick.files.wordpress.com/2022/09/open-in-terminal-menu-item.jpg)

В приложении Информация о системе теперь есть страница с техническими данными о диспетчере окон KWin, которые могут пригодиться при создании отчётов об ошибках (Nate Graham, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/111)):

![3](https://pointieststick.files.wordpress.com/2022/09/window-manager.jpg)

## Улучшения пользовательского интерфейса

Когда открыта виртуальная клавиатура, на панели задач теперь всегда есть кнопка, чтобы закрыть её, даже если не включён сенсорный режим (Nate Graham, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1493))

Теперь всплывающие окна уведомлений можно закрыть, щёлкнув по ним колёсиком мыши (Kai Uwe Broulik, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=456243))

Проводником виджетов Plasma, всплывающим окном «Взаимозаменяемые виджеты» и всеми виджетами Plasma, в которых есть списки с раскрывающимися элементами, теперь можно полностью управлять с помощью клавиш со стрелками (Fushan Wen, Plasma 5.26. [Ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1135), [ссылка 2](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1136) и [ссылка 3](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/573))

Теперь можно использовать сочетания клавиш Ctrl+Alt+[клавиши со стрелками], чтобы менять порядок элементов в меню запуска приложений, в виджете «Панель запуска» и на панели задач (Fushan Wen, Plasma 5.26. [Ссылка 1](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1137), [ссылка 2](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/231) и [ссылка 3](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1138#note_524304))

Виджеты «Сетевые подключения» и «Bluetooth» теперь отображают соответствующие им действия в своих контекстных меню для более быстрого доступа (Oliver Beard, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/149) и [ссылка 2](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/149)):

*![4](https://pointieststick.files.wordpress.com/2022/09/networks.jpg)
*![5](https://pointieststick.files.wordpress.com/2022/09/bluetooth.jpg)

При использовании функции «Цвет выделения: В соответствии с выбранными обоями» определяемый системой цвет теперь должен выглядеть значительно приятнее и лучше отражать самый заметный цвет на изображении (Fushan Wen, Plasma 5.26 с Frameworks 5.99. [Ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/731))

Обособленные ссылки в приложениях на основе библиотеки Kirigami теперь всегда подчёркнуты, чтобы вам было легче определить, что это ссылки (Nate Graham, Frameworks 5.99. [Ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/744))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

При использовании видеокарты NVIDIA в сеансе Plasma Wayland меню запуска приложений снова всегда появляется при щелчке по его значку на панели (Влад Загородний, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=455575))

Исправлена анимация перетаскивания окон в эффекте «Все рабочие столы» (Иван Ткаченко, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=453995))

Когда эффекты «Обзор», «Все окна» и «Все рабочие столы» вызываются перемещением указателя мыши в угол экрана, продолжение движения курсора в направлении угла, когда эффект уже открыт, больше не приводит к его немедленному закрытию (Marco Martin, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457280))

Прокрутка на рабочем столе для переключения виртуальных рабочих столов теперь всегда работает (Arjen Hiemstra, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=419878))

Несмотря на то, что мы ещё не до конца искоренили проблему с теряющимися и перемешивающимися рабочими столами и панелями Plasma, теперь панели должны, по крайней мере, реже теряться (Marco Martin, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2125))

Экраны с одинаковыми именами снова можно различить на соответствующей странице Параметров системы и в функции «Определение выходов» (Иван Ткаченко, Plasma 5.26. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=450344) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=419494))

В сеансе Plasma Wayland теперь соблюдаются настройки задержки и частоты повтора ввода с клавиатуры (Влад Загородний, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=408943))

Было внесено несколько исправлений, повышающих шансы на успешный автозапуск приложений при включённом запуске Plasma с помощью Systemd: сама Systemd теперь более терпима к незначительным проблемам в `.desktop`-файлах приложений из автозапуска, а Редактор меню KDE и диалоговое окно свойств усложняют создание некорректных ярлыков (David Edmundson, Plasma 5.26 с Frameworks 5.99 и systemd 252. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=455252), [ссылка 2](https://github.com/systemd/systemd/pull/24656), [ссылка 3](https://github.com/systemd/systemd/pull/24658), [ссылка 4](https://invent.kde.org/plasma/kmenuedit/-/merge_requests/10) и [ссылка 5](https://invent.kde.org/frameworks/kio/-/merge_requests/976))

В сеансе X11 приложения KDE теперь правильно запоминают размеры и положение своих окон в многомониторных конфигурациях (Richard Bízik, Frameworks 5.99. [Ссылка](https://invent.kde.org/frameworks/kconfig/-/merge_requests/129))

Прокрутка списков в диалоговых окнах из библиотеки Kirigami с помощью сенсорной панели теперь в целом должна быть куда менее дёрганой (Marco Martin, Frameworks 5.99. [Ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/738))

Другие сведения об ошибках, которые могут вас заинтересовать:

* 15 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 20). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 46 15-минутных ошибок в Plasma (как и на прошлой неделе, но это потому, что в список было добавлено несколько новых взамен исправленных). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 143 ошибки исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-09-09&chfieldto=2022-09-16&chfieldvalue=RESOLVED&list_id=2152747&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/09/16/this-week-in-kde-its-a-big-one-folks/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: KMenuEdit → Редактор меню KDE -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: window manager → диспетчер окон -->
