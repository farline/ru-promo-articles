# На этой неделе в KDE: изобилие улучшений интерфейса

На этой неделе мы подготовили множество улучшений пользовательского интерфейса. Если вы не найдёте в этой статье ничего, что вам понравится, я съем свою шляпу!

## Новые возможности

Страница «Брандмауэр» Параметров системы теперь поддерживает указание маски подсети в строке IP-адреса, например, `192.168.1.0/24` (Daniel Vrátil, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=434317))

## Улучшения пользовательского интерфейса

На вкладке «Сведения» боковой панели просмотрщика изображений Gwenview теперь можно уменьшить область, занимаемую разделом метаданных и описания, перетаскиванием разделителя между ним и разделом «Информация об изображении» над ним. Положение разделителя запоминается! (Corbin Schwimmbeck, Gwenview 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=332921))

![0](https://pointieststick.files.wordpress.com/2022/10/image-7.png)

Утилита для создания снимков экрана Spectacle теперь по умолчанию запоминает последнюю выбранную прямоугольную область — даже между запусками приложения. Это поведение, конечно же, настраивается (Bharadwaj Raju, Spectacle 22.12. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/156))

Экран приветствия в текстовых редакторах Kate и KWrite (по-прежнему необязательный и навсегда отключаемый флажком на нём самом) теперь содержит ссылки на сайт и руководство пользователя (Евгений Попов, Kate и KWrite 22.12. [Ссылка](https://invent.kde.org/utilities/kate/-/merge_requests/957)):

![1](https://pointieststick.files.wordpress.com/2022/10/image-6.png)

В сеансе Plasma Wayland при открытии диспетчера файлов Dolphin из всплывающего окна «Диски и устройства» теперь используется и выводится на передний план существующее окно Dolphin, если оно есть (Nicolas Fella, Dolphin 22.12 с Plasma 5.26.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460268))

Перетаскивание окон в виджете «Переключение рабочих столов» теперь работает намного лучше, без дёрганий (Niccolò Venerandi, Plasma 5.26.1. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1211))

В сеансе Plasma теперь используется версия диалогового окна «Открыть файл с помощью…» из портала XDG для улучшения визуальной согласованности и удобства использования. Старый диалог по-прежнему задействуется, когда вы вызываете функцию «Открыть файл во внешнем приложении» из приложения KDE, запущенного вне Plasma (Harald Sitter, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-integration/-/merge_requests/47)):

![2](https://pointieststick.files.wordpress.com/2022/10/image-5.png)

Виджеты в системном лотке, по которым можно щёлкнуть средней кнопкой мыши для включения или выключения чего-либо, теперь упоминают эту возможность в своих всплывающих подсказках (Nate Graham, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2205), [ссылка 2](https://invent.kde.org/plasma/bluedevil/-/merge_requests/101) и [ссылка 3](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/143)):

*![3](https://pointieststick.files.wordpress.com/2022/10/middle-click_to_mute.png)
*![4](https://pointieststick.files.wordpress.com/2022/10/middle_click_to_enable_bluetooth.jpg)
*![5](https://pointieststick.files.wordpress.com/2022/10/middle-click_to_exit_dnd_mode.jpg)
*![6](https://pointieststick.files.wordpress.com/2022/10/middle-click_to_play_media_player.jpg)

В сеансе Plasma Wayland Центр справки теперь может выводить на передний план своё открытое окно при вызове другим приложением (Nicolas Fella, Центр справки 5.27. [Ссылка](https://invent.kde.org/system/khelpcenter/-/merge_requests/21))

Диалог настройки размера значков в Параметрах системы был существенно переработан: из него были удалены ни на что не влияющие параметры, а оставшиеся стали понятнее (Nate Graham, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2214)):

![7](https://pointieststick.files.wordpress.com/2022/10/icons-popup.png)

При изменении размера всплывающего окна календаря Plasma текст в самом календаре теперь увеличивается и уменьшается соответствующим образом (Fushan Wen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2204)):

![8](https://pointieststick.files.wordpress.com/2022/10/image-9.png)

Эффекты «Обзор», «Все окна» и «Все рабочие столы» стали усерднее пытаться плотно замостить экран окнами, так что, надеюсь, вы больше не столкнётесь с тем, что окна расположены как ступеньки (Влад Загородний, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/3054))

При просмотре страницы приложения Flatpak, помеченного разработчиком как более не поддерживаемое, центр приложений Discover теперь сообщает вам указанную автором причину (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/378))

Тени за часами и датой на экранах входа в систему и блокировки стали чуть мягче и красивее (Bharadwaj Raju, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2196)):

![9](https://pointieststick.files.wordpress.com/2022/10/nicer-lockscreen-shadows.png)

Виджет «Состояние фиксации режимов клавиш» теперь показывает значок, отличный от виджета «Раскладка клавиатуры», так что их легко отличить (Bharadwaj Raju, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460357)):

![10](https://pointieststick.files.wordpress.com/2022/10/image-4.png)

В сеансе Plasma Wayland индикатор «Ваш экран записывается» в системном лотке теперь использует более подходящий по смыслу значок (Nate Graham, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460514)):

![11](https://pointieststick.files.wordpress.com/2022/10/after.jpg)

При попытке удалить в корзину объект, размер которого превышает установленный размер корзины, вам теперь будет предложено удалить его безвозвратно (Ahmad Samir, Dolphin 22.12 с Frameworks 5.100. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=431351)):

![12](https://pointieststick.files.wordpress.com/2022/10/image-8.png)

Изображения аватаров пользователей в программном обеспечении KDE теперь выглядят чётче и красивее на экранах с высокой плотностью пикселей и включённым масштабированием (Fushan Wen, Frameworks 5.100. [Ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/771))

Списки последних открытых файлов в соответствующем пункте меню в программном обеспечении KDE теперь отображают значки файлов (Eric Armbruster, Frameworks 5.100. [Ссылка](https://invent.kde.org/frameworks/kconfigwidgets/-/merge_requests/163))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Исправлена ещё одна причина, по которой виджеты на рабочем столе слегка перемещались при входе в систему — по-видимому, причин было несколько (Aaron Rainbolt, Plasma 5.24.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460618))

Исправлена ошибка, из-за которой новая функция переназначения кнопок мыши не срабатывала (David Redondo, Plasma 5.26.2 с Frameworks 5.100. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460345))

Plasma больше не тратит много ресурсов процессора при использовании анимированных изображений AVIF в качестве обоев (Fushan Wen, Frameworks 5.100. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460085))

Plasma больше не тратит много ресурсов процессора при отключении вставки средней кнопкой мыши и копировании определённого содержимого (David Edmundson, Frameworks 5.100. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460248))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Известно об 11 ошибках в Plasma с очень высоким приоритетом (на прошлой неделе было 10). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 55 (на прошлой неделе было 53). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 144 ошибки было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-10-14&chfieldto=2022-10-21&chfieldvalue=RESOLVED&list_id=2184176&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Для обсуждения этой инициативы теперь есть чат в Matrix! Найдите `#kde-institutional-memory:kde.org` в любимом клиенте Matrix или [щёлкните здесь](https://webchat.kde.org/#/room/#kde-institutional-memory:kde.org), чтобы открыть его в веб-клиенте.

У нас также есть новая страница команды на [invent.kde.org](https://invent.kde.org)! Вы можете присоединиться [здесь](https://invent.kde.org/groups/teams/automation/-/group_members). Мы будем использовать это для долгосрочной координации через [средство отслеживания задач](https://invent.kde.org/teams/automation/issues/-/issues).

Появилась вики-страница, объясняющая, как дистрибутивы могут опакетить Plasma для лучшего пользовательского опыта, полная накопленных знаний (Nate Graham. [Ссылка](https://community.kde.org/Distributions/Packaging_Recommendations))

В дистрибутивах, использующих GDB 12, мастер отправки отчётов о сбоях DrKonqi теперь может автоматически загружать отладочные символы для трассировки, что делает отчёты полезными и сокращает работу по их сортировке (Harald Sitter, Plasma 5.26.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=454063))

Добавлен автоматический тест для средства извлечения метаданных из изображений в формате PNG (Kai Uwe Broulik, Frameworks 5.100. [Ссылка](https://invent.kde.org/frameworks/kfilemetadata/-/merge_requests/71/diffs))

Добавлены автоматические тесты для кода платформы DRM (Direct Rendering Manager) диспетчера окон KWin (Xaver Hugl, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/1271))

## Изменения вне KDE, затрагивающие KDE

Субпиксельный хинтинг RGB при отрисовке текста теперь включён по умолчанию, что хорошо для пользователей программного обеспечения KDE, поскольку мы уважаем общесистемные настройки по умолчанию, а не переопределяем их собственными значениями (Akira Tagoh, Fontconfig 2.14.1 [Ссылка](https://gitlab.freedesktop.org/fontconfig/fontconfig/-/merge_requests/237))

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/10/21/this-week-in-kde-ui-improvements-abound-2/>  

<!-- 💡: Help Center → Центр справки -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
