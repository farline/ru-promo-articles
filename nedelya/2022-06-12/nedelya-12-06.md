# На этой неделе в KDE: часы меняют цвет

Потому что это, разумеется, главное событие недели, ведь так?!

> 5–12 июня, основное — прим. переводчика

В любом случае состоялся большой выпуск Plasma 5.25! Поэтому, параллельно с исправлением ошибок в 5.25, началась работа над новшествами для 5.26.

## Исправленные «15-минутные ошибки»

Число ошибок на момент публикации: **64, вместо бывших 65**. 2 оказались уже исправленными (спасибо тем, кто это сделал) и 1 добавилась.

[Актуальный список ошибок](https://tinyurl.com/plasma-15-minute-bugs)

## Новые возможности

Plasma [получила поддержку установки разных изображений в качестве обоев рабочего стола при использовании светлой и тёмной цветовых схем](https://bugs.kde.org/show_bug.cgi?id=207976)! В будущих выпусках Plasma будет поставляться с тёмной и светлой версиями обоев (Fushan Wen, Plasma 5.26):

<https://i.imgur.com/sA17uVc.mp4?_=1>

## Улучшения пользовательского интерфейса

Теперь архиватор Ark [проверяет наличие свободного места перед началом распаковки](https://bugs.kde.org/show_bug.cgi?id=393959) (Tomaz Canabrava, Ark 22.08)

Страницы Параметров системы [больше не будут отображаться так низко в списке результатов при поиске через любую строку поиска, использующую KRunner](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1811) (Alexander Lohnau, Plasma 5.25):

![1](https://pointieststick.files.wordpress.com/2022/06/mouse-on-top.png)

Добавлена возможность перетаскивания окон между экранами в эффектах [«Обзор»](https://bugs.kde.org/show_bug.cgi?id=448566) и [«Все окна»](https://bugs.kde.org/show_bug.cgi?id=283333) (Marco Martin, Plasma 5.25)

Прокрутка над значком виджета проигрывателя [теперь изменяет громкость приложения с шагом 5%, а не 3%](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1810), что соответствует шагу по умолчанию при изменении громкости всей системы. Как и для громкости системы, размер этого шага теперь настраиваемый! (Oliver Beard, Plasma 5.26)

Кнопки, оформленные в стиле Breeze, [больше не имеют градиента, когда они не под курсором](https://invent.kde.org/plasma/breeze/-/merge_requests/220#note_465153), благодаря чему они стали выглядеть немного легче и больше выделяться на фоне страницы (Кто-то, кто предпочёл остаться анонимным, Plasma 5.26):

![2](https://pointieststick.files.wordpress.com/2022/06/lighter-flatter-buttons.png)

Диалоговое окно «Комбинации клавиш», которое можно увидеть во многих приложениях и в Параметрах системы, [больше не будет отображать пустые столбцы для глобальных или локальных комбинаций клавиш, если приложение их не устанавливает](https://bugs.kde.org/show_bug.cgi?id=427129) (Ahmad Samir, Frameworks 5.95):

![3](https://pointieststick.files.wordpress.com/2022/06/elisa-shortcut-dialog.png)

Деления и цифры на часах с циферблатом [теперь соответствуют выбранному вами цвету выделения](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/543) (Ismael Asensio, Frameworks 5.95):

![4](https://pointieststick.files.wordpress.com/2022/06/volna-accented-clock.png)

…И, в дополнение, теперь вся лицевая панель этих часов [также соответствует вашей цветовой схеме](https://bugs.kde.org/show_bug.cgi?id=377935)! (Ismael Asensio, Frameworks 5.96):

![5](https://pointieststick.files.wordpress.com/2022/06/black-clock.png)

Анимация смены обоев [стала выполняться с учётом глобальных настроек скорости анимации](https://invent.kde.org/plasma/plasma-workspace/-/commit/8df97ad32394500af9e1195048e5121a1b9c9adc) (Fushan Wen, Plasma 5.26)

В программах, основанных на QtQuick, переход между представлениями теперь [учитывает глобальную настройку скорости анимации](https://bugs.kde.org/show_bug.cgi?id=395324) (Fushan Wen, Frameworks 5.96)

## Исправления ошибок и улучшения производительности

Миниатюры для папок [снова создаются вовремя, а не только после перехода в них](https://bugs.kde.org/show_bug.cgi?id=453497) (Martin T. H. Sandsmark, Dolphin 22.08)

Автоматическое монтирование [снова отключено по умолчанию, как и ожидалось](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/974) (Ismael Asensio, Plasma 5.25)

Темы экрана входа в систему (SDDM), полученные с помощью диалога «Загрузить \[что-то\]», [теперь будут появляться в списке мгновенно, без необходимости закрывать и снова открывать страницу «Экран входа» Параметров системы](https://bugs.kde.org/show_bug.cgi?id=454884) (Alexander Lohnau, Plasma 5.24.6)

Автоматическое определение сенсорного режима [теперь игнорирует виртуальные устройства ввода и не больше будет блокироваться, если запущено приложение, которое их создаёт](https://invent.kde.org/plasma/kwin/-/merge_requests/2494) (Александр Волков, Plasma 5.24.6)

Многократное монтирование и размонтирование диска [больше не приводит в некоторых случаях к накоплению пустых элементов в списке действий виджета «Диски и устройства»](https://bugs.kde.org/show_bug.cgi?id=449778) (Иван Ткаченко, Plasma 5.25)

В целях повышения безопасности [в полях ввода пароля в Plasma больше нельзя вернуть удалённый текст](https://bugs.kde.org/show_bug.cgi?id=453828) (Derek Christ, Frameworks 5.95 и Plasma 5.26)

Когда вы используете действие «Перейти вверх» в Dolphin, [папка, в которой вы только что были, снова выделяется](https://bugs.kde.org/show_bug.cgi?id=453289) (Jan Blackquill, Frameworks 5.95)

Диалоговое окно сочетаний клавиш [снова отображает точную информацию о конфликтах](https://bugs.kde.org/show_bug.cgi?id=454704) (Ahmad Samir, Frameworks 5.96)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Тимофей Москвин](https://t.me/alionapermes/)  
*Источник:* <https://pointieststick.com/2022/06/10/this-week-in-kde-the-analog-clock-changes-color/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: thumbnail → миниатюра -->
