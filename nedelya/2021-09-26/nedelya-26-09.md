# На этой неделе в KDE: Plasma на верном пути

Бета-тестирование Plasma 5.23 почти закончено, и мы заняты исправлением ошибок, обнаруженных нашими замечательными пользователями. Следует отметить, что я не упоминаю здесь исправления для недочётов, которые никогда не попадали в финальные версии, в том числе и исправления относительно бета-версий. Если бы я говорил и о них, список ниже был бы намного длиннее! Так что будьте уверены, мы исправили [полным-полно ошибок и ухудшений](https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&list_id=1913019&query_format=advanced&resolution=FIXED&version=5.22.90), найденных нашими верными тестировщиками в ходе бета-тестирования. Все эти отчёты об ошибках очень ценные. Так что, пожалуйста, [продолжайте присылать их](https://kde.ru/bugs)! Система учёта ошибок — не бездна!

> 19–25 сентября, основное — прим. переводчика

## Новые возможности

Теперь вы можете [выбрать произвольный собственный цвет акцента на странице «Цвета» Параметров системы](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1031) (Tanbir Jishan, Plasma 5.24):

![0](https://pointieststick.files.wordpress.com/2021/09/screenshot_20210924_115048.png)

В сеансе Plasma Wayland [диспетчер окон KWin теперь поддерживает «аренду DRM»](https://invent.kde.org/plasma/kwin/-/merge_requests/662), что позволяет нам возобновить поддержку VR-гарнитур и достичь оптимальной производительности (Xaver Hugl, Plasma 5.24)

KWin [теперь позволяет вам дополнительно установить глобальное сочетание клавиш для перемещения окна в центр экрана](https://invent.kde.org/plasma/kwin/-/merge_requests/1409) (Kristen McWilliam, Plasma 5.24)

В диалоговом окне открытия файлов [теперь есть пункт контекстного меню для открытия выбранного файла в другом внешнем приложении](https://bugs.kde.org/show_bug.cgi?id=440748) — на случай, если вам нужно предварительно просмотреть его перед открытием в запросившем приложении, а миниатюры в диалоговом окне недостаточно (Ahmad Samir, Frameworks 5.87)

## Wayland

Исправлен [сбой в KWin, из-за которого мог сломаться весь сеанс Plasma Wayland](https://bugs.kde.org/show_bug.cgi?id=437709) (Влад Загородний, Plasma 5.23)

Установка изолированных приложений Snap в сеансе Wayland [больше никогда не приводит к сбою KWin](https://bugs.kde.org/show_bug.cgi?id=442677) (Влад Загородний, Plasma 5.23)

В сеансе Wayland [курсор больше не становится невидимым после выключения и последующего включения экрана](https://bugs.kde.org/show_bug.cgi?id=438824) (Xaver Hugl, Plasma 5.23)

В сеансе Wayland текст, скопированный из GTK-приложения, [теперь можно вставить в другие приложения после закрытия GTK-приложения](https://bugs.kde.org/show_bug.cgi?id=424649) (David Edmundson, Plasma 5.23)

В сеансе Wayland [края экрана теперь правильно определяются в многоэкранных конфигурациях с автоматическим скрытием панелей](https://bugs.kde.org/show_bug.cgi?id=441400) (Lewis Lakerink, Plasma 5.23)

В сеансе Wayland [теперь можно вводить с клавиатуры числа в двунаправленный счётчик, используемый для задания толщины панели](https://bugs.kde.org/show_bug.cgi?id=442557) (David Edmundson, Plasma 5.23)

## Исправления ошибок и улучшения производительности

Dolphin [больше не остаётся висеть в фоновом режиме после сжатия файлов через контекстное меню и последующего выхода из приложения](https://bugs.kde.org/show_bug.cgi?id=441813) (Андрей Бутырский, Ark 21.08.2)

Конфигурация баланса громкости на странице «Звуковые устройства» Параметров системы [снова работает](https://bugs.kde.org/show_bug.cgi?id=439751) (Nicolas Fella, Plasma 5.23)

Опция «Автоматически монтировать только те носители, которые были ранее монтированы вручную» на странице «Внешние носители» Параметров системы [теперь работает](https://bugs.kde.org/show_bug.cgi?id=432026) (Méven Car, Plasma 5.24)

Звук запуска (если вы его включили) [теперь воспроизводится должным образом при использовании systemd для инициализации сеанса Plasma](https://bugs.kde.org/show_bug.cgi?id=433490) (Henri Chain, Plasma 5.24)

Discover [стал быстрее проверять наличие обновлений](https://invent.kde.org/frameworks/attica/-/merge_requests/15) (Aleix Pol Gonzalez, Frameworks 5.87)

Файлы, скопированные с помощью приложений KDE, [теперь полностью учитывают системное значение `umask` и поэтому создаются в целевой папке с правильными разрешениями](https://bugs.kde.org/show_bug.cgi?id=404777) (Ahmad Samir, Frameworks 5.87)

## Улучшения пользовательского интерфейса

Skanlite [теперь запоминает последний использованный сканер](https://bugs.kde.org/show_bug.cgi?id=435030) (Alexander Stippich, Skanlite 21.12)

В Konsole [отныне только одна опция для управления видимостью строки меню, и она всегда работает](https://bugs.kde.org/show_bug.cgi?id=440328), а не две опции в разных местах, которые конфликтуют друг с другом (Евгений Попов, Konsole 21.12)

Okular [теперь отображает осмысленное сообщение, когда просит вас ввести имя автора пометки](https://bugs.kde.org/show_bug.cgi?id=442381) (Albert Astals Cid, Okular 12.12)

Проблемы с обновлением в Discover, [вызванные ошибками в пакетах дистрибутива, теперь приводят к отображению диалогового окна с большой кнопкой «Сообщить об этой проблеме», которая отправляет вас прямо в систему отслеживания ошибок вашего дистрибутива](https://bugs.kde.org/show_bug.cgi?id=442785) (Nate Graham, Plasma 5.24):

![1](https://pointieststick.files.wordpress.com/2021/09/nice-new-button.png)

*Сообщение об ошибке на этом снимке выдумано, ведь я использую дистрибутив, в котором не бывает проблем с обновлением 😎*

Цветовая схема «Breeze, высококонтрастный вариант» [была удалена](https://invent.kde.org/plasma/breeze/-/merge_requests/141), поскольку на самом деле предлагала более низкий контраст, чем наиболее похожая цветовая схема «Breeze, тёмный вариант». Существующие пользователи будут переведены на «Breeze, тёмный вариант» (Nate Graham, Plasma 5.24)

Цветовая схема Breeze [была переименована в «Breeze, классический вариант»](https://invent.kde.org/plasma/breeze/-/merge_requests/145), чтобы лучше отличать её от цветовых схем «Breeze, светлый вариант» и «Breeze, тёмный вариант» (Nate Graham, Plasma 5.24)

Имена пользователей под изображениями аватаров на экранах входа, блокировки и выхода [были немного увеличены, чтобы лучше соответствовать размеру аватаров](https://bugs.kde.org/show_bug.cgi?id=442650) (Nate Graham, Plasma 5.24):

![2](https://pointieststick.files.wordpress.com/2021/09/bigger-username-text-in-lock-screen.png)

Текст заголовка на панелях инструментов приложений Kirigami [теперь меньше и чуть лучше соответствует масштабу всего вокруг](https://invent.kde.org/frameworks/kirigami/-/merge_requests/311) (Devin Lin, Frameworks 5.87):

* ![3](https://pointieststick.files.wordpress.com/2021/09/discover.png)

* ![4](https://pointieststick.files.wordpress.com/2021/09/system-settings.png)

* ![5](https://pointieststick.files.wordpress.com/2021/09/sytem-monitor.png)

## Как вы можете помочь

Если вы опытный разработчик или даже новичок, поработайте над [исправлением этих ухудшений в Plasma 5.23](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=REOPENED&bug_status=NEEDSINFO&keywords=regression&keywords_type=allwords&list_id=1913519&query_format=advanced&version=5.22.90). Если вы не являетесь разработчиком и можете воспроизвести любую из ошибок, ещё не помеченных как CONFIRMED («ПОДТВЕРЖДЕНА»), прокомментируйте её и отметьте как CONFIRMED. У нас есть две недели, чтобы исправить эти ухудшения, а также все остальные, о которых люди постоянно сообщают, и мы рады любой помощи!

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Максим Маршев](https://t.me/msmarshev)  
*Источник:* <https://pointieststick.com/2021/09/25/this-week-in-kde-plasma-on-the-move/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: bug tracker → система отслеживания ошибок -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
