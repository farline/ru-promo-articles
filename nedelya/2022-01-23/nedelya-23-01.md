# На этой неделе в KDE: стабильность — получите и распишитесь!

Всю эту неделю мы были очень заняты исправлением всевозможных ошибок:

* ошибок, относящихся к инициативе [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs);
* ухудшений работы в бета-версии Plasma 5.24 (которые не упомянуты здесь, поскольку так и не были выпущены, а их могло быть столько, что просто голова кругом!);
* других ошибок, не относящихся к вышеуказанным.

Думаю, каждый найдёт здесь то, что ему понравится! Так что давайте посмотрим:

> 16–23 января, основное — прим. переводчика

## Исправленные «15-минутные ошибки»

Текущее число ошибок: **89, из бывших 99**. [Актуальный список ошибок](https://tinyurl.com/plasma-15-minute-bugs)

Некоторые из них недавно были исправлены, и эти исправления попадут в следующий выпуск, а некоторые были вызваны проблемами в компонентах-зависимостях или зависимых продуктах (многие уже решены для следующего выпуска). Вот исправления в коде KDE на этой неделе:

В сеансе Plasma X11 опции касания двумя пальцами на странице «Сенсорная панель» Параметров системы [теперь отображаются корректно](https://bugs.kde.org/show_bug.cgi?id=447105) (Arjen Hiemstra, Plasma 5.24)

В сеансе Plasma Wayland бумажник KWallet [теперь автоматически разблокируется, если правильно настроен](https://bugs.kde.org/show_bug.cgi?id=448479) (David Edmundson, Plasma 5.24)

При использовании модуля PAM `pam_deny`, который временно блокирует вход в систему после определённого числа неудачных попыток ввода пароля, [экран блокировки теперь сообщает вам об этом, вместо того чтобы оставлять вас в недоумении, почему ваш пароль не принимается](https://bugs.kde.org/show_bug.cgi?id=428613) (David Edmundson, Plasma 5.24)

[Флажки](https://bugs.kde.org/show_bug.cgi?id=446303) и [панели вкладок Plasma](https://bugs.kde.org/show_bug.cgi?id=445899) снова реагируют на касания сенсорного экрана (Arjen Hiemstra, Frameworks 5.91)

## Новые возможности

Вы [теперь можете управлять макетами Plasma, расположенными на других экранах, из центрального расположения](https://bugs.kde.org/show_bug.cgi?id=447044)! Это позволяет вам перемещать рабочие столы и панели между экранами или восстанавливать рабочие столы и панели на отключённых в данный момент экранах. Доступно из глобальной панели инструментов режима редактирования (Cyril Rossi и Marco Martin, Plasma 5.25):

![0](https://pointieststick.files.wordpress.com/2022/01/containment-management-window-1.png)

## Wayland

В сеансе Wayland исправлены [различные сценарии, при которых диспетчер окон KWin мог аварийно завершиться при подключении внешних экранов](https://invent.kde.org/plasma/kwin/-/merge_requests/1905#note_379652) (Xaver Hugl, Plasma 5.24)

KWin в сеансе Wayland [больше не завершается аварийно из-за отключения внешнего монитора при использовании режима «Переключиться на внешний экран»](https://bugs.kde.org/show_bug.cgi?id=448454) (Xaver Hugl, Plasma 5.24)

В сеансе Wayland [исправлено серьёзное ухудшение производительности, у некоторых пользователей вызывавшее задержку ввода и сильную нагрузку на процессор](https://bugs.kde.org/show_bug.cgi?id=448557) (Влад Загородний, Plasma 5.24)

В сеансе Wayland эффекты «Рисование мышью» и «Анимация щелчка мышью» [теперь работают со стилусом](https://bugs.kde.org/show_bug.cgi?id=426584) (Aleix Pol Gonzalez, Plasma 5.25)

В набор правок для платформы Qt было включено очень важное изменение, [которое делает сеанс Plasma Wayland куда работоспособнее для пользователей видеокарт NVIDIA с драйвером серии 495+](https://invent.kde.org/qt/qt/qtwayland/-/merge_requests/24) (Elvis Lee и Adrien Faveraux, как только ваш дистрибутив обновит свой набор правок от KDE)

Ещё одно важное изменение было включено в набор правок для Qt, [предотвращая аварийное завершение работы Plasma в сеансе Wayland при отключении и повторном включении внешнего экрана](https://bugs.kde.org/show_bug.cgi?id=438839) (David Edmundson и Fabian Vogt, как только ваш дистрибутив обновит свой набор правок от KDE)

## Другие исправления ошибок и улучшения производительности

Приложения KDE в формате Flatpak [теперь мгновенно реагируют на глобальные изменения цветовых схем, наборов значков, размеров шрифтов и так далее](https://invent.kde.org/packaging/flatpak-kde-runtime/-/merge_requests/73) (Aleix Pol Gonzalez, при перевыпуске содержащей изменения версии 21.08 среды выполнения KDE для Flatpak)

Всплывающее окно системного лотка на нижней панели [больше не испытывает проблем с отрисовкой в области заголовка, когда вы нажимаете кнопку «Назад» в виджете, у которого есть свой собственный заголовок](https://bugs.kde.org/show_bug.cgi?id=438178) (Nate Graham, Plasma 5.24)

Функция центра приложений Discover, отображающая зависимости пакетов для приложений, входящих в состав дистрибутива, [снова работает](https://bugs.kde.org/show_bug.cgi?id=414822) (Aleix Pol Gonzalez, Plasma 5.24)

Discover [теперь показывает точные размеры приложений и дополнений для Plasma](https://bugs.kde.org/show_bug.cgi?id=432846) (Aleix Pol Gonzalez, Plasma 5.24)

Результаты поиска в меню запуска приложений, в строке поиска KRunner и других местах, где для поиска используется KRunner, [больше не мигают и не мерцают, когда вы вводите больше символов для уточнения результатов поиска](https://bugs.kde.org/show_bug.cgi?id=423161) (Eduardo Cruz, Plasma 5.25)

Немного [снижена нагрузка на процессор и память всего программного обеспечения KDE при загрузке значков](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/51) (Nicolas Fella, Frameworks 5.91)

## Улучшения пользовательского интерфейса

Теперь вы можете [прокручивать колесо мыши над панелью вкладок Plasma для переключения между вкладками](https://bugreports.qt.io/browse/QTBUG-99619) (Noah Davis, Qt 6.3, но включается в набор правок от KDE)

В режиме просмотра «Таблица» диспетчера файлов Dolphin подсветка [теперь распространяется на всю строку](https://bugs.kde.org/show_bug.cgi?id=181437) (Tom Lin, Dolphin 22.04):

![1](https://pointieststick.files.wordpress.com/2022/01/full-row-highlight.png)

Discover [больше не показывает вам кнопку «Запустить» на страницах тех вещей, которые не могут быть запущены, вроде расширений или обоев рабочего стола](https://bugs.kde.org/show_bug.cgi?id=429060) (Aleix Pol Gonzalez, Plasma 5.24)

Диалог выбора папки, который вы видите, когда приложение Flatpak просит вас выбрать папку, [теперь выглядит и ведёт себя точно так же, как и диалог, отображаемый в таких случаях приложением, входящим в состав дистрибутива](https://bugs.kde.org/show_bug.cgi?id=437505) (Fabian Vogt, Plasma 5.24)

Диалоги запроса разрешений для приложений Flatpak теперь [выглядят немного красивее и ближе к стилю KDE](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1380) и [предварительно выбирают единственный пункт из списка в тех случаях, когда вариант только один](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/67) (Nate Graham, Plasma 5.24):

![2](https://pointieststick.files.wordpress.com/2022/01/obs-permission.png)

Виджет «Буфер обмена» получил некоторые исправления, касающиеся клавиатуры при фокусе в поле поиска: [стрелки вверх и вниз теперь перемещают по списку](https://bugs.kde.org/show_bug.cgi?id=448811); нажатие клавиши Delete при выделенном тексте [теперь удаляет его](https://bugs.kde.org/show_bug.cgi?id=448738), а когда текст не выбран, клавиша Delete [теперь не делает ничего, кроме удаления выделенного элемента истории](https://bugs.kde.org/show_bug.cgi?id=448739) (Fushan Wen, Plasma 5.24 и 5.25)

Когда вы размонтируете диск, который всё ещё выполняет операции по передаче файлов (ввиду [использования ядром Linux асинхронного файлового ввода-вывода](https://bugzilla.kernel.org/show_bug.cgi?id=41472)), виджет «Подключаемые устройства» [теперь показывает более подходящее сообщение: «Не отключайте оборудование, выполняется передача файлов…»](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1367) (Nate Graham, Plasma 5.25):

![3](https://pointieststick.files.wordpress.com/2022/01/biggoat-transfer.png)

Различные приложения и виджеты Plasma, у которых есть поля поиска с фокусом по умолчанию, [больше не фокусируются по умолчанию на них при включённом режиме планшета, чтобы предотвратить немедленное появление экранной клавиатуры, загораживающей приложение в момент запуска](https://invent.kde.org/frameworks/kirigami/-/merge_requests/398) (Arjen Hiemstra, Frameworks 5.91 и Plasma 5.25)

Счётчики уведомлений в панели задач [теперь используют новый стиль подсветки](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/805) (Jan Blackquill и Nate Graham, Plasma 5.25):

![4](https://pointieststick.files.wordpress.com/2022/01/new-task-manager-badge-highlight-style.png)

Элементы меню в приложениях, основанных на QtQuick, с оформлением Breeze [стали больше и удобнее для нажатия](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/119) в режиме планшета (Nate Graham, Frameworks 5.91):

![5](https://pointieststick.files.wordpress.com/2022/01/tablet-mode.png)

*ЛЮДЯМ, КОТОРЫЕ ТЕРПЕТЬ НЕ МОГУТ ОТСТУПЫ: Это только в режиме планшета! Только в режиме планшета! Не в обычном режиме! Вам никогда не придётся видеть это снижение плотности! Так что не жалуйтесь по этому поводу! 🙂*

Текстовый редактор Kate, среда разработки KDevelop и другие приложения, основанные на KTextEditor, [теперь автоматически определяют стиль пробельных символов в открытых вами файлах](https://bugs.kde.org/show_bug.cgi?id=109338). Теперь вы никогда больше не столкнётесь с ситуацией, когда при открытии файла с символами табуляции вместо пробелов вы нажимаете Tab, а вставляется пробел, и вы замечаете, что испортили все пробельные символы, только когда выполняете `git diff` для ваших изменений (Waqar Ahmed, Frameworks 5.91)

Функция «Закомментировать или раскомментировать» в Kate или других основанных на KTextEditor приложениях [теперь работает правильно, когда строка, которую вы пытаетесь закомментировать или раскомментировать, содержит встроенный комментарий](https://bugs.kde.org/show_bug.cgi?id=426981) (Waqar Ahmed, Frameworks 5.91)

Выпадающие списки в приложениях KDE, основанных на QtQuick, [теперь достаточно длинные, чтобы вместить текст длинных элементов](https://bugs.kde.org/show_bug.cgi?id=403153) (Alexander Stippich, Frameworks 5.91)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу новую инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Сергей Гримальский](https://t.me/user32767)  
*Источник:* <https://pointieststick.com/2022/01/21/this-week-in-kde-you-wanted-stability-heres-some-stability/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: downstream → зависимый продукт -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: location → расположение -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: upstream → [компонент-]зависимость | исходный репозиторий -->
