# На этой неделе в KDE: о, как много всего

Пока мы работаем над ошибками в Plasma 5.24 (и, как можно узнать ниже, многое исправили), мы также начали работать над улучшениями для Plasma 5.25 и приложений KDE! Смотрите!

> 13–20 февраля, основное — прим. переводчика

## Исправленные «15-минутные ошибки»

Текущее число ошибок: **82, из бывших 83**. [Актуальный список ошибок](https://tinyurl.com/plasma-15-minute-bugs)

Снова [можно запускать приложения со страниц «Последние» и «Часто используемые» в меню запуска приложений](https://bugs.kde.org/show_bug.cgi?id=449348) (Олег Соловьёв, Plasma 5.24.1)

Установка предела на зарядку аккумулятора ноутбука [больше не приводит к тому, что значок «Батарея и яркость» системного лотка остаётся видимым, когда ядро решает, что батарея полностью заряжена](https://bugs.kde.org/show_bug.cgi?id=435931) (Nate Graham, Plasma 5.24.1)

## Новые возможности

Текстовый редактор Kate [теперь обладает интерактивной основанной на путях панелью навигации, которая показывает иерархию каталогов открытого в данный момент документа и позволяет переключиться на другой](https://invent.kde.org/utilities/kate/-/merge_requests/597) (Waqar Ahmed, Kate 22.04):

![0](https://pointieststick.files.wordpress.com/2022/02/kate-navigation-bar.png)

Цветовые схемы [теперь можно настроить так, чтобы цвет выделения применялся к заголовкам окон или даже ко всей заголовочной области](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1304), и [цветовая схема «Breeze, классический вариант» использует эту возможность](https://invent.kde.org/plasma/breeze/-/merge_requests/182) (Dominic Hayes, Plasma 5.25):

* ![1](https://pointieststick.files.wordpress.com/2022/02/accented-titlebar.png)
* ![2](https://pointieststick.files.wordpress.com/2022/02/accented-tools-area.png)

Обратите внимание, что внешний вид этого большого синего заголовка не используется по умолчанию в цветовой схеме «Breeze, светлый вариант»; вам нужно включить новую опцию «Применить цвет выделения к заголовку», чтобы она использовала цвет выделения таким образом. Однако, как можно видеть, это возможно!

## Wayland

В сеансе Wayland диспетчер окон KWin [больше не завершается аварийно случайным образом, когда вы перетаскиваете что-то из браузера Firefox, работающего в режиме XWayland, на рабочий стол](https://bugs.kde.org/show_bug.cgi?id=449644) (David Redondo, Plasma 5.24.1)

Касание стилусом поля ввода [вновь заставляет виртуальную клавиатуру появляться как положено](https://bugs.kde.org/show_bug.cgi?id=449888) (Aleix Pol Gonzalez, Plasma 5.24.1)

Когда нативное приложение Wayland запускается с распахнутым окном, кнопка «Распахнуть/восстановить» на заголовке этого окна [теперь показывает правильное состояние](https://bugs.kde.org/show_bug.cgi?id=450053) (Влад Загородний, Plasma 5.24.1)

## Исправления ошибок и улучшения производительности

Главная область просмотра в диспетчере файлов Dolphin [больше не искажает анимацию при масштабировании](https://bugs.kde.org/show_bug.cgi?id=449179) (Евгений Попов, Dolphin 22.04)

Нажатие правой кнопкой мыши по значкам приложений старой спецификации в системном лотке для вызова контекстного меню [снова работает, если оказалось, что у вас не установлена библиотека `libappindicator`](https://bugs.kde.org/show_bug.cgi?id=449870) (Claudio Holo, Plasma 5.24.1)

Plasma [теперь позволяет настроить максимальный уровень заряда для устройств, которые поддерживают установку максимального уровня заряда, но не минимального](https://bugs.kde.org/show_bug.cgi?id=449997) (Méven Car, Plasma 5.24.1)

Приложения, основанные на библиотеке Kirigami и использующие боковые шторки, [больше не поглощают события мыши у боковых краёв окна, что, в частности, означает, что полосы прокрутки с правого края окон работают корректно](https://bugs.kde.org/show_bug.cgi?id=438017) (Tranter Madi, Frameworks 5.92)

Устранена [утечка памяти в приложениях KDE, использующих библиотеку Solid](https://invent.kde.org/frameworks/solid/-/merge_requests/77) (Méven Car, Frameworks 5.92)

## Улучшения пользовательского интерфейса

В строку состояния просмотрщика изображений Gwenview [вернулась кнопка «Вместить»](https://bugs.kde.org/show_bug.cgi?id=441447) (Felix Ernst, Gwenview 22.04):

![3](https://pointieststick.files.wordpress.com/2022/02/fit-button-in-gwenview.png)

Уведомления, которые посылает архиватор Ark, когда завершено сжатие, [теперь красивее и полезнее](https://invent.kde.org/utilities/ark/-/merge_requests/99) (Nicolas Fella, Ark 22.04):

![4](https://pointieststick.files.wordpress.com/2022/02/fancier-and-more-useful-ark-notifications.png)

Слой затенения в средстве создания снимков экрана Spectacle в режиме прямоугольной области [теперь темнее](https://invent.kde.org/graphics/spectacle/-/merge_requests/126) (Nate Graham, Spectacle 22.04):

![5](https://pointieststick.files.wordpress.com/2022/02/darker-overlay-in-spectacle-rectangular-region-mode.png)

Списки из карточек в приложениях, основанных на библиотеке GTK, с темой Breeze [теперь выглядят намного лучше](https://bugs.kde.org/show_bug.cgi?id=439878) (Jan Blackquill, Plasma 5.25)

Индикаторы громкости звука и чувствительности микрофона [теперь выглядят намного лучше](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/112) (Light Yagami, Plasma 5.25):

![6](https://pointieststick.files.wordpress.com/2022/02/audio-levels-look-better-now.png)

Страница настройки брандмауэра в Параметрах системы [теперь по умолчанию использует упрощённый интерфейс добавления правил, так что вам не надо знать номера портов и вот это вот всё, но если вы хотите, можно открыть все эти утомительные продвинутые элементы управления](https://invent.kde.org/plasma/plasma-firewall/-/merge_requests/37) (Lucas Biaggi и Nate Graham, Plasma 5.25):

![7](https://pointieststick.files.wordpress.com/2022/02/simpler-filrewall-rule-dialog.png)

У меню в стиле Breeze в приложениях, основанных на библиотеках Qt и GTK, [появились небольшие поля](https://bugs.kde.org/show_bug.cgi?id=445886), что не только выглядит симпатично, но и исправляет [древнюю ошибку, из-за которой было слишком просто случайно активировать верхний пункт меню](https://bugs.kde.org/show_bug.cgi?id=374311) (Jan Blackquill, Plasma 5.25):

![8](https://pointieststick.files.wordpress.com/2022/02/outer-menu-margins.png)

В приложениях Qt меню в стиле Breeze, которые больше высоты экрана (боже мой), [теперь прокручиваются вертикально вместо того, чтобы разворачиваться в несколько столбцов](https://bugs.kde.org/show_bug.cgi?id=54716) (Jan Blackquill, Plasma 5.25). Этому отчёту об ошибке столько лет, что ему продадут спиртное во многих странах!

Контекстные меню элементов панели задач [теперь можно вызвать долгим касанием, что делает их доступнее на устройствах с сенсорными экранами](https://bugs.kde.org/show_bug.cgi?id=445901) (Nate Graham, Plasma 5.25):

![9](https://pointieststick.files.wordpress.com/2022/02/fat-menu-1.png)

*И пункты меню становятся высокими, чтобы было проще на них нажимать!*

Уведомление, показываемое при попытке воспользоваться VPN, для которого требуется отсутствующее в системе ПО, [теперь более полезное и понятное и остаётся на экране, пока не будет явно скрыто](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/102) (Nicolas Fella, Plasma 5.25)

Чтобы сделать [панель команд](https://vk.com/@kde_ru-nedelya-23-05) заметнее, в меню «Справка» каждого приложения KDE [добавлен пункт «Найти действие» для вызова этой возможности](https://bugs.kde.org/show_bug.cgi?id=442099) (Waqar Ahmed, Frameworks 5.92):

![10](https://pointieststick.files.wordpress.com/2022/02/find-action-menu-item-1.png)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* Пётр Шкенев  
*Источник:* <https://pointieststick.com/2022/02/18/this-week-in-kde-oh-so-many-things/>  

<!-- 💡: KCommandBar → панель команд [в приложениях KDE] (Ctrl+Alt+I) -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
