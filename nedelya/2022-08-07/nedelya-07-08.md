# На этой неделе в KDE: упрощаем настройку Samba

> 31 июля – 7 августа, основное — прим. переводчика

На этой неделе мы продолжили разбираться с ошибками, и теперь я буду упоминать «очень высокоприоритетные» ошибки в Plasma в дополнение к «15-минутным ошибкам», у которых несколько более низкий приоритет. Разработчикам Plasma, которые это читают, я рекомендую уделять первоочередное внимание именно ошибкам с очень высоким приоритетом. 🙂

## Исправленные высокоприоритетные ошибки в Plasma

Текущее число ошибок: **26, вместо бывших 29**. 3 исправлены:

Виджеты Системного монитора [больше не сбрасывают различные настройки до значений по умолчанию после перезапуска системы](https://bugs.kde.org/show_bug.cgi?id=454004) (Alexander Lohnau, Plasma 5.25.5)

Снова можно [выбирать окна на других экранах с помощью эффектов «Все окна» и «Обзор»](https://bugs.kde.org/show_bug.cgi?id=455783) (Marco Martin, Plasma 5.26)

Виджеты меню запуска приложений, которые вы хотите вызывать нажатием клавиши Meta (Win), [больше не требуют назначения на них комбинации клавиш Alt+F1](https://bugs.kde.org/show_bug.cgi?id=391322), что должно значительно снизить число случаев, когда вы нажимаете клавишу Meta, но ничего не происходит. Кроме того, если у вас несколько меню запуска приложений, которые хотят открыться по нажатию Meta, [теперь будет вызвано меню с активного экрана](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1937) (David Redondo, Plasma 5.26)

[Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)

## Исправленные «15-минутные ошибки» в Plasma

Текущее число ошибок: **50, вместо бывших 51**. 1 новая и 2 исправлены:

Стандартное меню запуска приложений [больше не выделяет непонятно зачем результат выполненного поиска в той же позиции, в которой вы что-либо выбрали мышью при прошлом поиске, вместо того чтобы просто выделить первый](https://bugs.kde.org/show_bug.cgi?id=454349) (Nate Graham, Plasma 5.25.5)

Курсор мыши, наведённый на элемент в стандартном меню запуска приложений, [больше не приводит к постоянному перевыбору этого элемента, если вы используете клавиатуру, чтобы выбрать что-то другое](https://bugs.kde.org/show_bug.cgi?id=455674) (Nate Graham, Plasma 5.25.5)

[Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)

## Новые возможности

Теперь вы можете настроить комбинацию клавиш для [активации эффекта «Все окна» с показом только окон из текущего приложения, находящихся на текущем рабочем столе](https://bugs.kde.org/show_bug.cgi?id=413342) (Arjen Hiemstra, Plasma 5.26)

## Улучшения пользовательского интерфейса

Стало гораздо проще выполнять начальную настройку папки для общего доступа через Samba, поскольку [мастер теперь выдаёт подробные сообщения об ошибках, объясняющие, что делать](https://bugs.kde.org/show_bug.cgi?id=425202) (Nate Graham, kdenetwork-filesharing 22.12):

* ![0](https://pointieststick.files.wordpress.com/2022/08/1_folder_doesn_t_exist_error.jpeg)

*Например, такие: «Этой папкой нельзя поделиться, так как каталог `/var/lib/samba/usershares` не существует. Эта ошибка вызвана отсутствием должной настройки Samba в вашем дистрибутиве. Вы можете исправить её самостоятельно, создав недостающий каталог и заново открыв это окно»*

* ![1](https://pointieststick.files.wordpress.com/2022/08/2_wrong_group_name.jpeg)

*…или такие: «Этой папкой нельзя поделиться, так как в качестве группы-владельца каталога `/var/lib/samba/usershares`» указан „root“, что неправильно. Эта ошибка вызвана отсутствием должной настройки Samba в вашем дистрибутиве. Вы можете исправить её самостоятельно, сменив группу-владельца указанного каталога на `usershares` и добавив себя в эту группу. После этого потребуется перезагрузка». В будущем на всех подобных страницах появится кнопка «Исправить»*

Виджет «Таймер» [претерпел значительные изменения, благодаря которым было исправлено большинство ошибок и появилась кнопка запуска/остановки таймера](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/211) (Fushan Wen, Plasma 5.26):

![2](https://pointieststick.files.wordpress.com/2022/08/timer-button-on-panel.jpeg)

Поиск виджетов в списке установленных [теперь также сопоставляет ваш запрос с ключевыми словами, предоставляя ещё один способ найти то, что вы ищете](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1960) (Alexander Lohnau, Plasma 5.26)

Комбинации клавиш в контекстных меню [теперь отображаются приглушённым серым цветом, что делает их менее заметными по сравнению с названиями пунктов меню](https://bugs.kde.org/show_bug.cgi?id=456254) (Jan Blackquill, Plasma 5.26):

![3](https://pointieststick.files.wordpress.com/2022/08/de-emphasized-shortcuts.jpeg)

В наборе значков Breeze [теперь есть значки для динамически подключаемых библиотек Windows (DLL)](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/217) (Alexander Wilms, Frameworks 5.97)

## Другие исправления и улучшения производительности

В просмотрщике документов Okular [больше нельзя утаскивать всплывающие заметки за пределы экрана](https://bugs.kde.org/show_bug.cgi?id=423363) (Nikola Nikolic, Okular 22.12)

Строка поиска и запуска KRunner при первом открытии [больше не выезжает из ниоткуда; теперь она всегда выезжает вниз от верхнего края экрана, как и ожидалось](https://bugs.kde.org/show_bug.cgi?id=447096) (Kai Uwe Broulik, Plasma 5.24.7)

В сеансе Plasma Wayland [мониторы больше не теряют иногда свои имена и возможность быть выбранными в качестве основного](https://bugs.kde.org/show_bug.cgi?id=449285) (Xaver Hugl, Plasma 5.24.7)

Стиль Breeze [снова учитывает размер маленьких значков, выбранный в Параметрах системы](https://bugs.kde.org/show_bug.cgi?id=455513) (Александр Керножицкий, Plasma 5.25.5)

Служба обработки экранов KScreen [теперь снисходительнее относится к определению экранов как уникальных](https://invent.kde.org/plasma/kscreen/-/merge_requests/123), что должно исправить разные странные проблемы с расположением экранов и рабочих столов при горячем подключении экранов и доков, когда меняются и идентификатор экрана, и идентификатор разъёма (Harald Sitter, Plasma 5.26)

Установка языка в Параметрах системы [теперь также обновляет стандартизированное значение `org.freedesktop.Accounts.User.Language`](https://bugs.kde.org/show_bug.cgi?id=411305), используемое приложениями Flatpak и в целом многими сторонними приложениями для определения предпочитаемого вами языка (Han Young, Plasma 5.26)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/08/05/this-week-in-kde-easier-samba-sharing-setup/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
