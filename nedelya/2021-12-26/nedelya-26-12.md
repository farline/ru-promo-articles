# На этой неделе в KDE: обзор принтеров Samba и другое

> 19–26 декабря, основное — прим. переводчика

С наступающим Новым годом! 🎅 Мы со своей стороны дарим подарки всем, кто хорошо вёл себя в уходящем году:

## Новые возможности

Добавлен [обзор принтеров Samba](https://bugs.kde.org/show_bug.cgi?id=368305)! (Harald Sitter, print-manager 22.04):

![0](https://pointieststick.files.wordpress.com/2021/12/samba-printer-browsing.png)

*Предупреждая вопросы: да, интерфейс староват. В конечном итоге он будет заменён версией на Qt Quick наряду с оставшимися старыми разделами Параметров системы.*

Эффекты переключения окон «Карусель» и «Перелистывание» [вернулись](https://bugs.kde.org/show_bug.cgi?id=443757), и теперь они реализованы на языке QML для лучшей расширяемости в будущем! (Ismael Asensio, Plasma 5.24)

![2](https://pointieststick.files.wordpress.com/2021/12/qml_flipswitch.png)

*Следующим будет «Куб рабочих столов» — надеюсь, в Plasma 5.25!*

## Исправления ошибок и улучшения производительности

Окно эмулятора терминала Yakuake [стало появляться быстрее](https://invent.kde.org/utilities/yakuake/-/merge_requests/51) (Jan Blackquill, Yakuake 21.12.1)

В сеансе Plasma Wayland Yakuake [больше не перекрывается верхней панелью](https://bugs.kde.org/show_bug.cgi?id=408468) (Tranter Madi, Yakuake 22.04)

Диспетчер разделов [больше не повторяет до бесконечности запрос прав при его отклонении](https://bugs.kde.org/show_bug.cgi?id=428974), а вместо этого показывает сообщение с описанием проблемы и кнопкой повтора (Alessio Bonfiglio, Диспетчер разделов 22.04):

![1](https://pointieststick.files.wordpress.com/2021/12/message.png)

Исправлена [утечка памяти в уведомлениях](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1303) (David Edmundson, Plasma 5.18.9)

Всплывающее окно календаря в виджете «Цифровые часы» [теперь всегда правильно раскрашивается при использовании оформления рабочего стола «Breeze, светлый вариант» или любого другого с жёстко прописанными светлыми цветами](https://bugs.kde.org/show_bug.cgi?id=446991) (Noah Davis, Plasma 5.23.5)

Plasma [теперь завершается быстрее, так как перестаёт принимать новые подключения вместе с началом процесса завершения, что особенно заметно в связке со службой синхронизации KDE Connect](https://bugs.kde.org/show_bug.cgi?id=432643) (Tomasz Lemeich, Plasma 5.24)

Диалог изменения ярлыков на рабочем столе [стал показывать правильную информацию в нужных местах](https://invent.kde.org/frameworks/kio/-/merge_requests/572) (Алексей Никифоров, Frameworks 5.90)

## Улучшения пользовательского интерфейса

Пункт контекстного меню рабочего стола [«Открыть в Dolphin»](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1237) был по умолчанию заменён на [«Настроить параметры экрана»](https://bugs.kde.org/show_bug.cgi?id=355679) (Ezike Ebuka и Nate Graham, Plasma 5.24):

![3](https://pointieststick.files.wordpress.com/2021/12/configure-display-settings.png)

*И не забывайте, что это меню настраивается (Настроить рабочий стол и обои > Действия мыши > кнопка настойки справа от ПКМ), так что вы можете убрать оттуда действия, которые никогда не используете.*

Теперь в режиме редактирования Plasma [можно перемещать панели за любое место плашки с опциями, а не только за маленькую кнопку (что, в общем-то, куда понятнее, особенно с соответствующей подсказкой)](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/764) (Björn Feber, Plasma 5.24):

![4](https://pointieststick.files.wordpress.com/2021/12/drag-to-move.png)

Страницы Параметров системы, представляющие собой большую сетку или список, теперь [используют современный безрамочный стиль](https://invent.kde.org/frameworks/kdeclarative/-/merge_requests/91) (Nate Graham, Frameworks 5.90)

* ![6](https://pointieststick.files.wordpress.com/2021/12/cursors.png)

* ![7](https://pointieststick.files.wordpress.com/2021/12/background_services.png)

Кнопки панели инструментов, при нажатии с удержанием которых появляется меню, теперь также [показывают это меню по щелчку правой кнопкой мыши](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/86) (Kai Uwe Broulik, Frameworks 5.90)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2021/12/25/this-week-in-kde-samba-printer-browsing-and-more/>  

<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Settings → Параметры системы -->
