# На этой неделе в KDE: множество улучшений производительности

> 23–30 мая, основное — прим. переводчика

## Новые возможности

По умолчанию добавлена глобальная комбинация клавиш Meta+Ctrl+PrintScreen [для создания снимка окна, находящегося в данный момент под курсором](https://bugs.kde.org/show_bug.cgi?id=435083) (Antonio Prcela, Spectacle 21.08)

Gwenview теперь [может читать](https://invent.kde.org/graphics/gwenview/-/merge_requests/68) информацию о встроенных цветовых профилях для форматов изображений, отличных от JPEG и PNG (Daniel Novomeský, Gwenview 21.08)

Если на вашей клавиатуре отсутствует клавиша «Отключить микрофон», теперь [вы можете отключить микрофон с помощью сочетания клавиш Meta+Отключить звук](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/69) (David Edmundson, Plasma 5.22)

В сеансе Plasma Wayland системный лоток [теперь уведомляет вас, когда что-то записывает экран](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/38), и даёт вам возможность прекратить запись (Aleix Pol Gonzalez, Plasma 5.23)

Строка поиска по содержимому меню KCommandBar [была добавлена во все приложения KDE, использующие KXMLGui](https://invent.kde.org/frameworks/kxmlgui/-/merge_requests/54), поэтому вы можете начать использовать её, нажав Ctrl+Alt+I в Dolphin, Gwenview, Okular, Konsole, Krita, Kdenlive и т. д.! (Waqar Ahmed, Frameworks 5.83)

## Исправления и улучшения производительности

Значительно повышена надёжность сеанса Plasma Wayland [для пользователей видеокарт Nvidia с проприетарным драйвером](https://bugs.kde.org/show_bug.cgi?id=437573) (David Edmundson, Plasma 5.22)

Spectacle [теперь намного быстрее и надёжнее в сеансе Plasma Wayland](https://bugs.kde.org/show_bug.cgi?id=437652) (Влад Загородний, Spectacle 21.04.2)

В сеансе Plasma Wayland отключение внешнего экрана [больше не вызывает сбой всех открытых приложений Qt](https://bugs.kde.org/show_bug.cgi?id=426293) (Влад Загородний, Plasma 5.22)

Новый Системный монитор Plasma [теперь отображает точную информацию об использовании дискового пространства](https://bugs.kde.org/show_bug.cgi?id=436300) (David Redondo, Plasma 5.22)

Исправлено множество недочётов на странице «Рабочие столы» Параметров системы, в том числе [сломанное отображение текста](https://bugs.kde.org/show_bug.cgi?id=437466), то, что [кнопка «Применить» вовремя не активировалась](https://bugs.kde.org/show_bug.cgi?id=435014), а [кнопка «По умолчанию» не восстанавливала удалённые рабочие столы](https://bugs.kde.org/show_bug.cgi?id=437459), а также [несколько проблем](https://bugs.kde.org/show_bug.cgi?id=437472) [с выбором](https://bugs.kde.org/show_bug.cgi?id=432618) [продолжительности](https://bugs.kde.org/show_bug.cgi?id=437460) [анимации](https://bugs.kde.org/show_bug.cgi?id=425538) (Nicolas Fella и David Edmundson, Plasma 5.22)

## Улучшения пользовательского интерфейса

В Gwenview [задействовано KHamburgerMenu](https://invent.kde.org/graphics/gwenview/-/merge_requests/70), что придаёт ему более чистый вид и более доступный выбор действий, когда строка меню скрыта (Noah Davis, Gwenview 21.08):

![0](https://pointieststick.files.wordpress.com/2021/05/screenshot_20210528_221507-1.png?w=1024)

Напоминаю, если вам это не нравится, вы можете просто включить показ строки меню, и меню-гамбургер исчезнет

В представлении «Установлено» в Discover [регистр теперь не учитывается при поиске](https://bugs.kde.org/show_bug.cgi?id=437371) (Aleix Pol Gonzalez, Plasma 5.22)

Новый Системный монитор теперь позволяет вам [вводить несколько поисковых запросов через запятую](https://bugs.kde.org/show_bug.cgi?id=434630), как в старом KSysGuard (David Edmundson, Plasma 5.23)

Теперь пользователям [сообщается](https://bugs.kde.org/show_bug.cgi?id=437152), что они потеряют, если отключат индексацию файлов (Nate Graham, Plasma 5.23)

Виджет виртуальной клавиатуры в системном лотке, который появляется в сеансе Wayland, [теперь полностью отключается](https://bugs.kde.org/show_bug.cgi?id=437171), если вы отключили виртуальную клавиатуру глобально (Nicolas Fella, Plasma 5.23)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://vk.com/ilya_bizyaev)  
*Источник:* <https://pointieststick.com/2021/05/28/this-week-in-kde-performance-improvements-galore/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
