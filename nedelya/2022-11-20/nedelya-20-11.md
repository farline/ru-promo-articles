# На этой неделе в KDE: сносные ошибки в Discover

На этой неделе я хотел бы выделить конкретную исправленную 15-минутную ошибку: когда центр приложений Discover показывает вам важные сообщения об ошибках, [они теперь выглядят как обычные диалоги](https://bugs.kde.org/show_bug.cgi?id=403791), а не крошечные уведомления внизу экрана, исчезающие через несколько секунд. Да и в целом [бесполезных сообщений стало меньше](https://bugs.kde.org/show_bug.cgi?id=461813)! Эти улучшения войдут в Plasma 5.27 благодаря работе Jakub Narolewski и Aleix Pol Gonzalez, спасибо им!

Но это не всё! Мы потрудились над исправлением других заметных ошибок, а также добавили пару новшеств:

## Новые возможности

Системный монитор и его виджеты теперь могут определять и отслеживать энергопотребление видеокарт NVIDIA (Pedro Liberatti, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/ksystemstats/-/merge_requests/43))

На значок виджета погоды — как в системном лотке, так и вне него — теперь можно выводить текущую температуру! (Ismael Asensio, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/268)):

![0](https://pointieststick.files.wordpress.com/2022/11/image-7.png)

## Улучшения пользовательского интерфейса

Скорость прокрутки в просмотрщике документов Okular при использовании сенсорной панели теперь значительно выше и соответствует скорости прокрутки сенсорной панелью в других местах (Евгений Попов, Okular 23.04. [Ссылка](https://invent.kde.org/graphics/okular/-/merge_requests/650))

В диалоге выполняемых задач в Discover индикаторы состояния больше не сливаются с бесполезным выделением на фоне (Nate Graham, Plasma 5.26.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461812)):

![1](https://pointieststick.files.wordpress.com/2022/11/image-8.png)

При смене воспроизводимых дорожек с открытым виджетом «Проигрыватель» в нём больше не мелькает значок воспроизводящего приложения (Fushan Wen, Plasma 5.26.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461496))

Улучшено сообщение об ошибке, отображаемое при неудавшемся запуске службы передачи файлов по Bluetooth (Fushan Wen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/bluedevil/-/merge_requests/83))

Discover больше не будет автоматически проверять наличие обновлений при использовании тарифицируемого интернет-соединения (Bernardo Gomes Negri, Plasma 6. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/414))

## Другие важные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Когда эмулятор терминала Konsole запускается после изменения расположения экранов, его главное окно больше не становится смехотворно маленьким (Влад Загородний, Konsole 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460428))

Музыкальный проигрыватель Elisa больше не запинается периодически во время воспроизведения (Роман Лебедев, Elisa 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461299))

При использовании док-панели Latte в сеансе Plasma Wayland различные всплывающие окна Plasma больше не располагаются неправильно (David Redondo, Latte Dock 0.10.9. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461856))

Plasma больше не падает иногда в сеансе Wayland при движении курсора над элементами панели (Arjen Hiemstra, Plasma 5.26.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=447717))

Когда стандартное меню запуска приложений настроено на использование обычного размера элементов списка, значки приложений на боковой панели категорий (например, значок Центра справки) больше не слишком большие (Nate Graham, Plasma 5.26.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460349))

Диспетчер окон KWin теперь учитывает свойство «Ориентация панели DRM», которое иногда устанавливается ядром для экранов. Это означает, что на разнообразных устройствах, которым требуется по умолчанию повернуть экран, это теперь будет выполняться автоматически (Xaver Hugl, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=397452))

Различные элементы пользовательского интерфейса Plasma снова имеют правильный размер в сеансе Plasma X11, если не активировано масштабирование Plasma средствами Qt (Fushan Wen, Frameworks 5.100.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461682))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 10 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 11). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 50 (на прошлой неделе была 51). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 137 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-11-11&chfieldto=2022-11-18&chfieldvalue=RESOLVED&list_id=2212523&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

На Вики появилась новая страница [«Добро пожаловать в KDE»](https://community.kde.org/Welcome_to_KDE), ссылка на которую будет включена в наше новое [Приложение приветствия при первом запуске](https://invent.kde.org/plasma/plasma-welcome), готовящееся к дебюту в Plasma 5.27 (Nate Graham)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/11/18/this-week-in-kde-less-rage-inducing-error-messages-in-discover/>  

<!-- 💡: Help Center → Центр справки -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: system tray → системный лоток -->
