# На этой неделе в KDE: долой надоедливые ошибки!

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)

Это была серьёзная неделя в плане исправления ошибок, в ходе которой было устранено множество раздражающих проблем: некоторые недавние ухудшения, а также довольно много старых ошибок.

Что касается ошибок и недавних ухудшений, я начал думать более глобально над тем, как исправить ситуацию к лучшему. Программы KDE в значительной степени преодолели свои исторические проблемы чрезмерного потребления ресурсов и визуальной несогласованности, и наш следующий серьёзный вызов — надёжность. Одна из идей, которую я обдумываю: начать инициативу, в ходе которой нужно сосредоточиться на «15-минутных ошибках» — тех неприятных проблемах, которые можно легко обнаружить за несколько минут обычного использования системы. [Вот](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=1939484&order=dupecount%20DESC%2Cbug_severity%2Cchangeddate%20DESC%2Cbug_status%2Cpriority%2Cassigned_to%2Cbug_id&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced) предварительный список этих проблем в Plasma. Я бы посоветовал всем опытным разработчикам попытаться сосредоточиться именно на них! Эффект будет очень заметным.

> 21–28 ноября, основное — прим. переводчика

## Wayland

В сеансе Plasma Wayland перетаскивание файла или папки из всплывающего окна виджета «Просмотр папки» в родительскую папку больше [не приводит к сбою](https://bugs.kde.org/show_bug.cgi?id=399864) Plasma (Marco Martin, Plasma 5.24)

В сеансе Wayland при использовании стилуса теперь [можно](https://bugs.kde.org/show_bug.cgi?id=432104) активировать другое окно, щёлкнув по его заголовку, а также просто взаимодействовать с заголовками окон в остальных случаях (перетаскивать, менять размер и т. д.) (Fushan Wen, Plasma 5.24)

В сеансе Wayland виджет «Буфер обмена» теперь [отображает](https://bugs.kde.org/show_bug.cgi?id=442923) записи изображений, скопированных с помощью консольной программы `wl-copy` (Méven Car, Plasma 5.24)

## Исправления ошибок и улучшения производительности

Создание архивов в основном пользовательском интерфейсе Ark [снова работает](https://bugs.kde.org/show_bug.cgi?id=445610) (Kai Uwe Broulik, Ark 21.12)

Кнопки масштабирования Okular теперь [всегда включаются и выключаются](https://bugs.kde.org/show_bug.cgi?id=440173) в нужное время, в частности при открытии нового документа (Albert Astals Cid, Okular 21.12)

Теперь Ark может обрабатывать архивы, файлы в которых [имеют абсолютные пути](https://invent.kde.org/utilities/ark/-/merge_requests/30), а не только относительные (Kai Uwe Broulik, Ark 22.04)

Сенсорная прокрутка в Konsole теперь [работает правильно](https://bugs.kde.org/show_bug.cgi?id=437553) (Henry Heino, Konsole 22.04)

[Исправлена](https://bugs.kde.org/show_bug.cgi?id=44396) распространённая причина падения в системном лотке (Fushan Wen, Plasma 5.23.4)

[Устранено](https://bugs.kde.org/show_bug.cgi?id=443745) часто встречавшееся падение в Discover, возникавшее в процессе управления приложениями Flatpak (Aleix Pol Gonzalez, Plasma 5.23.4)

Экран выхода из системы снова [имеет размытый фон](https://bugs.kde.org/show_bug.cgi?id=444899) вместе с [анимациями появления и исчезновения](https://bugs.kde.org/show_bug.cgi?id=444898) (David Edmundson, Plasma 5.23.4)

Изменение различных настроек в Параметрах системы больше [не вызывает эффект мерцания](https://invent.kde.org/plasma/kwin/-/merge_requests/1672) за панелями Plasma (Влад Загородний, Plasma 5.24)

## Улучшения пользовательского интерфейса

Текстовый редактор Kate был [заменён](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/704) на KWrite в наборе избранных приложений по умолчанию, так как он несколько более удобен для обычных пользователей и менее ориентирован на программистов (Nate Graham, Plasma 5.24)

Несколько сбивающий с толку флажок в нижней части страницы обновлений Discover был [преобразован](https://invent.kde.org/plasma/discover/-/merge_requests/207) в пару кнопок и надпись, что должно быть понятнее, а также на этой странице больше [не так часто встречается слово «Обновления»](https://invent.kde.org/plasma/discover/-/merge_requests/206) (Nate Graham, Plasma 5.24):

![0](https://pointieststick.files.wordpress.com/2021/11/updates-page-now.png)

При использовании мультимедийного сервера PipeWire и потоковой передачи звука с одного устройства на другое, аудиопоток теперь [отображает](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/96) имя удалённого устройства в виджете «Громкость» Plasma (Nicolas Fella, Plasma 5.24)

В окне свойств файла теперь [отображается](https://invent.kde.org/frameworks/kio/-/merge_requests/636) приложение, которое откроет этот файл (Kai Uwe Broulik, Frameworks 5.89):

![1](https://pointieststick.files.wordpress.com/2021/11/screenshot_20211122_114332.png)

В диалоговом окне выбора значка папки теперь [предварительно выбирается](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/48) используемый на данный момент значок для облегчения визуализации и навигации с помощью клавиатуры (Kai Uwe Broulik, Frameworks 5.89)

Текст в небольших временных сообщениях, которые иногда появляются в нижней части окон приложений на основе библиотеки Kirigami (и бессмысленно называются «тостами» в мире Android), теперь [разборчивее](https://bugs.kde.org/show_bug.cgi?id=440390) (Felipe Kinoshita, Frameworks 5.89)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Перевод:* [Илья Андреев](https://vk.com/ilyandl)  
*Источник:* <https://pointieststick.com/2021/11/26/this-week-in-kde-fixing-a-bunch-of-annoying-bugs/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
