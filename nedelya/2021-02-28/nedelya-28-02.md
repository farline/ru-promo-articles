# На этой неделе в KDE: всего понемногу

Продолжаем исправлять Plasma 5.21, а на этой неделе ещё многое довели до ума в интерфейсе:

> 21–28 февраля, основное — прим. переводчика

## Новые возможности

Теперь вы [можете применять глобальные темы, цветовые схемы, наборы курсоров, темы Plasma и обои из командной строки, используя новые утилиты командной строки с именами вроде `plasma-apply-colorscheme`](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/630) (Dan Leinir Turthra Jensen, Plasma 5.22)

Приложения KDE [теперь поддерживают форматы изображений HEIF и HEIC](https://invent.kde.org/frameworks/kimageformats/-/merge_requests/16) (Daniel Novomeský, Frameworks 5.80)

Панель задач [теперь может быть настроена таким образом, чтобы скрытая панель не становилась видимой, когда одно или нескольких приложений получают статус «требует внимания»](https://bugs.kde.org/show_bug.cgi?id=394119) (Michael Moon, Plasma 5.22)

## Исправления и улучшения производительности

Установленный kio-fuse [больше не мешает строке поиска и запуска KRunner открывать адреса `man:` и `info:` в веб-браузере](https://bugs.kde.org/show_bug.cgi?id=433300) (Fabian Vogt, kio-fuse 5.0.1)

Диспетчер окон KWin [больше не ломается в сеансе Plasma Wayland при копировании чего-либо из приложения XWayland и немедленной вставке в «родное» приложение Wayland](https://invent.kde.org/plasma/kwin/-/merge_requests/693) (Jan Blackquill, Plasma 5.21.1)

Повтор клавиш [наконец-то, действительно, точно включён по умолчанию](https://bugs.kde.org/show_bug.cgi?id=431923). Извините за это. 😦 (Jan Blackquill и David Edmundson, Plasma 5.21.2)

Экраны на странице их настройки в Параметрах системы [снова перетаскиваются](https://bugs.kde.org/show_bug.cgi?id=433178) (Marco Martin, Plasma 5.21.2)

На странице настройки значков Параметров системы [нижний ряд кнопок теперь скрывает не уместившиеся кнопки в выпадающее меню](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/665), что особенно полезно в Plasma Mobile (Dan Leinir Turthra Jensen, Plasma 5.21.2):

<https://i.imgur.com/h0VyVh0.mp4>

Значки системного лотка в очень тонких панелях [больше не мыльные](https://bugs.kde.org/show_bug.cgi?id=433138) (Niccolò Venerandi, Plasma 5.21.2)

Открытые виртуальные клавиатуры [больше не перекрывают панели Plasma](https://invent.kde.org/plasma/kwin/-/merge_requests/717) (Aleix Pol Gonzalez, Plasma 5.22)

Переименование файла изображения тем же именем, что было у другого переименованного файла, [больше не приводит к отображению неправильной миниатюры у нового файла](https://bugs.kde.org/show_bug.cgi?id=433127) (Méven Car, Frameworks 5.80)

## Улучшение пользовательского интерфейса

Функция перекомпоновки текста в Konsole [теперь работает лучше для пользователей оболочки zsh](https://invent.kde.org/utilities/konsole/-/merge_requests/337) (Carlos Alves, Konsole 21.04)

Dolphin [теперь чуть быстрее отображает миниатюры](https://invent.kde.org/system/dolphin/-/merge_requests/182) (Méven Car, Dolphin 21.04)

Панель заголовка у GTK-приложений теперь отображает кнопки свернуть/развернуть/и т. д., соответствующие теме, [даже при использовании темы декорации окон Aurorae](https://bugs.kde.org/show_bug.cgi?id=432712) (Alois Wohlschlager, Plasma 5.21.2)

Поиск Discover среди приложений Flatpak [теперь сильнее основывается на соответствии заголовку и более высоких рейтингах](https://invent.kde.org/plasma/discover/commit/28cc7c56471bd11b84e0da639a9dd33cfbcaf3b9) (Aleix Pol Gonzalez, Plasma 5.22):

![1](https://pointieststick.files.wordpress.com/2021/02/screenshot_20210224_195511.png?w=881)

Discover [теперь понятнее показывает в поиске и списках, когда приложение приходит из нестандартного источника](https://bugs.kde.org/show_bug.cgi?id=433370) (Aleix Pol Gonzalez и Nate Graham, Plasma 5.22):

![2](https://pointieststick.files.wordpress.com/2021/02/screenshot_20210224_104946.png?w=1003)

Компактный/мобильный вид Discover [теперь понятнее показывает на домашней странице, что доступны обновления](https://invent.kde.org/plasma/discover/-/merge_requests/82) (Dan Leinir Turthra Jensen, Plasma 5.22):

![3](https://pointieststick.files.wordpress.com/2021/02/image.png?w=744)

Уведомления, информирующие вас о файловой операции с известным адресом назначения, [теперь всегда отображают кнопку «Открыть содержащую папку»](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/672) (Kai Uwe Broulik, Plasma 5.22)

При сворачивании боковой панели в приложении на основе библиотеки Kirigami содержимое панели инструментов этого приложения [теперь движется как по маслу](https://bugs.kde.org/show_bug.cgi?id=413841) (Marco Martin, Frameworks 5.80)

## Сетевое присутствие

Carl Schwan [переделал](https://carlschwan.eu/2021/02/26/documentation-improvements-in-kde/) [api.kde.org](https://api.kde.org) и онлайн-документацию Kirigami

Niccolò Venerandi опубликовал третье видео в серии о создании тем Plasma:

<https://www.youtube.com/watch?v=5HGdJ8ZhXos>

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://pointieststick.com/2021/02/26/this-week-in-kde-a-little-bit-of-everything-2/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
