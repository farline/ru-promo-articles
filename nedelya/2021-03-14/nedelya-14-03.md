# На этой неделе в KDE: Elisa растёт!

На этой неделе я хочу отметить кое-что важное: музыкальный проигрыватель Elisa [получил](https://invent.kde.org/multimedia/elisa/-/merge_requests/205) мобильный интерфейс, что делает его первоклассным приложением для Plasma Mobile и Android! Масса кода при этом переиспользуется благодаря инструментарию пользовательского интерфейса Kirigami. Большое спасибо Devin Lin за этот чрезвычайно значимый вклад, превращающий Elisa в полностью адаптивный музыкальный проигрыватель!

![1](https://pointieststick.files.wordpress.com/2021/03/tracks.png?w=472)
![2](https://pointieststick.files.wordpress.com/2021/03/playlist.png?w=460)
![3](https://pointieststick.files.wordpress.com/2021/03/player.png?w=472)

Кроме того, мы проделали большую работу над новым Системным монитором Plasma, который по умолчанию заменит древний KSysGuard в Plasma 5.22. В настоящее время он опционален, поэтому мы очень благодарны пользователям-смельчакам за его тестирование. Благодаря им, полное его внедрение в Plasma 5.22 должно пройти куда беспроблемнее. Спасибо вам всем!

> 7–14 марта, основное — прим. переводчика

## Другие новые возможности

В Kate [появилась](https://invent.kde.org/utilities/kate/-/merge_requests/306) функция, позволяющая вернуться к предыдущему положению курсора (Waqar Ahmed, Kate 21.04):

![4](https://pointieststick.files.wordpress.com/2021/03/screenshot_20210310_092614.png?w=1024)

[Реализовано](https://bugs.kde.org/show_bug.cgi?id=429804) уведомление с просьбой создать резервную копию данных при обнаружении некоторых проблем с диском, которые ещё не дотягивают до полного сбоя SMART, но, тем не менее, требуют внимания и потенциально в скором времени могут привести к потере данных (Harald Sitter, Plasma 5.22)

## Исправления и улучшения производительности

Когда в Dolphin выбрана сортировка по размеру, он больше [не отображает](https://bugs.kde.org/show_bug.cgi?id=433207) каталоги первыми, когда его об этом не просят (Méven Car, Dolphin 21.04)

Утилита для создания снимков экрана Spectacle больше [не включает](https://invent.kde.org/graphics/spectacle/-/merge_requests/57) непрошенный режим «По щелчку» при определённых обстоятельствах (Влад Загородний, Spectacle 21.04)

[Исправлен](https://bugs.kde.org/show_bug.cgi?id=434074) случай, когда новый Системный монитор мог аварийно завершить работу под Wayland (Arjen Hiemstra, Plasma 5.21.3)

Параметр «Использовать другой DPI» снова [можно использовать](https://bugs.kde.org/show_bug.cgi?id=433115) в сеансе Wayland (Nate Graham, Plasma 5.21.3)

В новом Системном мониторе [заработало](https://bugs.kde.org/show_bug.cgi?id=433706) перемещение виджета по странице (Arjen Hiemstra, Plasma 5.21.3)

Режим Spectacle «Прямоугольная область» снова [работает](https://bugs.kde.org/show_bug.cgi?id=432260) в сеансе Plasma Wayland (Влад Загородний, Plasma 5.22)

При использовании Dropbox и установке его элемента в системном лотке как «Всегда скрывать», его состояние теперь [запоминается](https://bugs.kde.org/show_bug.cgi?id=378910) при перезагрузке компьютера (Konrad Materka, Plasma 5.22)

Файловые диалоги теперь [добавляют](https://bugs.kde.org/show_bug.cgi?id=431638) правильное расширение имени файла при сохранении в случае, когда имя файла документа уже заканчивается точкой (Robert Hoffman, Frameworks 5.81)

Гарнитуры со встроенной кнопкой «Воспроизвести/Остановить» теперь [делают то, что нужно](https://bugs.kde.org/show_bug.cgi?id=403636), каждый раз при её нажатии, а не через раз (David Redondo, Frameworks 5.81)

Поля панели больше [не меняются](https://bugs.kde.org/show_bug.cgi?id=430622) при отключении расширения компоновщика (Niccolò Venerandi, Frameworks 5.81)

## Улучшения пользовательского интерфейса

Konsole снова [показывает](https://bugs.kde.org/show_bug.cgi?id=433210) профиль по умолчанию в пользовательском интерфейсе (Ahmad Samir, Konsole 21.04)

В Okular [реализована](https://bugs.kde.org/show_bug.cgi?id=340134) поддержка тегов `<annotation>` и `<subtitle>` в файлах FictionBook (Юрий Чорноиван, Okular 21.04)

После перемещения или копирования файла в уведомлении теперь [указывается](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/575), какое приложение откроет файл, если вы нажмёте кнопку «Открыть» (Kai Uwe Broulik, Plasma 5.22):

![5](https://pointieststick.files.wordpress.com/2021/03/open-with-gwenview-from-notification.png?w=778)

Виджет «Зашифрованные папки» (Plasma Vaults) теперь [предлагает](https://invent.kde.org/plasma/plasma-vault/-/merge_requests/10) отдельное действие «Открыть в диспетчере файлов», которое можно выполнить, чтобы из списка сразу же перейти к просмотру нужной папки (Nate Graham, Plasma 5.22):

![6](https://pointieststick.files.wordpress.com/2021/03/show-in-flie-manager-vaults-action.png?w=1024)

При обновлении драйверов Nvidia (или любых других пакетов с лицензионным соглашением) Discover больше [не предлагает](https://bugs.kde.org/show_bug.cgi?id=431871) повторно принять лицензионное соглашение, если оно фактически не изменилось (Aleix Pol Gonzalez, Plasma 5.22)

Виджет «Клейкая заметка» теперь [спрашивает](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/59) подтверждение при попытке удалить заметку, но только в том случае, когда в ней есть реальное содержимое, а не когда она пуста или только что создана из содержимого буфера обмена (Nate Graham, Plasma 5.22):

<https://i.imgur.com/O59ZVBs.mp4>

<https://i.imgur.com/iyl229d.mp4>

Новый Системный монитор теперь [запоминает](https://invent.kde.org/plasma/libksysguard/-/merge_requests/136) расположение столбцов таблиц и боковых панелей при выходе и повторном запуске (Arjen Hiemstra, Plasma 5.22)

Значки Breeze, отображающие состояние блокировки, теперь [соответствуют](https://bugs.kde.org/show_bug.cgi?id=244542) единому визуальному соглашению: значки «заблокировано» имеют заполненное тело, а «разблокировано» — полое (Nate Graham, Frameworks 5.81):

![7](https://pointieststick.files.wordpress.com/2021/03/light-icons.png?w=1024)

## Сетевое присутствие

Доступен четвёртый выпуск серии Niccolò о создании тем Plasma:

<https://www.youtube.com/watch?v=QWomFi0WGic>

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность оказать финансовую поддержку фонду KDE e.V.

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Андреев](https://vk.com/ilyandl)  
*Источник:* <https://pointieststick.com/2021/03/12/this-week-in-kde-elisa-grows-up/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: convergent → адаптивный -->
<!-- 💡: icons → значки -->
<!-- 💡: location → расположение -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: venerable → древний -->
