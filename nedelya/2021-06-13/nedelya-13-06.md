# На этой неделе в KDE: Plasma 5.22 — круто, но сейчас вы захотите 5.23

> 6–13 июня, основное — прим. переводчика

На этой неделе вышла Plasma 5.22! В целом, наше внимание к стабильности дало свои плоды, и на данный момент нам не известно о каких-либо критических ошибках в новом выпуске. А менее серьёзные проблемы мы уже исправили в 5.22.1 :)

Но в то же время случилось кое-что поважнее: был завершён крупный этап работы над инициативой [«Эволюция Breeze»](https://phabricator.kde.org/T10891), который обновил стиль [кнопок, пунктов меню, флажков, переключателей, ползунков и других компонентов](https://invent.kde.org/plasma/breeze/-/merge_requests/26)! Выглядит прекрасно:

![1](https://pointieststick.files.wordpress.com/2021/06/dolphin-sort-menu.png?w=2142)

![2](https://pointieststick.files.wordpress.com/2021/06/gwenview.png?w=2142)

![3](https://pointieststick.files.wordpress.com/2021/06/okular.png?w=2142)

![4](https://pointieststick.files.wordpress.com/2021/06/spectacle.png?w=2142)

![5](https://pointieststick.files.wordpress.com/2021/06/touchpad-kcm.png?w=1811)

Заодно новый стиль исправляет пару вечных проблем старого: [«исчезновение» полос прокрутки над выделенным элементом списка](https://bugs.kde.org/show_bug.cgi?id=414990) и [неочевидное обозначение действия по умолчанию в диалогах](https://bugs.kde.org/show_bug.cgi?id=364226).

Эту работу проделал Jan Blackquill в соответствии с [макетами](https://phabricator.kde.org/T13451) от Manuel Jesús de la Fuente и других участников команды дизайна (KDE VDG). Нововведения войдут в Plasma 5.23. Для внесения изменений в итоговый стиль ещё много времени, но в целом, как мне кажется, получилось очень хорошо; надеюсь, вы рады так же, как и я!

## Новые возможности

Kate теперь поддерживает работу с языковым сервером [языка программирования Dart](https://invent.kde.org/utilities/kate/-/merge_requests/428) (Waqar Ahmed, Kate 21.08)

Эмулятор терминала Konsole [теперь совместим со стандартом DECSET 1003](https://bugs.kde.org/show_bug.cgi?id=416530), что позволяет использовать функции, требующие отслеживания движений мыши, в консольных программах вроде `vim` (Luis Javier Merino Morán, Konsole 21.08)

Строго говоря, это не проект KDE, но точно важно для нас: экран входа в систему (SDDM) [теперь может работать как полноценный клиент Wayland](https://github.com/sddm/sddm/pull/1393) без каких-либо зависимостей от X11! (Aleix Pol Gonzalez, SDDM 0.20)

## Исправления ошибок и улучшения производительности

Konsole теперь [правильно обрабатывает](https://bugs.kde.org/show_bug.cgi?id=425926) двойные щелчки правой кнопкой мыши (Luis Javier Merino Morán, Konsole 21.08)

Режим «мышь XTerm» редактора Emacs [теперь работает в Konsole](https://bugs.kde.org/show_bug.cgi?id=302731) (Luis Javier Merino Morán, Konsole 21.08)

В сеансе Plasma Wayland вторичные экраны [теперь обнаруживаются при использовании нескольких видеокарт](https://bugs.kde.org/show_bug.cgi?id=431062) (Xaver Hugl, Plasma 5.22.1)

Источник данных BBC в виджете погоды [снова работает](https://bugs.kde.org/show_bug.cgi?id=430643): они поменяли API, нам пришлось адаптироваться (Joe Dight, Plasma 5.22.1)

В сеансе Plasma Wayland прозрачный фон за переключателями окон [теперь всегда размыт, как и должно быть](https://bugs.kde.org/show_bug.cgi?id=433131) (Влад Загородний, Plasma 5.22.1)

Пользовательские сочетания клавиш для обхода окон приложения (по умолчанию Alt+`) [теперь работают](https://bugs.kde.org/show_bug.cgi?id=359141) (Андрей Бутырский, Plasma 5.22.1)

## Другие улучшения пользовательского интерфейса

Диалоги «Загрузить \[что-то\]» теперь лучше [обрабатывают](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/125) ситуации, когда сервер KDE Store не отвечает или отвечает медленно (Dan Leinir Turthra Jensen, Frameworks 5.84):

![6](https://pointieststick.files.wordpress.com/2021/06/image.png?w=798)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2021/06/11/this-week-in-kde-plasma-5-22-arrives-but-i-bet-youll-want-5-23-once-i-show-you-this/>  
