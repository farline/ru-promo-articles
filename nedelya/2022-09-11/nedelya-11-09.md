# На этой неделе в KDE: подготовка Plasma 5.26

> 4–11 сентября, основное — прим. переводчика

На этой неделе мы готовились к бета-выпуску Plasma 5.26 и были сосредоточены на исправлении ошибок и доработке пользовательского интерфейса. Ожидайте больше подобного в течение нескольких следующих недель!

## Новые возможности

В сенсорном режиме в сеансе Plasma Wayland теперь можно принудительно вызвать виртуальную клавиатуру Maliit, даже если она не появилась автоматически (Aleix Pol Gonzalez, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/2907))

В приложении Системный монитор и одноимённых виджетах Plasma теперь можно запросить у датчиков минимальную, максимальную и среднюю температуру и частоту вашего процессора (Alessio Bonfiglio, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/ksystemstats/-/merge_requests/21))

## Улучшения пользовательского интерфейса

Просмотрщик изображений Gwenview теперь может открывать файлы формата `.xcf`, принадлежащего графическому редактору GIMP (Nicolas Fella, Gwenview 22.08.1. [Ссылка](https://invent.kde.org/graphics/gwenview/-/merge_requests/156))

Музыкальный проигрыватель Elisa теперь показывает вам понятное сообщение, объясняющее, в чём проблема, когда вы перетаскиваете на него несовместимый файл (Bharadwaj Raju, Elisa 22.12. [Ссылка](https://invent.kde.org/multimedia/elisa/-/merge_requests/376))

Стандартное меню запуска приложений теперь отображает пункт «Удалить или настроить дополнения…» в контекстном меню приложений Flatpak (Nate Graham, Plasma 5.24.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=458812))

В разделе «Ночная цветовая схема» Параметров системы был упрощён интерфейс включения и выключения этой функции: теперь состояние «Выключено» является частью выпадающего списка для выбора времени активации, а не отдельным флажком (Bharadwaj Raju, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2093)):

![0](https://pointieststick.files.wordpress.com/2022/09/night-color-activation-times.jpg)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Подключение к общим папкам SMB, предоставляемым устройствами с ОС Windows, теперь работает при использовании библиотеки `samba-libs` версии 4.16 и новее: её авторы ни с того ни с сего поменяли её поведение, и нам пришлось подстраиваться! (Harald Sitter, kio-extra 22.08.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=453090))

Исправлена ещё одна распространённая причина сбоев диспетчера окон KWin в сеансе Plasma Wayland при подключении или отключении экранов (Влад Загородний, Plasma 5.25.5. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=451028))

Сторонние оформления рабочего стола больше не могут переопределять код строки поиска и запуска KRunner, а значит, и ломать его до невозможности запуститься — да, было и такое (Alexander Lohnau, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457174))

Эффект плавного перехода вернулся в KWin, а значит, вы снова увидите красивое плавное перекрёстное затухание при распахивании и восстановлении окон, а также при перемещении курсора между всплывающими подсказками панели (Marco Martin, Plasma 5.26. [Ссылка](https://invent.kde.org/plasma/kwin/-/merge_requests/2753))

Элементы панели задач стали гораздо устойчивее к случайному перетаскиванию, когда вы хотите просто щёлкнуть по ним (Nate Graham, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=402376))

Анимация преобразования всплывающих окон снова работает на всплывающих подсказках панелей в сеансе Plasma Wayland (Marco Martin, Frameworks 5.99. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=446375))

Другие сведения об ошибках, которые могут вас заинтересовать:

* 45 15-минутных ошибок в Plasma (на прошлой неделе было 49). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* 20 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе была 21). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 121 ошибка исправлена на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-09-02&chfieldto=2022-09-09&chfieldvalue=RESOLVED&list_id=2148189&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/09/09/this-week-in-kde-getting-plasma-5-26-ready/>  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: applet → виджет -->
<!-- 💡: task manager → панель задач -->
